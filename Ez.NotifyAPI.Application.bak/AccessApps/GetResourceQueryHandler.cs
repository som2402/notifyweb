﻿// using System.Collections.Generic;
// using System.Threading;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Ez.IS4.API.Driver.Models.Response;
// using MediatR;
// using Ez.Notify.Application.Config;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Application.AcessApps
// {
//     public class GetResourceQuery : IRequest<BaseEntityResponse<IList<Resource>>>
//     {
//     }
//
//     public class GetResourceQueryHandler : IRequestHandler<GetResourceQuery, BaseEntityResponse<IList<Resource>>>
//     {
//         private readonly AccessClient _accessClient;
//         private readonly AppConfig _appConfig;
//
//         public GetResourceQueryHandler(AppConfig appConfig, AccessClient accessClient)
//         {
//             _appConfig = appConfig;
//             _accessClient = accessClient;
//         }
//
//         public async Task<BaseEntityResponse<IList<Resource>>> Handle(GetResourceQuery request,
//             CancellationToken cancellationToken)
//         {
//             var data = await _accessClient.GetResources(_appConfig.AppGroupResourceKey);
//             var response = new BaseEntityResponse<IList<Resource>>
//             {
//                 Data = data,
//                 Status = true
//             };
//             return response;
//         }
//     }
// }