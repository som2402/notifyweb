﻿using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.EmailActionClicks.Repositories
{
    public class EmailActionClickRepository : EfRepository<DataNotifyDbContext, EmailActionClick>,
        IEmailActionClickRepository
    {
        public EmailActionClickRepository(DataNotifyDbContext context) : base(context)
        {
        }
    }

    public interface IEmailActionClickRepository : IRepository<EmailActionClick>
    {
    }
}