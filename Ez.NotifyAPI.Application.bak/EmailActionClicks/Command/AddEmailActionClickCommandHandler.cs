﻿using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.EmailActionClicks.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.EmailActionClicks.Command
{
    public class AddEmailActionClickCommand : IRequest<bool>
    {
        public AddEmailActionClickCommand(int emailId, string link)
        {
            EmailId = emailId;
            Link = link;
        }

        public int EmailId { get; private set; }
        public string Link { get; private set; }
    }

    public class AddEmailActionClickCommandHandler : IRequestHandler<AddEmailActionClickCommand, bool>
    {
        private readonly IEmailActionClickRepository _emailActionClickRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddEmailActionClickCommandHandler(IEmailActionClickRepository emailActionClickRepository,
            IUnitOfWork unitOfWork)
        {
            _emailActionClickRepository = emailActionClickRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddEmailActionClickCommand request, CancellationToken cancellationToken)
        {
            await _emailActionClickRepository.AddAsync(new EmailActionClick(request.EmailId, request.Link));
            await _unitOfWork.CommitAsync(cancellationToken);
            return true;
        }
    }
}