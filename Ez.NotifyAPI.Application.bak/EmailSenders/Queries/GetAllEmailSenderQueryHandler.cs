﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
//
// namespace Ez.Notify.Application.EmailSenders.Queries
// {
//     public class GetAllEmailSenderQuery : IRequest<BaseEntityResponse<List<EmailSenderDto>>>
//     {
//     }
//
//     public class
//         GetAllEmailSenderQueryHandler : IRequestHandler<GetAllEmailSenderQuery, BaseEntityResponse<List<EmailSenderDto>>
//         >
//     {
//         private readonly IRepository<EmailSender> _repository;
//
//         public GetAllEmailSenderQueryHandler(IRepository<EmailSender> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<List<EmailSenderDto>>> Handle(GetAllEmailSenderQuery request,
//             CancellationToken cancellationToken)
//         {
//             var emailSenders = await _repository.GetAllAsync();
//             var emailSenderDtos = emailSenders.Select(item => new EmailSenderDto {Code = item.Code, Title = item.Title})
//                 .ToList();
//
//             var response = new BaseEntityResponse<List<EmailSenderDto>>
//             {
//                 Data = emailSenderDtos,
//                 Status = true
//             };
//             return response;
//         }
//     }
// }