﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.EmailSenders.Repositories
{
    public interface IEmailSenderRepository : IRepository<EmailSender>
    {
        Task<EmailSender> GetByCode(string code);
        Task<EmailSender> GetByDefault();
    }

    public class EmailSenderRepository : EfRepository<DataNotifyDbContext, EmailSender>, IEmailSenderRepository
    {
        public EmailSenderRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<EmailSender> GetByCode(string code)
        {
            return await Queryable.Where(EmailSenderExpression.Code(code)).SingleOrDefaultAsync();
        }

        public async Task<EmailSender> GetByDefault()
        {
            return await Queryable.Where(EmailSenderExpression.Default()).SingleOrDefaultAsync();
        }
    }
}