﻿namespace Ez.Notify.Application.Config
{
    public class FirebaseConfig
    {
        public string GoogleCredential { get; set; }
    }
}