﻿namespace Ez.Notify.Application.Config
{
    public class AppConfig
    {
        public string AppGroupResourceKey { get; set; }
        public string ClickTrackingPrivateKeyPepper { get; set; }
        public string NotifyPublicApiDomain { get; set; }
    }
}