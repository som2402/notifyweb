﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;

namespace Ez.Notify.Application.CustomerNotifyConfigs.Repositories
{
    public class CustomerNotifyConfigExpression
    {
        public static Expression<Func<CustomerNotifyConfig, bool>> Id(int id) =>
            customerNotifyConfig => customerNotifyConfig.Id == id;

        public static Expression<Func<CustomerNotifyConfig, bool>> AccountId(string accountId) =>
            customerNotifyConfig => customerNotifyConfig.AccountId == accountId;

        public static Expression<Func<CustomerNotifyConfig, bool>> FindByCondition(string appId, string accountId,
            string notifyConfigCode) =>
            customerNotifyConfig => customerNotifyConfig.AccountId == accountId &&
                                    customerNotifyConfig.AppId.Equals(appId) &&
                                    customerNotifyConfig.NotifyConfigCode.Equals(notifyConfigCode);
    }
}