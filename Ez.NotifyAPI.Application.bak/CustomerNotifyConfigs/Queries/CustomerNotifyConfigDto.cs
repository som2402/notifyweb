﻿namespace Ez.Notify.Application.CustomerNotifyConfigs.Queries
{
    public class CustomerNotifyConfigDto
    {
        public int Id { get; set; }
        public string AppId { get; set; }
        public string AccountId { get; set; }
        public string NotifyConfigCode { get; set; }
        public int? SendEmail { get; set; }
        public int? SendWeb { get; set; }
        public int? SendMobile { get; set; }
        public int? SendDesktop { get; set; }
    }
}