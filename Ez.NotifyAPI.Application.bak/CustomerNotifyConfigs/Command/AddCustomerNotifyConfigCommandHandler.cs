﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Common.Utilities;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.CustomerNotifyConfigs.Command
// {
//     public class AddCustomerNotifyConfigCommand : IRequest<BaseEntityResponse<List<CustomerNotifyConfig>>>
//     {
//         public AddCustomerNotifyConfigCommand(string appId, string accountId,
//             List<CustomerNotifyConfig> customerNotifyConfigs)
//         {
//             AppId = appId;
//             AccountId = accountId;
//             CustomerNotifyConfigs = customerNotifyConfigs;
//         }
//
//         public string AppId { get; }
//         public string AccountId { get; }
//         public List<CustomerNotifyConfig> CustomerNotifyConfigs { get; }
//     }
//
//     public class AddCustomerNotifyConfigCommandValidator : BaseValidator<AddCustomerNotifyConfigCommand>
//     {
//     }
//
//     public class AddCustomerNotifyConfigCommandHandler : IRequestHandler<AddCustomerNotifyConfigCommand,
//         BaseEntityResponse<List<CustomerNotifyConfig>>>
//     {
//         private readonly IRepository<CustomerNotifyConfig> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public AddCustomerNotifyConfigCommandHandler(IRepository<CustomerNotifyConfig> repository,
//             IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<List<CustomerNotifyConfig>>> Handle(AddCustomerNotifyConfigCommand request,
//             CancellationToken cancellationToken)
//         {
//             var customerNotifyConfigs = await _repository.GetManyAsync(x =>
//                 x.AccountId.Equals(request.AccountId) && x.AppId.Equals(request.AppId));
//             var changeCustomerNotifyConfigs =
//                 request.CustomerNotifyConfigs.Except(customerNotifyConfigs, new CustomerNotifyConfigComparer())
//                     .ToList();
//
//             var newCustomerNotifyConfigs = new List<CustomerNotifyConfig>();
//             if (customerNotifyConfigs.Count > 0)
//             {
//                 var editCustomerNotifyConfigs = changeCustomerNotifyConfigs.Where(item => item.Id != 0).ToList();
//                 if (editCustomerNotifyConfigs.Count > 0)
//                 {
//                     foreach (var item in editCustomerNotifyConfigs)
//                     {
//                         var customerNotifyConfig = await _repository.GetSingleAsync(x => x.Id.Equals(item.Id));
//                         customerNotifyConfig.Update(item.AppId, item.AccountId, item.NotifyConfigCode, item.SendEmail,
//                             item.SendWeb, item.SendMobile,
//                             item.SendDesktop);
//                         _repository.Update(customerNotifyConfig);
//                     }
//
//                     newCustomerNotifyConfigs.AddRange(changeCustomerNotifyConfigs.Where(item => item.Id == 0));
//                 }
//             }
//             else
//             {
//                 newCustomerNotifyConfigs = changeCustomerNotifyConfigs;
//             }
//
//             if (newCustomerNotifyConfigs.Count > 0) _repository.AddRange(newCustomerNotifyConfigs);
//
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var result = await _repository.GetManyAsync(x =>
//                 x.AccountId.Equals(request.AccountId) && x.AppId.Equals(request.AppId));
//             var response = new BaseEntityResponse<List<CustomerNotifyConfig>>
//             {
//                 Data = result.ToList(),
//                 Status = true
//             };
//             return response;
//         }
//     }
// }