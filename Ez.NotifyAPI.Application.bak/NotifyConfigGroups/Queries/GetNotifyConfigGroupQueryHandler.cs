﻿using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigGroups.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.EF.Interfaces;
using Shared.Extensions;

namespace Ez.Notify.Application.NotifyConfigGroups.Queries
{
    public class GetNotifyConfigGroupQuery : IRequest<NotifyConfigGroupDto>
    {
        public GetNotifyConfigGroupQuery(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
    }

    public class
        GetNotifyConfigGroupQueryHandler : IRequestHandler<GetNotifyConfigGroupQuery, NotifyConfigGroupDto>
    {
        private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;

        public GetNotifyConfigGroupQueryHandler(INotifyConfigGroupRepository notifyConfigGroupRepository)
        {
            _notifyConfigGroupRepository = notifyConfigGroupRepository;
        }


        public async Task<NotifyConfigGroupDto> Handle(GetNotifyConfigGroupQuery request,
            CancellationToken cancellationToken)
        {
            var query = await _notifyConfigGroupRepository.GetByIdAsync(request.Id);
            var result = query.Map<NotifyConfigGroupDto>();
            return result;
        }
    }
}