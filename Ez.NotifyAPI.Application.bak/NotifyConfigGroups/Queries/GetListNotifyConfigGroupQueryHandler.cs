﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.Queries;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.NotifyConfigGroups.Queries
// {
//     public class GetListNotifyConfigGroupQuery : ListQuery, IRequest<PagingResponse<IList<NotifyConfigGroupDto>>>
//     {
//         public GetListNotifyConfigGroupQuery(int pageIndex, int pageSize, string query = null, string sorts = null) :
//             base(pageIndex,
//                 pageSize, query, sorts)
//         {
//         }
//     }
//
//     public class
//         GetListNotifyConfigGroupQueryHandler : IRequestHandler<GetListNotifyConfigGroupQuery,
//             PagingResponse<IList<NotifyConfigGroupDto>>>
//     {
//         // private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;
//         private readonly IRepository<NotifyConfigGroup> _notifyConfigGroupRepository;
//
//         public GetListNotifyConfigGroupQueryHandler(IRepository<NotifyConfigGroup> notifyConfigGroupRepository)
//         {
//             _notifyConfigGroupRepository = notifyConfigGroupRepository;
//         }
//
//
//         public async Task<PagingResponse<IList<NotifyConfigGroupDto>>> Handle(GetListNotifyConfigGroupQuery request,
//             CancellationToken cancellationToken)
//         {
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _notifyConfigGroupRepository.QueryAsync(x =>
//                             string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<NotifyConfigGroupDto>>(true,
//                         new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<NotifyConfigGroupDto>>().ToList(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var query = await
//                 _notifyConfigGroupRepository.QueryAsync(x =>
//                         string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                     request.PageIndex, request.PageSize);
//             var response =
//                 new PagingResponse<IList<NotifyConfigGroupDto>>(true,
//                     new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = query.Items.Map<List<NotifyConfigGroupDto>>(),
//                     Total = query.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//     }
// }