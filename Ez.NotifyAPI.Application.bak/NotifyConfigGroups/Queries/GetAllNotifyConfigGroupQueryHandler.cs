﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigGroups.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.NotifyConfigGroups.Queries
{
    public class GetAllNotifyConfigGroupQuery : IRequest<BaseEntityResponse<List<NotifyConfigGroupDto>>>
    {
    }

    public class
        GetAllNotifyConfigGroupQueryHandler : IRequestHandler<GetAllNotifyConfigGroupQuery,
            BaseEntityResponse<List<NotifyConfigGroupDto>>>
    {
        private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;

        public GetAllNotifyConfigGroupQueryHandler(INotifyConfigGroupRepository notifyConfigGroupRepository)
        {
            _notifyConfigGroupRepository = notifyConfigGroupRepository;
        }


        public async Task<BaseEntityResponse<List<NotifyConfigGroupDto>>> Handle(GetAllNotifyConfigGroupQuery request,
            CancellationToken cancellationToken)
        {
            var result = await _notifyConfigGroupRepository.GetAllAsync();
            var response = new BaseEntityResponse<List<NotifyConfigGroupDto>>
            {
                Data = result.ToList(),
                Status = true
            };
            return response;
        }
    }
}