﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigGroups.Queries;
using Microsoft.EntityFrameworkCore;
using Ez.Notify.Application.NotifyConfigs.Queries;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.NotifyConfigGroups.Repositories
{
    public interface INotifyConfigGroupRepository : IRepository<NotifyConfigGroup>
    {
        Task<NotifyConfigGroup> GetByIdAsync(int id);
        Task<IEnumerable<NotifyConfigGroupDto>> GetAllAsync();
    }

    public class NotifyConfigGroupRepository : EfRepository<DataNotifyDbContext, NotifyConfigGroup>,
        INotifyConfigGroupRepository
    {
        public NotifyConfigGroupRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<NotifyConfigGroup> GetByIdAsync(int id)
        {
            return await Queryable.Where(NotifyConfigGroupExpression.Id(id)).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<NotifyConfigGroupDto>> GetAllAsync()
        {
            return await Queryable.Select(NotifyConfigGroupExpression.Model).ToListAsync();
        }
    }
}