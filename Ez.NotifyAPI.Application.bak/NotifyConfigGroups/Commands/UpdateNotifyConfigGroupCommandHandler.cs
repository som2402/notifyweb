﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.NotifyConfigGroups.Commands
// {
//     public class UpdateNotifyConfigGroupCommand : IRequest<BaseEntityResponse<NotifyConfigGroup>>
//     {
//         public UpdateNotifyConfigGroupCommand(int id, string title, int ord, string appId)
//         {
//             Title = title;
//             Ord = ord;
//             AppId = appId;
//             Id = id;
//         }
//
//         public int Id { get; set; }
//         public string Title { get; }
//         public int Ord { get; }
//         public string AppId { get; }
//     }
//
//     public class UpdateNotifyConfigGroupCommandValidator : BaseValidator<UpdateNotifyConfigGroupCommand>
//     {
//         public UpdateNotifyConfigGroupCommandValidator(IRepository<NotifyConfigGroup> notifyConfigGroupRepository)
//         {
//             RuleFor(x => x.Title).NotEmpty().WithMessage("Title is not null");
//             RuleFor(x => x.Id).NotEmpty().WithMessage("Id is not null");
//             RuleFor(x => x.Ord).NotNull().WithMessage("Order is not null");
//             RuleFor(x => x.AppId).NotEmpty().WithMessage("AppId is not null");
//
//             RuleFor(command => command).Custom((command, context) =>
//             {
//                 var notifyConfigGroup = notifyConfigGroupRepository.GetByIdAsync(command.Id).GetAwaiter().GetResult();
//                 if (notifyConfigGroup == null)
//                     context.AddFailure($"{nameof(command.Id)}", $"NotifyConfigGroup#{command.Id} could not be found.");
//             });
//         }
//     }
//
//     public class UpdateNotifyConfigGroupCommandHandler : IRequestHandler<UpdateNotifyConfigGroupCommand,
//         BaseEntityResponse<NotifyConfigGroup>>
//     {
//         private readonly IRepository<NotifyConfigGroup> _notifyConfigGroupRepository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public UpdateNotifyConfigGroupCommandHandler(IUnitOfWork unitOfWork,
//             IRepository<NotifyConfigGroup> notifyConfigGroupRepository)
//         {
//             _unitOfWork = unitOfWork;
//             _notifyConfigGroupRepository = notifyConfigGroupRepository;
//         }
//
//
//         public async Task<BaseEntityResponse<NotifyConfigGroup>> Handle(UpdateNotifyConfigGroupCommand request,
//             CancellationToken cancellationToken)
//         {
//             var entity = await _notifyConfigGroupRepository.GetByIdAsync(request.Id);
//             entity.Update(request.Title, request.Ord, request.AppId);
//             _notifyConfigGroupRepository.Update(entity);
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<NotifyConfigGroup>
//             {
//                 Data = request.Map<NotifyConfigGroup>(),
//                 Status = true
//             };
//             return response;
//         }
//     }
// }