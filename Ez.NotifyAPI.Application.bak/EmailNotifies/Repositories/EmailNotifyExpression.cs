﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;

namespace Ez.Notify.Application.EmailNotifies.Repositories
{
    public static class EmailNotifyExpression
    {
        public static Expression<Func<EmailNotify, bool>> Id(long id) => emailNotify => emailNotify.Id.Equals(id);

        public static Expression<Func<EmailNotify, bool>> CustomerId(string customerId) =>
            emailNotify => emailNotify.AccountId.Equals(customerId);

        public static Expression<Func<EmailNotify, bool>> RefCode(string refCode) =>
            emailNotify => emailNotify.RefCode.Equals(refCode);

        public static Expression<Func<EmailNotify, bool>> RefType(string RefType) =>
            emailNotify => emailNotify.RefCode.Equals(RefType);
    }
}