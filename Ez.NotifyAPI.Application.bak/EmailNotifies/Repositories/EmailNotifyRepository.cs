﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
<<<<<<< HEAD:Ez.NotifyAPI.Application.bak/EmailNotifies/Repositories/EmailNotifyRepository.cs
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
=======
using Notify.Domain;
using Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.Common.Sort;
using Shared.Dto;
>>>>>>> 49309a97b6888bcd4a9deebabbbe949cee6e0ab0:NotifyAPI.Application/EmailNotifies/Repositories/EmailNotifyRepository.cs
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.EmailNotifies.Repositories
{
    public interface IEmailNotifyRepository : IRepository<EmailNotify>
    {
        Task<EmailNotify> GetByIdAsync(long id);
        Task<IEnumerable<EmailNotify>> GetByRefCode(string refCode);
        Task<IEnumerable<EmailNotify>> GetByAccountId(string accountId);
        Task<QueryResult<EmailNotify>> GetListByAccountId(string accountId, string refType, int pageIndex, int pageSize, int? status = null, Sorts sort = null);
        Task<EmailNotify> GetByIdAsync(long id, string accountId);
        Task<IEnumerable<EmailNotify>> GetByIds(string accountId, params long[] emailNotifyIds);
    }

    public class EmailNotifyRepository : EfRepository<DataNotifyDbContext, EmailNotify>, IEmailNotifyRepository
    {
        public EmailNotifyRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<EmailNotify> GetByIdAsync(long id)
        {
            return await Queryable.Where(EmailNotifyExpression.Id(id)).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByRefCode(string refCode)
        {
            return await Queryable.Where(EmailNotifyExpression.RefCode(refCode)).Take(100).OrderBy(x => x.CreatedDate)
                .ToListAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByAccountId(string accountId)
        {
            return await Queryable.Where(EmailNotifyExpression.CustomerId(accountId)).Take(100)
                .OrderBy(x => x.CreatedDate)
                .ToListAsync();
        }

        public async Task<QueryResult<EmailNotify>> GetListByAccountId(string accountId, string refType, int pageIndex, int pageSize, int? status = null, Sorts sort = null)
        {
            var data = Queryable.Where(x=> x.AccountId == accountId
                        && (
                              (status == null && x.Status != Common.Constants.EmailNotifyStatus.Deleted) ||
                              (status == Common.Constants.EmailNotifyStatus.Deleted && x.Status == Common.Constants.EmailNotifyStatus.Deleted) ||
                              (status == Common.Constants.EmailNotifyStatus.UnRead && (x.Status == Common.Constants.EmailNotifyStatus.UnRead || x.Status == null )) ||
                              (x.Status == status)
                           )
                        && (string.IsNullOrWhiteSpace(refType) || x.RefType == refType)
                    );
            var res = await data.ToQueryResultAsync(pageIndex, pageSize, sort);
            return res;
        }

        public async Task<EmailNotify> GetByIdAsync(long id, string accountId)
        {
            return await Queryable.Where(x => x.Id == id && x.AccountId == accountId).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByIds(string accountId, params long[] emailNotifyIds)
        {
            return await Queryable.Where(x=> x.AccountId == accountId && emailNotifyIds.Contains(x.Id))
                .ToListAsync();
        }
    }
}