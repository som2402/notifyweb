﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.EmailNotifies.Command
// {
//     public class DeleteEmailNotifyCommand : IRequest<BaseEntityResponse<long>>
//     {
//         public DeleteEmailNotifyCommand(long id)
//         {
//             Id = id;
//         }
//
//         public long Id { get; }
//     }
//
//     public class DeleteEmailNotifyCommandValidator : BaseValidator<DeleteEmailNotifyCommand>
//     {
//         public DeleteEmailNotifyCommandValidator(IRepository<EmailNotify> repository)
//         {
//             RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
//             {
//                 var notifyConfig = await repository.GetByIdAsync(x);
//                 return notifyConfig != null;
//             }).WithErrorCode("ID_NOT_FOUND");
//         }
//     }
//
//     public class DeleteEmailNotifyCommandHandler : IRequestHandler<DeleteEmailNotifyCommand, BaseEntityResponse<long>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public DeleteEmailNotifyCommandHandler(IRepository<EmailNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<long>> Handle(DeleteEmailNotifyCommand request,
//             CancellationToken cancellationToken)
//         {
//             var entity = _repository.Delete(request.Id);
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<long>().SetData(entity);
//             return response;
//         }
//     }
// }