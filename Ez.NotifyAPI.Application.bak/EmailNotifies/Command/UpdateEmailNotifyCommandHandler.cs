﻿// using System;
// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.EmailNotifies.Command
// {
//     public class UpdateEmailNotifyCommand : IRequest<BaseEntityResponse<EmailNotify>>
//     {
//         public UpdateEmailNotifyCommand(long id, string subject, string textBody, string htmlBody, string to, string cc,
//             string bcc, string sender, string senderTitle, string senderCode, string refCode, string refType,
//             string customerId, DateTime? createdDate, string createdBy, string sendMessage, DateTime? sentDate,
//             int? sentCount)
//         {
//             Id = id;
//             Subject = subject;
//             TextBody = textBody;
//             HtmlBody = htmlBody;
//             To = to;
//             Cc = cc;
//             Bcc = bcc;
//             Sender = sender;
//             SenderTitle = senderTitle;
//             SenderCode = senderCode;
//             RefCode = refCode;
//             RefType = refType;
//             CustomerId = customerId;
//             CreatedDate = createdDate;
//             CreatedBy = createdBy;
//             SendMessage = sendMessage;
//             SentDate = sentDate;
//             SentCount = sentCount;
//         }
//
//         public long Id { get; }
//         public string Subject { get; }
//         public string TextBody { get; }
//         public string HtmlBody { get; }
//         public string To { get; }
//         public string Cc { get; }
//         public string Bcc { get; }
//         public string Sender { get; }
//         public string SenderTitle { get; }
//         public string SenderCode { get; }
//         public string RefCode { get; }
//         public string RefType { get; }
//         public string CustomerId { get; }
//         public DateTime? CreatedDate { get; }
//         public string CreatedBy { get; }
//         public string SendMessage { get; }
//         public DateTime? SentDate { get; }
//         public int? SentCount { get; }
//     }
//
//     public class UpdateEmailNotifyCommandValidator : BaseValidator<UpdateEmailNotifyCommand>
//     {
//         public UpdateEmailNotifyCommandValidator(IRepository<EmailNotify> repository)
//         {
//             RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
//             {
//                 var notifyConfig = await repository.GetByIdAsync(x);
//                 return notifyConfig != null;
//             }).WithErrorCode("ID_NOT_FOUND");
//         }
//     }
//
//     public class
//         UpdateEmailNotifyCommandHandler : IRequestHandler<UpdateEmailNotifyCommand, BaseEntityResponse<EmailNotify>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public UpdateEmailNotifyCommandHandler(IRepository<EmailNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<EmailNotify>> Handle(UpdateEmailNotifyCommand request,
//             CancellationToken cancellationToken)
//         {
//             var emailnotify = await _repository.GetByIdAsync(request.Id);
//             emailnotify.Update(request.Subject, request.TextBody, request.HtmlBody, request.To, request.Cc, request.Bcc,
//                 request.Sender, request.SenderTitle, request.SenderCode, request.RefCode, request.RefType,
//                 request.CreatedDate, request.CreatedBy, request.SendMessage, request.SentDate,
//                 request.SentCount);
//             var data = _repository.Update(emailnotify);
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<EmailNotify>().SetData(data);
//             return response;
//         }
//     }
// }