<<<<<<< HEAD:Ez.NotifyAPI.Application.bak/EmailNotifies/Command/UpdateEmailNotifyStatusCommandHandler.cs
﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.EmailNotifies.Command
// {
//     public class UpdateEmailNotifyStatusCommand : IRequest<BaseEntityResponse<EmailNotify>>
//     {
//         public UpdateEmailNotifyStatusCommand(long id, int? emailStatus)
//         {
//             Id = id;
//             EmailStatus = emailStatus;
//         }
//
//         public long Id { get; }
//         public int? EmailStatus { get; }
//     }
//
//     public class UpdateEmailNotifyStatusCommandValidator : BaseValidator<UpdateEmailNotifyStatusCommand>
//     {
//         public UpdateEmailNotifyStatusCommandValidator(IRepository<EmailNotify> repository)
//         {
//             RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
//             {
//                 var notifyConfig = await repository.GetByIdAsync(x);
//                 return notifyConfig != null;
//             }).WithErrorCode("ID_NOT_FOUND");
//         }
//     }
//
//     public class
//         UpdateEmailNotifyStatusCommandHandler : IRequestHandler<UpdateEmailNotifyStatusCommand,
//             BaseEntityResponse<EmailNotify>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public UpdateEmailNotifyStatusCommandHandler(IRepository<EmailNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<EmailNotify>> Handle(UpdateEmailNotifyStatusCommand request,
//             CancellationToken cancellationToken)
//         {
//             var emailnotify = await _repository.GetByIdAsync(request.Id);
//             emailnotify.UpdateStatus(request.EmailStatus);
//             var data = _repository.Update(emailnotify);
//             var response = new BaseEntityResponse<EmailNotify>().SetData(data);
//             return response;
//         }
//     }
// }
=======
﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Notify.Application.EmailNotifies.Repositories;
using Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Validators;

namespace Notify.Application.EmailNotifies.Command
{
    public class UpdateEmailNotifyStatusCommand : IRequest<BaseResponse>
    {
        public UpdateEmailNotifyStatusCommand(int? status, string accountId, params long[] emailNotifyIds)
        {
            Status = status;
            EmailNotifyIds = emailNotifyIds;
            AccountId = accountId;
        }
        public string AccountId { get; set; }
        public int? Status { get; }
        public long[] EmailNotifyIds { get; }
    }

    public class UpdateEmailNotifyStatusCommandValidator : BaseValidator<UpdateEmailNotifyStatusCommand>
    {
        public UpdateEmailNotifyStatusCommandValidator(IEmailNotifyRepository repository)
        {
            RuleFor(x => x).MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfigs = await repository.GetByIds(x.AccountId, x.EmailNotifyIds);
                return notifyConfigs?.Count() > 0;
            }).WithErrorCode("ID_NOT_FOUND");
        }
    }

    public class UpdateEmailNotifyStatusCommandHandler : IRequestHandler<UpdateEmailNotifyStatusCommand, BaseResponse>
    {
        private readonly IEmailNotifyRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmailNotifyStatusCommandHandler(IEmailNotifyRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseResponse> Handle(UpdateEmailNotifyStatusCommand request,
            CancellationToken cancellationToken)
        {
            var emailNotifies = await _repository.GetByIds(request.AccountId, request.EmailNotifyIds);
            var emailNotifiesUpdate = emailNotifies.Select(x => {
                x.Status = request.Status;
                return x;
            }).ToList();

            await _repository.UpdateRangeAsync(emailNotifiesUpdate);
            await _unitOfWork.CommitAsync(cancellationToken);
            var response = new BaseResponse(true, null);
            return response;
        }
    }
}
>>>>>>> 49309a97b6888bcd4a9deebabbbe949cee6e0ab0:NotifyAPI.Application/EmailNotifies/Command/UpdateEmailNotifyStatusCommandHandler.cs
