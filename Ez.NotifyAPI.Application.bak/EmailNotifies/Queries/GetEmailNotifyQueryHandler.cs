<<<<<<< HEAD:Ez.NotifyAPI.Application.bak/EmailNotifies/Queries/GetEmailNotifyQueryHandler.cs
﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.EmailNotifies.Queries
// {
//     public class GetEmailNotifyQuery : IRequest<BaseEntityResponse<EmailNotifyDto>>
//     {
//         public GetEmailNotifyQuery(long id)
//         {
//             Id = id;
//         }
//
//         public long Id { get; set; }
//     }
//
//     public class GetEmailNotifyQueryValidator : BaseValidator<GetEmailNotifyQuery>
//     {
//         public GetEmailNotifyQueryValidator(IRepository<EmailNotify> repository)
//         {
//             RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
//             {
//                 var notifyConfig = await repository.GetByIdAsync(x);
//                 return notifyConfig != null;
//             }).WithErrorCode("ID_NOT_FOUND");
//         }
//     }
//
//     public class
//         GetEmailNotifyQueryHandler : IRequestHandler<GetEmailNotifyQuery, BaseEntityResponse<EmailNotifyDto>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//
//         public GetEmailNotifyQueryHandler(IRepository<EmailNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<EmailNotifyDto>> Handle(GetEmailNotifyQuery request,
//             CancellationToken cancellationToken)
//         {
//             var entity = await _repository.GetByIdAsync(request.Id);
//             var response = new BaseEntityResponse<EmailNotifyDto>().SetData(entity.Map<EmailNotifyDto>());
//             return response;
//         }
//     }
// }
=======
﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Notify.Application.EmailNotifies.Repositories;
using Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Validators;

namespace Notify.Application.EmailNotifies.Queries
{
    public class GetEmailNotifyQuery : IRequest<BaseEntityResponse<EmailNotifyDto>>
    {
        public GetEmailNotifyQuery(long id, string accountId)
        {
            Id = id;
            AccountId = accountId;
        }

        public long Id { get; set; }
        public string AccountId { get; set; }
    }

    public class GetEmailNotifyQueryValidator : BaseValidator<GetEmailNotifyQuery>
    {
        public GetEmailNotifyQueryValidator(IEmailNotifyRepository repository)
        {
            RuleFor(x => x).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfig = await repository.GetByIdAsync(x.Id, x.AccountId);
                return notifyConfig != null;
            }).WithErrorCode("EMAIL_NOTIFY_NOT_FOUND");
        }
    }

    public class GetEmailNotifyQueryHandler : IRequestHandler<GetEmailNotifyQuery, BaseEntityResponse<EmailNotifyDto>>
    {
        private readonly IEmailNotifyRepository _repository;

        public GetEmailNotifyQueryHandler(IEmailNotifyRepository repository)
        {
            _repository = repository;
        }

        public async Task<BaseEntityResponse<EmailNotifyDto>> Handle(GetEmailNotifyQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _repository.GetByIdAsync(request.Id, request.AccountId);
            var response = new BaseEntityResponse<EmailNotifyDto>().SetData(entity.Map<EmailNotifyDto>());
            return response;
        }
    }
}
>>>>>>> 49309a97b6888bcd4a9deebabbbe949cee6e0ab0:NotifyAPI.Application/EmailNotifies/Queries/GetEmailNotifyQueryHandler.cs
