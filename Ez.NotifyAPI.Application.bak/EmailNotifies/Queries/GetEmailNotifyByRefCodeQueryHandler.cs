﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.EmailNotifies.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;

namespace Ez.Notify.Application.EmailNotifies.Queries
{
    public class GetEmailNotifyByRefCodeQuery : IRequest<BaseEntityResponse<List<EmailNotifyDto>>>
    {
        public GetEmailNotifyByRefCodeQuery(string refCode)
        {
            RefCode = refCode;
        }

        public string RefCode { get; set; }
    }

    public class GetEmailNotifyByRefCodeQueryHandler : IRequestHandler<GetEmailNotifyByRefCodeQuery,
        BaseEntityResponse<List<EmailNotifyDto>>>
    {
        private readonly IEmailNotifyRepository _emailNotifyRepository;

        public GetEmailNotifyByRefCodeQueryHandler(IEmailNotifyRepository emailNotifyRepository)
        {
            _emailNotifyRepository = emailNotifyRepository;
        }


        public async Task<BaseEntityResponse<List<EmailNotifyDto>>> Handle(GetEmailNotifyByRefCodeQuery request,
            CancellationToken cancellationToken)
        {
            var entities = await _emailNotifyRepository.GetByRefCode(request.RefCode);
            var result = entities.Map<List<EmailNotifyDto>>();
            foreach (var item in result.Where(item => item.CreatedDate != null))
                if (item.CreatedDate != null)
                    item.CreatedDate = item.CreatedDate.Value.ConvertGmt7();

            var response = new BaseEntityResponse<List<EmailNotifyDto>> {Data = result, Status = true};
            return response;
        }
    }
}