<<<<<<< HEAD:Ez.NotifyAPI.Application.bak/EmailNotifies/Queries/GetListEmailNotifyQueryHandler.cs
﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.Queries;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.EmailNotifies.Queries
// {
//     public class GetListEmailNotifyQuery : ListQuery, IRequest<PagingResponse<IList<EmailNotifyDto>>>
//     {
//         public GetListEmailNotifyQuery(int pageIndex, int pageSize, string query, string sorts) : base(pageIndex,
//             pageSize,
//             query, sorts)
//         {
//         }
//     }
//
//     public class GetListEmailNotifyQueryHandler : IRequestHandler<GetListEmailNotifyQuery,
//         PagingResponse<IList<EmailNotifyDto>>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//
//
//         public GetListEmailNotifyQueryHandler(IRepository<EmailNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<PagingResponse<IList<EmailNotifyDto>>> Handle(GetListEmailNotifyQuery request,
//             CancellationToken cancellationToken)
//         {
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _repository.QueryAsync(x =>
//                             string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Subject, $"%{request.Query}%"),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<EmailNotifyDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<EmailNotifyDto>>(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var query = await _repository.QueryAsync(x =>
//                     string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Subject, $"%{request.Query}%"),
//                 request.PageIndex, request.PageSize);
//             var response =
//                 new PagingResponse<IList<EmailNotifyDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = query.Items.Map<IList<EmailNotifyDto>>().ToList(),
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//     }
// }
=======
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Notify.Application.EmailNotifies.Repositories;
using Shared.BaseModel;
using Shared.Common.Sort;
using Shared.Extensions;

namespace Notify.Application.EmailNotifies.Queries
{
    public class GetListEmailNotifyQuery : IRequest<PagingResponse<IList<EmailNotifyListDto>>>
    {
        public GetListEmailNotifyQuery(int pageIndex, int pageSize, string accountId, string refType, int? status = null, Sorts sorts = null)
        {
            AccountId = accountId;
            RefType = refType;
            PageIndex = pageIndex;
            PageSize = pageSize;
            Sorts = sorts;
            Status = status;
        }

        public string AccountId { get; }
        public string RefType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int? Status { get; set; }
        public Sorts Sorts { get; set; }
    }

    public class GetListEmailNotifyQueryHandler : IRequestHandler<GetListEmailNotifyQuery,
        PagingResponse<IList<EmailNotifyListDto>>>
    {
        private readonly IEmailNotifyRepository repository;

        public GetListEmailNotifyQueryHandler(IEmailNotifyRepository repository)
        {
            this.repository = repository;
        }

        public async Task<PagingResponse<IList<EmailNotifyListDto>>> Handle(GetListEmailNotifyQuery request,
            CancellationToken cancellationToken)
        {
            //if (!string.IsNullOrEmpty(request.Sorts))
            //{
            //    var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
            //    var sorts = new Sorts(sortConditions);
            //    var querySortResult = await repository.GetListByAccountId(request.AccountId, request.PageIndex, request.PageSize, sorts);

            //    var sortResponse =
            //        new PagingResponse<IList<EmailNotifyDto>>(true, new List<DetailError> { new DetailError(null, null) })
            //        {
            //            Data = querySortResult.Items.Map<List<EmailNotifyDto>>(),
            //            Total = querySortResult.Count,
            //            PageIndex = request.PageIndex,
            //            PageSize = request.PageSize
            //        };
            //    return sortResponse;
            //}

            var query = await repository.GetListByAccountId(request.AccountId, request.RefType, request.PageIndex, request.PageSize, request.Status);
            var response =
                new PagingResponse<IList<EmailNotifyListDto>>(true, new List<DetailError> { new DetailError(null, null) })
                {
                    Data = query.Items.Map<IList<EmailNotifyListDto>>().ToList(),
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    Total = query.Count
                };
            return response;
        }
    }
}
>>>>>>> 49309a97b6888bcd4a9deebabbbe949cee6e0ab0:NotifyAPI.Application/EmailNotifies/Queries/GetListEmailNotifyQueryHandler.cs
