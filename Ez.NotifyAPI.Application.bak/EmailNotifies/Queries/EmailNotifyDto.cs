﻿using System;
using System.Collections.Generic;
using Ez.Notify.Application.EmailAttachments.Queries;

namespace Ez.Notify.Application.EmailNotifies.Queries
{
    public class EmailNotifyDto
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string TextBody { get; set; }
        public string HtmlBody { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Sender { get; set; }
        public string SenderTitle { get; set; }
        public string SenderCode { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int? SendStatus { get; set; }
        public string SendMessage { get; set; }
        public DateTime? SentDate { get; set; }
        public int? SentFailCount { get; set; }
        public List<EmailAttachmentDto> EmailAttachment { get; set; }
        public int? Zone { get; set; }
        public string AccountId { get; set; }
        public int? Status { get; set; }
    }
}