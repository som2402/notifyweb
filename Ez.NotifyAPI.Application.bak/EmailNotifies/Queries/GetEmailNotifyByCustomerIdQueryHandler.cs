﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.EmailNotifies.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;

namespace Ez.Notify.Application.EmailNotifies.Queries
{
    public class GetEmailNotifyByCustomerIdQuery : IRequest<BaseEntityResponse<List<EmailNotifyDto>>>
    {
        public GetEmailNotifyByCustomerIdQuery(string customerId)
        {
            CustomerId = customerId;
        }

        public string CustomerId { get; }
    }

    public class GetEmailNotifyByCustomerIdQueryHandler : IRequestHandler<GetEmailNotifyByCustomerIdQuery,
        BaseEntityResponse<List<EmailNotifyDto>>>
    {
        private readonly IEmailNotifyRepository _emailNotifyRepository;

        public GetEmailNotifyByCustomerIdQueryHandler(IEmailNotifyRepository emailNotifyRepository)
        {
            _emailNotifyRepository = emailNotifyRepository;
        }


        public async Task<BaseEntityResponse<List<EmailNotifyDto>>> Handle(GetEmailNotifyByCustomerIdQuery request,
            CancellationToken cancellationToken)
        {
            var data = await _emailNotifyRepository.GetByAccountId(request.CustomerId);
            var result = data.Map<List<EmailNotifyDto>>();
            foreach (var item in result.Where(item => item.CreatedDate != null))
                if (item.CreatedDate != null)
                    item.CreatedDate = item.CreatedDate.Value.ConvertGmt7();

            var response = new BaseEntityResponse<List<EmailNotifyDto>>
            {
                Data = result,
                Status = true
            };
            return response;
        }
    }
}