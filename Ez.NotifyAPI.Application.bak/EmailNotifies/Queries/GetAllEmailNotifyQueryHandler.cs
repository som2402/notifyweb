﻿// using System.Collections.Generic;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.EmailNotifies.Queries
// {
//     public class GetAllEmailNotifyQuery : IRequest<BaseEntityResponse<IList<EmailNotifyDto>>>
//     {
//     }
//
//     public class GetAllEmailNotifyQueryHandler : IRequestHandler<GetAllEmailNotifyQuery,
//         BaseEntityResponse<IList<EmailNotifyDto>>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//
//         public GetAllEmailNotifyQueryHandler(IRepository<EmailNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<IList<EmailNotifyDto>>> Handle(GetAllEmailNotifyQuery request,
//             CancellationToken cancellationToken)
//         {
//             var listEntity = await _repository.GetAllAsync();
//             var response =
//                 new BaseEntityResponse<IList<EmailNotifyDto>>().SetData(
//                     listEntity.Map<IList<EmailNotifyDto>>());
//             return response;
//         }
//     }
// }