﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Newtonsoft.Json.Linq;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.Common.JTable;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.EmailNotifies.Queries
// {
//     public class GetJTableListEmailNotifyQuery : JTableModel, IRequest<JObject>
//     {
//         public string Query { get; set; }
//     }
//
//     public class GetJTableListEmailNotifyQueryHandler : IRequestHandler<GetJTableListEmailNotifyQuery, JObject>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//
//         public GetJTableListEmailNotifyQueryHandler(IRepository<EmailNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<JObject> Handle(GetJTableListEmailNotifyQuery request, CancellationToken cancellationToken)
//         {
//             var result = await
//                 _repository.QueryAsync(x =>
//                         string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Subject, $"%{request.Query}%"),
//                     request.PageIndex, request.PageSize, request.Sorts);
//             var response =
//                 new PagingResponse<IList<EmailNotifyDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = result.Items.Map<List<EmailNotifyDto>>(),
//                     Total = result.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             var responseJTable = JTableHelper.JObjectData(response.Data.ToList(),
//                 request.Draw,
//                 (int) response.Total);
//             return responseJTable;
//         }
//     }
// }