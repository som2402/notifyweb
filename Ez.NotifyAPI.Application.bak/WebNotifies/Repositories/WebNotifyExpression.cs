﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;

namespace Ez.Notify.Application.WebNotifies.Repositories
{
    public static class WebNotifyExpression
    {
        public static Expression<Func<WebNotify, bool>> CustomerId(string customerId) =>
            webNotify => webNotify.CustomerId.Equals(customerId);
    }
}