﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.WebNotifies.Repositories;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Validators;

namespace Ez.Notify.Application.WebNotifies.Commands
{
    public class AddWebNotifyCommand : IRequest<BaseEntityResponse<List<WebNotify>>>
    {
        public AddWebNotifyCommand(string title, string body, string link, string group, string createdBy,
            string customerId, string refCode, string refType)
        {
            Title = title;
            Body = body;
            Link = link;
            Group = group;
            CreatedBy = createdBy;
            CustomerId = customerId;
            RefCode = refCode;
            RefType = refType;
        }

        public string Title { get; }
        public string Body { get; }
        public string Link { get; }
        public string Group { get; }
        public string CreatedBy { get; }
        public string CustomerId { get; }
        public string RefCode { get; }
        public string RefType { get; }
    }

    public class AddWebNotifyValidator : BaseValidator<AddWebNotifyCommand>
    {
    }

    public class AddWebNotifyCommandHandler : IRequestHandler<AddWebNotifyCommand, BaseEntityResponse<List<WebNotify>>>
    {
        private readonly IWebNotifyRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public AddWebNotifyCommandHandler(IWebNotifyRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }


        public async Task<BaseEntityResponse<List<WebNotify>>> Handle(AddWebNotifyCommand request,
            CancellationToken cancellationToken)
        {
            await _repository.AddAsync(request.Map<WebNotify>());
            await _unitOfWork.CommitAsync(cancellationToken);
            var webNotify = await _repository.GetByCustomerId(request.CustomerId);
            var response = new BaseEntityResponse<List<WebNotify>>
            {
                Data = webNotify.ToList(),
                Status = true
            };
            return response;
        }
    }
}