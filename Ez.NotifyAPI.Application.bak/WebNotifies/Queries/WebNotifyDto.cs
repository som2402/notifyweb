﻿using System;

namespace Ez.Notify.Application.WebNotifies.Queries
{
    public class WebNotifyDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Link { get; set; }
        public string Group { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerId { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
        public bool? Read { get; set; }
        public Guid? Uid { get; set; }
    }
}