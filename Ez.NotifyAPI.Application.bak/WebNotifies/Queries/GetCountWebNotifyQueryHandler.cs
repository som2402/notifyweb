﻿// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
//
// namespace Ez.Notify.Application.WebNotifies.Queries
// {
//     public class GetCountWebNotifyQuery : IRequest<BaseEntityResponse<int>>
//     {
//         public GetCountWebNotifyQuery(string customerId, bool? read, string group)
//         {
//             CustomerId = customerId;
//             Read = read;
//             Group = group;
//         }
//
//         public string Group { get; }
//         public string CustomerId { get; }
//         public bool? Read { get; }
//     }
//
//     public class GetCountWebNotifyQueryHandler : IRequestHandler<GetCountWebNotifyQuery, BaseEntityResponse<int>>
//     {
//         private readonly IRepository<WebNotify> _repository;
//
//         public GetCountWebNotifyQueryHandler(IRepository<WebNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<int>> Handle(GetCountWebNotifyQuery request,
//             CancellationToken cancellationToken)
//         {
//             var query = _repository.GetDbSet().AsQueryable();
//             if (!string.IsNullOrEmpty(request.Group)) query = query.Where(x => x.Group.Equals(request.Group));
//
//             if (!string.IsNullOrEmpty(request.CustomerId))
//                 query = query.Where(x => x.CustomerId.Equals(request.CustomerId));
//
//             if (request.Read != null)
//                 query = query.Where(x =>
//                     request.Read == false && (x.Read == null || x.Read == false) ||
//                     request.Read == true && x.Read == true);
//
//             var response = new BaseEntityResponse<int>
//             {
//                 Data = await query.CountAsync(cancellationToken),
//                 Status = true
//             };
//             return response;
//         }
//     }
// }