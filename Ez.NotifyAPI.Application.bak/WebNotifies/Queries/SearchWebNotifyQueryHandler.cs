﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.WebNotifies.Queries
// {
//     public class SearchWebNotifyQuery : IRequest<BaseEntityResponse<List<WebNotifyDto>>>
//     {
//         public SearchWebNotifyQuery(string customerId, string group, int top, bool? read)
//         {
//             CustomerId = customerId;
//             Group = group;
//             Top = top;
//             Read = read;
//         }
//
//         public string CustomerId { get; }
//         public string Group { get; }
//         public int Top { get; }
//         public bool? Read { get; }
//     }
//
//     public class
//         SearchWebNotifyQueryHandler : IRequestHandler<SearchWebNotifyQuery, BaseEntityResponse<List<WebNotifyDto>>>
//     {
//         private readonly IRepository<WebNotify> _repository;
//
//         public SearchWebNotifyQueryHandler(IRepository<WebNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<List<WebNotifyDto>>> Handle(SearchWebNotifyQuery request,
//             CancellationToken cancellationToken)
//         {
//             var query = _repository.GetDbSet().AsQueryable();
//             if (!string.IsNullOrEmpty(request.Group)) query = query.Where(x => x.Group.Equals(request.Group));
//
//             if (!string.IsNullOrEmpty(request.CustomerId))
//                 query = query.Where(x => x.CustomerId.Equals(request.CustomerId));
//
//             if (request.Read != null)
//                 query = query.Where(x =>
//                     request.Read == false && (x.Read == null || x.Read == false) ||
//                     request.Read == true && x.Read == true);
//
//             var result = await query.OrderByDescending(g => g.CreatedDate).Take(request.Top)
//                 .ToListAsync(cancellationToken);
//             var response = new BaseEntityResponse<List<WebNotifyDto>>
//             {
//                 Data = result.Map<List<WebNotifyDto>>(),
//                 Status = true
//             };
//             return response;
//         }
//     }
// }