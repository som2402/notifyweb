﻿using System;
using System.Collections.Generic;

namespace Ez.Notify.Application.MessageFirebase.Queries
{
    public class MessageFirebaseDto
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string AccountId { get; set; }
        //public IReadOnlyList<string> DeviceIds { get; set; }
    }
}