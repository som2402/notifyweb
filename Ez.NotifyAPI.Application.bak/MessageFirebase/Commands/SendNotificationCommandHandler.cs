﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FirebaseAdmin.Messaging;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using Ez.Notify.Customer.Driver;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Validators;

namespace Ez.Notify.Application.MessageFirebase.Commands
{
    public class SendNotificationCommand : IRequest<BaseResponse>
    {
        public SendNotificationCommand(string title, string body, string urlAction, string accountId)
        {
            Title = title;
            Body = body;
            AccountId = accountId;
            UrlAction = urlAction;
        }

        public string Title { get; set; }
        public string Body { get; set; }
        public string UrlAction { get; set; }
        public string AccountId { get; set; }
    }

    public class SendNotificationCommandValidator : BaseValidator<SendNotificationCommand>
    {
        public SendNotificationCommandValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithErrorCode("TITLE_NOT_ALLOW_NULL");

            RuleFor(x => x.Body)
                 .NotEmpty()
                 .WithErrorCode("BODY_NOT_ALLOW_NULL");
        }
    }

    public class SendNotificationCommandHandler : IRequestHandler<SendNotificationCommand, BaseResponse>
    {

        private readonly CustomerClient _apiCustomerClient;

        public SendNotificationCommandHandler(CustomerClient apiCustomerClient)
        {
            _apiCustomerClient = apiCustomerClient;
        }

        public async Task<BaseResponse> Handle(SendNotificationCommand request,
            CancellationToken cancellationToken)
        {
            var response = new BaseResponse();
            
            var custommer = await _apiCustomerClient.GetCustomerByAccountId(request.AccountId);
            if (string.IsNullOrWhiteSpace(custommer.DeviceToken))
            {
                response.Status = false;
                return response;
            }
            var deviceIds = JsonConvert.DeserializeObject<IReadOnlyList<string>>(custommer.DeviceToken);
            var model = new MulticastMessage()
            {
                Notification = new Notification()
                {
                    Title = request.Title,
                    Body = request.Body
                },
                Tokens = deviceIds,
                Webpush = new WebpushConfig()
                {
                    FcmOptions = new WebpushFcmOptions()
                    {
                        Link = request.UrlAction
                    }
                }
            };
            var result = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(model);
            response.Successful();
            return response;
        }
    }
}