﻿using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Shared.BaseModel;
using Shared.Extensions;
using Shared.Validators;

namespace Ez.Notify.Application.NotifyConfigs.Queries
{
    public class GetNotifyConfigQuery : IRequest<BaseEntityResponse<NotifyConfigDto>>
    {
        public GetNotifyConfigQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }

    public class GetNotifyConfigQueryValidator : BaseValidator<GetNotifyConfigQuery>
    {
        public GetNotifyConfigQueryValidator(INotifyConfigRepository notifyConfigRepository)
        {
            RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfig = await notifyConfigRepository.GetNotifyConfigById(x);
                return notifyConfig != null;
            }).WithErrorCode("ID_NOT_FOUND");
        }
    }

    public class
        GetNotifyConfigQueryHandler : IRequestHandler<GetNotifyConfigQuery, BaseEntityResponse<NotifyConfigDto>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;

        public GetNotifyConfigQueryHandler(INotifyConfigRepository notifyConfigRepository)
        {
            _notifyConfigRepository = notifyConfigRepository;
        }


        public async Task<BaseEntityResponse<NotifyConfigDto>> Handle(GetNotifyConfigQuery request,
            CancellationToken cancellationToken)
        {
            var query = await _notifyConfigRepository.GetNotifyConfigById(request.Id);
            var response = new BaseEntityResponse<NotifyConfigDto>
            {
                Data = query.Map<NotifyConfigDto>(),
                Status = true
            };
            return response;
        }
    }
}