﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigs.Repositories;
using MediatR;
using Shared.BaseModel;

namespace Ez.Notify.Application.NotifyConfigs.Queries
{
    public class GetAllNotifyConfigQuery : IRequest<BaseEntityResponse<IEnumerable<NotifyConfigDto>>>
    {
    }

    public class GetAllNotifyConfigQueryHandler : IRequestHandler<GetAllNotifyConfigQuery,
        BaseEntityResponse<IEnumerable<NotifyConfigDto>>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;

        public GetAllNotifyConfigQueryHandler(INotifyConfigRepository notifyConfigRepository)
        {
            _notifyConfigRepository = notifyConfigRepository;
        }


        public async Task<BaseEntityResponse<IEnumerable<NotifyConfigDto>>> Handle(GetAllNotifyConfigQuery request,
            CancellationToken cancellationToken)
        {
            var result = await _notifyConfigRepository.GetAllNotifyConfig();
            var response = new BaseEntityResponse<IEnumerable<NotifyConfigDto>>
            {
                Status = true,
                Data = result
            };
            return response;
        }
    }
}