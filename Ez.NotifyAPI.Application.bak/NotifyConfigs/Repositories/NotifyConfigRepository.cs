﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigs.Queries;
using Microsoft.EntityFrameworkCore;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel.Grids;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Extensions;

namespace Ez.Notify.Application.NotifyConfigs.Repositories
{
    public interface INotifyConfigRepository : IRepository<NotifyConfig>
    {
        Task<Grid<NotifyConfigDto>> GridAsync(GridParameters parameters);
        Task<IEnumerable<NotifyConfigDto>> GetAllNotifyConfig();
        Task<NotifyConfig> GetNotifyConfigById(int id);
        Task<NotifyConfig> GetNotifyConfigByCode(string code);
    }

    public class NotifyConfigRepository : EfRepository<DataNotifyDbContext, NotifyConfig>, INotifyConfigRepository
    {
        public NotifyConfigRepository(DataNotifyDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Grid<NotifyConfigDto>> GridAsync(GridParameters parameters)
        {
            return await Queryable.Select(NotifyConfigExpression.Model).GridAsync(parameters);
        }

        public async Task<IEnumerable<NotifyConfigDto>> GetAllNotifyConfig()
        {
            return await Queryable.Select(NotifyConfigExpression.Model).ToListAsync();
        }


        public async Task<NotifyConfig> GetNotifyConfigById(int id)
        {
            return await Queryable.Where(NotifyConfigExpression.Id(id))
                .SingleOrDefaultAsync();
        }

        public async Task<NotifyConfig> GetNotifyConfigByCode(string code)
        {
            return await Queryable.Where(NotifyConfigExpression.Code(code))
                .SingleOrDefaultAsync();
        }
    }
}