﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Validators;

namespace Ez.Notify.Application.NotifyConfigs.Commands
{
    public class AddNotifyConfigCommand : IRequest<BaseEntityResponse<NotifyConfig>>
    {
        public AddNotifyConfigCommand(string code, string name, int groupId, string description, bool active,
            int? required, int? desktopRequired, int? mobileRequired, int? webRequired, int ord)
        {
            Code = code;
            Name = name;
            GroupId = groupId;
            Description = description;
            Active = active;
            Required = required;
            DesktopRequired = desktopRequired;
            MobileRequired = mobileRequired;
            WebRequired = webRequired;
            Ord = ord;
        }

        public string Code { get; }
        public string Name { get; }
        public int GroupId { get; }
        public string Description { get; }
        public bool Active { get; }
        public int? Required { get; }
        public int? WebRequired { get; set; }
        public int? MobileRequired { get; set; }
        public int? DesktopRequired { get; set; }
        public int Ord { get; set; }
    }

    public class AddNotifyConfigCommandValidtor : BaseValidator<AddNotifyConfigCommand>
    {
        public AddNotifyConfigCommandValidtor(INotifyConfigRepository notifyConfigGroupRepository)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithErrorCode("NAME_NOT_ALLOW_NULL")
                .MaximumLength(100)
                .WithErrorCode("NAME_LENGTH_OVER_LIMIT")
                .WithState(_ => new Dictionary<string, string> {{"NAME_MAX_LENGTH", "100"}});
            RuleFor(x => x.GroupId)
                .NotNull()
                .WithErrorCode("GROUP_ID_NOT_ALLOW_NULL")
                .GreaterThan(0)
                .WithErrorCode("GROUP_ID_VALUE_INVALID")
                .WithState(_ => new Dictionary<string, string> {{"ORD_MIN_VALUE", "1"}}).Must(x =>
                    notifyConfigGroupRepository.GetNotifyConfigById(x).GetAwaiter().GetResult() != null)
                .WithErrorCode("GROUP_ID_NOT_FOUND");
        }
    }

    public class
        AddNotifyConfigCommandHandler : IRequestHandler<AddNotifyConfigCommand, BaseEntityResponse<NotifyConfig>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddNotifyConfigCommandHandler(INotifyConfigRepository notifyConfigRepository, IUnitOfWork unitOfWork)
        {
            _notifyConfigRepository = notifyConfigRepository;
            _unitOfWork = unitOfWork;
        }


        public async Task<BaseEntityResponse<NotifyConfig>> Handle(AddNotifyConfigCommand request,
            CancellationToken cancellationToken)
        {
            await _notifyConfigRepository.AddAsync(new NotifyConfig(request.Code, request.Name,
                request.GroupId, request.Description, request.Active, request.Required, request.DesktopRequired,
                request.MobileRequired, request.WebRequired, request.Ord));
            await _unitOfWork.CommitAsync(cancellationToken);
            var response = new BaseEntityResponse<NotifyConfig> {Data = request.Map<NotifyConfig>(), Status = true};
            return response;
        }
    }
}