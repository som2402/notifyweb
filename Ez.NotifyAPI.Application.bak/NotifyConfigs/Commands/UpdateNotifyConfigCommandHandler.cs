﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Application.NotifyConfigGroups.Repositories;
using Ez.Notify.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Validators;

namespace Ez.Notify.Application.NotifyConfigs.Commands
{
    public class UpdateNotifyConfigCommand : IRequest<BaseEntityResponse<NotifyConfig>>
    {
        public UpdateNotifyConfigCommand(int id, string code, string name, int groupId, string description, bool active,
            int? required, int? desktopRequired, int? mobileRequired, int? webRequired, int ord)
        {
            Id = id;
            Code = code;
            Name = name;
            GroupId = groupId;
            Description = description;
            Active = active;
            Required = required;
            DesktopRequired = desktopRequired;
            MobileRequired = mobileRequired;
            WebRequired = webRequired;
            Ord = ord;
        }

        public int Id { get; }
        public string Code { get; }
        public string Name { get; }
        public int GroupId { get; }
        public string Description { get; }
        public bool Active { get; }
        public int? Required { get; }
        public int? WebRequired { get; set; }
        public int? MobileRequired { get; set; }
        public int? DesktopRequired { get; set; }
        public int Ord { get; set; }
    }


    public class UpdateNotifyConfigCommandValidator : BaseValidator<UpdateNotifyConfigCommand>
    {
        public UpdateNotifyConfigCommandValidator(INotifyConfigRepository notifyConfigRepository,
            INotifyConfigGroupRepository notifyConfigGroupRepository)
        {
            RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfig = await notifyConfigRepository.GetNotifyConfigById(x);
                return notifyConfig != null;
            }).WithErrorCode("ID_NOT_FOUND");

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithErrorCode("NAME_NOT_ALLOW_NULL")
                .MaximumLength(100)
                .WithErrorCode("NAME_LENGTH_OVER_LIMIT")
                .WithState(_ => new Dictionary<string, string> {{"NAME_MAX_LENGTH", "100"}});
            RuleFor(x => x.GroupId)
                .NotNull()
                .WithErrorCode("GROUP_ID_NOT_ALLOW_NULL")
                .GreaterThan(0)
                .WithErrorCode("GROUP_ID_VALUE_INVALID")
                .WithState(_ => new Dictionary<string, string> {{"ORD_MIN_VALUE", "1"}}).MustAsync(
                    async (x, cancellation) =>
                    {
                        var notifyConfigGroup = await notifyConfigGroupRepository.GetByIdAsync(x);
                        return notifyConfigGroup != null;
                    })
                .WithErrorCode("GROUP_ID_NOT_FOUND");
        }
    }

    public class
        UpdateNotifyConfigCommandHandler : IRequestHandler<UpdateNotifyConfigCommand, BaseEntityResponse<NotifyConfig>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateNotifyConfigCommandHandler(INotifyConfigRepository notifyConfigRepository, IUnitOfWork unitOfWork)
        {
            _notifyConfigRepository = notifyConfigRepository;
            _unitOfWork = unitOfWork;
        }


        public async Task<BaseEntityResponse<NotifyConfig>> Handle(UpdateNotifyConfigCommand request,
            CancellationToken cancellationToken)
        {
            var notifyConfig = await _notifyConfigRepository.GetNotifyConfigById(request.Id);
            notifyConfig.Update(request.Code, request.Name, request.GroupId, request.Description, request.Active,
                request.Required, request.DesktopRequired, request.MobileRequired, request.WebRequired, request.Ord);
            await _notifyConfigRepository.UpdateAsync(notifyConfig.Id, notifyConfig);
            await _unitOfWork.CommitAsync(cancellationToken);
            var response = new BaseEntityResponse<NotifyConfig> {Status = true, Data = request.Map<NotifyConfig>()};
            return response;
        }
    }
}