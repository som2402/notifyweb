﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.CampaignsLists.Command
// {
//     public class UpdateStatusCampaignsListCommand : IRequest
//     {
//         public int? Status { get; set; }
//         public int CampaignId { get; set; }
//     }
//
//     public class UpdateStatusCampaignsListCommandValidator : BaseValidator<UpdateStatusCampaignsListCommand>
//     {
//         public UpdateStatusCampaignsListCommandValidator(IRepository<Domain.Entity.CampaignsLists> repository)
//         {
//             RuleFor(x => x.CampaignId).NotNull().WithErrorCode("CAMPAIGNS_ID_NOT_ALLOW_NULL").MustAsync(
//                 async (x, cancellationToken) =>
//                 {
//                     var campaignsLists = await repository.GetSingleAsync(e => e.CampaignId.Equals(x));
//                     return campaignsLists != null;
//                 }).WithErrorCode("CAMPAIGNS_ID_NOT_FOUND");
//         }
//     }
//
//     public class UpdateStatusCampaignsListCommandHandler : IRequestHandler<UpdateStatusCampaignsListCommand>
//     {
//         private readonly IRepository<Domain.Entity.CampaignsLists> _campaignslistsrepository;
//
//         public UpdateStatusCampaignsListCommandHandler(
//             IRepository<Domain.Entity.CampaignsLists> campaignslistsrepository)
//         {
//             _campaignslistsrepository = campaignslistsrepository;
//         }
//
//
//         public async Task<Unit> Handle(UpdateStatusCampaignsListCommand request, CancellationToken cancellationToken)
//         {
//             var campaignsLists =
//                 await _campaignslistsrepository.GetSingleAsync(x => x.CampaignId.Equals(request.CampaignId));
//             campaignsLists.UpdateStatus(request.Status);
//             return Unit.Value;
//         }
//     }
// }