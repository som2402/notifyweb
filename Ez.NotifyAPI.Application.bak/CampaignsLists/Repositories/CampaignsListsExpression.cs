﻿using System;
using System.Linq.Expressions;

namespace Ez.Notify.Application.CampaignsLists.Repositories
{
    public static class CampaignsListsExpression
    {
        public static Expression<Func<global::Ez.Notify.Domain.Entity.CampaignsLists, bool>> Status(int status) =>
            campaignsList => campaignsList.Status.Equals(status);
    }
}