﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ez.Notify.Domain;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.CampaignsLists.Repositories
{
    public interface ICampaignsListsRepository : IRepository<global::Ez.Notify.Domain.Entity.CampaignsLists>
    {
        Task<IEnumerable<global::Ez.Notify.Domain.Entity.CampaignsLists>> GetListByStatus(int status);
    }

    public class CampaignsListsRepository : EfRepository<DataNotifyDbContext, global::Ez.Notify.Domain.Entity.CampaignsLists>,
        ICampaignsListsRepository
    {
        public CampaignsListsRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<global::Ez.Notify.Domain.Entity.CampaignsLists>> GetListByStatus(int status)
        {
            return await Queryable.Where(CampaignsListsExpression.Status(status)).ToListAsync();
        }
    }
}