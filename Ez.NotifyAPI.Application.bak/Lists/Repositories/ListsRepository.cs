﻿using Ez.Notify.Domain;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.Lists.Repositories
{
    public interface IListsRepository : IRepository<global::Ez.Notify.Domain.Entity.Lists>
    {
    }

    public class ListsRepository : EfRepository<DataNotifyDbContext, global::Ez.Notify.Domain.Entity.Lists>, IListsRepository
    {
        public ListsRepository(DataNotifyDbContext context) : base(context)
        {
        }
    }
}