﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Application.CampaignsLists.Queries;
// using Ez.Notify.Application.Subcribes.Queries;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
//
// namespace Ez.Notify.Application.Lists.Queries
// {
//     public class GetAllListQuery : IRequest<BaseEntityResponse<List<ListsDto>>>
//     {
//     }
//
//     public class GetAllListQueryHandler : IRequestHandler<GetAllListQuery,
//         BaseEntityResponse<List<ListsDto>>>
//     {
//         private readonly IRepository<Domain.Entity.Lists> _repository;
//
//         public GetAllListQueryHandler(IRepository<Domain.Entity.Lists> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<List<ListsDto>>> Handle(GetAllListQuery request,
//             CancellationToken cancellationToken)
//         {
//             var result =
//                 await _repository.GetAllAsync();
//             var response = new BaseEntityResponse<List<ListsDto>>
//             {
//                 Data = ConvertListToListsDto(result.ToList()),
//                 Status = true
//             };
//             return response;
//         }
//
//         private List<ListsDto> ConvertListToListsDto(List<Domain.Entity.Lists> list)
//         {
//             var result = new List<ListsDto>();
//             foreach (var item in list)
//             {
//                 var listDto = new ListsDto
//                 {
//                     Code = item.Code,
//                     Id = item.Id,
//                     Description = item.Description,
//                     Sub = item.Sub,
//                     Title = item.Code
//                 };
//                 foreach (var itemj in item.Subscribes.ToList())
//                     listDto.Subscribes.Add(new SubscribesDto
//                     {
//                         Code = itemj.Code,
//                         Email = itemj.Email,
//                         Id = itemj.Id,
//                         Name = itemj.Name,
//                         AccountId = itemj.AccountId,
//                         ListId = itemj.ListId
//                     });
//
//                 foreach (var itemz in item.CampaignsLists.ToList())
//                     listDto.CampaignsLists.Add(new CampaignsListsDto
//                     {
//                         Id = itemz.Id,
//                         Status = itemz.Status,
//                         Sub = itemz.Sub,
//                         CampaignId = itemz.CampaignId,
//                         ListId = itemz.ListId
//                     });
//
//                 result.Add(listDto);
//             }
//
//             return result;
//         }
//     }
// }