﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.CampaignsLists.Queries;
// using Ez.Notify.Application.Queries;
// using Ez.Notify.Application.Subcribes.Queries;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.Lists.Queries
// {
//     public class GetListsQuery : ListQuery, IRequest<PagingResponse<IList<ListsDto>>>
//     {
//         public GetListsQuery(int pageIndex, int pageSize, string query, string sorts) : base(pageIndex, pageSize, query,
//             sorts)
//         {
//         }
//     }
//
//     public class GetListsQueryHandler : IRequestHandler<GetListsQuery,
//         PagingResponse<IList<ListsDto>>>
//     {
//         private readonly IRepository<Domain.Entity.Lists> _repository;
//
//         public GetListsQueryHandler(IRepository<Domain.Entity.Lists> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<PagingResponse<IList<ListsDto>>> Handle(GetListsQuery request,
//             CancellationToken cancellationToken)
//         {
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _repository.QueryAsync(x =>
//                             string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<ListsDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<ListsDto>>(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var query = await
//                 _repository.QueryAsync(x =>
//                         string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                     request.PageIndex, request.PageSize);
//             var response =
//                 new PagingResponse<IList<ListsDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = ConvertListToListsDto(query.Items.ToList()),
//                     Total = query.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//
//         private List<ListsDto> ConvertListToListsDto(List<Domain.Entity.Lists> list)
//         {
//             var result = new List<ListsDto>();
//             foreach (var item in list)
//             {
//                 var listDto = new ListsDto
//                 {
//                     Code = item.Code,
//                     Id = item.Id,
//                     Description = item.Description,
//                     Sub = item.Sub,
//                     Title = item.Code
//                 };
//                 foreach (var itemj in item.Subscribes.ToList())
//                     listDto.Subscribes.Add(new SubscribesDto
//                     {
//                         Code = itemj.Code,
//                         Email = itemj.Email,
//                         Id = itemj.Id,
//                         Name = itemj.Name,
//                         AccountId = itemj.AccountId,
//                         ListId = itemj.ListId
//                     });
//
//                 foreach (var itemz in item.CampaignsLists.ToList())
//                     listDto.CampaignsLists.Add(new CampaignsListsDto
//                     {
//                         Id = itemz.Id,
//                         Status = itemz.Status,
//                         Sub = itemz.Sub,
//                         CampaignId = itemz.CampaignId,
//                         ListId = itemz.ListId
//                     });
//
//                 result.Add(listDto);
//             }
//
//             return result;
//         }
//     }
// }