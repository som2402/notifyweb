﻿using System.Collections.Generic;
using Ez.Notify.Application.CampaignsLists.Queries;
using Ez.Notify.Application.Subcribes.Queries;

namespace Ez.Notify.Application.Lists.Queries
{
    public class ListsDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Sub { get; set; }

        public List<CampaignsListsDto> CampaignsLists { get; set; } = new List<CampaignsListsDto>();

        public List<SubscribesDto> Subscribes { get; set; } = new List<SubscribesDto>();
    }
}