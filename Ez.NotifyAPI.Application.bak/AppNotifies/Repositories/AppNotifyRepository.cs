﻿using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.AppNotifies.Repositories
{
    public interface IAppNotifyRepository : IRepository<AppNotify>
    {
    }

    public class AppNotifyRepository : EfRepository<DataNotifyDbContext, AppNotify>, IAppNotifyRepository
    {
        public AppNotifyRepository(DataNotifyDbContext context) : base(context)
        {
        }
    }
}