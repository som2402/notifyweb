﻿namespace Ez.Notify.Application.EmailAttachments.Queries
{
    public class EmailAttachmentDto
    {
        public long Id { get; set; }
        public long EmailId { get; set; }
        public string FileName { get; set; }
        public byte[] FileBinary { get; set; }
    }
}