﻿using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Application.Campaigns.Queries;
using Microsoft.EntityFrameworkCore;
using Ez.Notify.Domain;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.Notify.Application.Campaigns.Repositories
{
    public interface ICampaignsRepository : IRepository<global::Ez.Notify.Domain.Entity.Campaigns>
    {
        Task<CampainsDto> GetByIdAsync(int id);
    }

    public class CampaignsRepository : EfRepository<DataNotifyDbContext, global::Ez.Notify.Domain.Entity.Campaigns>, ICampaignsRepository
    {
        public CampaignsRepository(DataNotifyDbContext dataNotifyDbContext) : base(dataNotifyDbContext)
        {
        }

        public async Task<CampainsDto> GetByIdAsync(int id)
        {
            return await Queryable.Where(CampaignsExpression.Id(id)).Select(CampaignsExpression.Model)
                .SingleOrDefaultAsync();
        }
    }
}