﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Application.Campaigns.Queries;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.Campaigns.Command
// {
//     public class UpdateCampaignsCommand : IRequest<BaseEntityResponse<List<CampainsDto>>>
//     {
//         public UpdateCampaignsCommand(string code, string title, string description, string htmlBody, string textBody,
//             int? status, string campaignType,
//             string senderCode, int? zone, int listId, int id)
//         {
//             Code = code;
//             Title = title;
//             Description = description;
//             HtmlBody = htmlBody;
//             TextBody = textBody;
//             Status = status;
//             CampaignType = campaignType;
//             SenderCode = senderCode;
//             Zone = zone;
//             ListId = listId;
//             Id = id;
//         }
//
//         public int Id { get; }
//         public string Code { get; }
//         public string Title { get; }
//         public string Description { get; }
//         public string HtmlBody { get; }
//         public string TextBody { get; }
//         public int? Status { get; }
//         public string CampaignType { get; }
//         public string Sender { get; set; }
//         public string SenderTitle { get; set; }
//         public string SenderCode { get; }
//         public int? Zone { get; }
//         public int ListId { get; }
//     }
//
//     public class UpdateCampaignsCommandValidator : BaseValidator<UpdateCampaignsCommand>
//     {
//         public UpdateCampaignsCommandValidator(IRepository<Domain.Entity.Lists> repository)
//         {
//             RuleFor(x => x.ListId)
//                 .NotNull()
//                 .WithErrorCode("LISTS_ID_NOT_ALLOW_NULL")
//                 .GreaterThan(0)
//                 .WithErrorCode("LISTS_ID_VALUE_INVALID")
//                 .WithState(_ => new Dictionary<string, string> {{"ORD_MIN_VALUE", "1"}}).Must(x =>
//                     repository.GetByIdAsync(x).GetAwaiter().GetResult() != null)
//                 .WithErrorCode("LISTS_ID_NOT_FOUND");
//         }
//     }
//
//     public class
//         UpdateCampaignsCommandHandler : IRequestHandler<UpdateCampaignsCommand, BaseEntityResponse<List<CampainsDto>>>
//     {
//         private readonly IRepository<Domain.Entity.Campaigns> _campaignsRepository;
//         private readonly IRepository<EmailSender> _emailSenderRepository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public UpdateCampaignsCommandHandler(IRepository<Domain.Entity.Campaigns> campaignsRepository,
//             IRepository<EmailSender> emailSenderRepository, IUnitOfWork unitOfWork)
//         {
//             _campaignsRepository = campaignsRepository;
//             _emailSenderRepository = emailSenderRepository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<List<CampainsDto>>> Handle(UpdateCampaignsCommand request,
//             CancellationToken cancellationToken)
//         {
//             var emailSender = await _emailSenderRepository.GetSingleAsync(x => x.Code.Equals(request.SenderCode));
//             var campains = await _campaignsRepository.GetByIdAsync(request.Id);
//             campains.Update(request.Code, request.Title, request.Description, request.HtmlBody, request.TextBody,
//                 request.Status, request.CampaignType, emailSender.Email, emailSender.Title, request.SenderCode,
//                 request.Zone);
//             _campaignsRepository.Update(campains);
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var result = await _campaignsRepository.GetAllAsync();
//             var ressponse = new BaseEntityResponse<List<CampainsDto>>
//             {
//                 Data = ConvertListCompainToListCampainsDtos(result.ToList()),
//                 Status = true
//             };
//             return ressponse;
//         }
//
//
//         private List<CampainsDto> ConvertListCompainToListCampainsDtos(List<Domain.Entity.Campaigns> campaignses)
//         {
//             var result = new List<CampainsDto>();
//             foreach (var item in campaignses)
//                 result.Add(new CampainsDto
//                 {
//                     Code = item.Code,
//                     Description = item.Description,
//                     Id = item.Id,
//                     Sender = item.Sender,
//                     Status = item.Status,
//                     Title = item.Title,
//                     Zone = item.Zone,
//                     CampaignType = item.CampaignType,
//                     CreatedBy = item.CreatedBy,
//                     CreatedDate = item.CreatedDate,
//                     HtmlBody = item.HtmlBody,
//                     SenderCode = item.SenderCode,
//                     SenderTitle = item.SenderTitle,
//                     TextBody = item.TextBody
//                 });
//
//             return result;
//         }
//     }
// }