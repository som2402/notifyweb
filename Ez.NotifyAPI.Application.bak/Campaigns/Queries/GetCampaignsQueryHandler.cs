﻿// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.Campaigns.Queries
// {
//     public class GetCampaignsQuery : IRequest<BaseEntityResponse<CampainsDto>>
//     {
//         public GetCampaignsQuery(int id)
//         {
//             Id = id;
//         }
//
//         public int Id { get; }
//     }
//
//     public class GetCampaignsQueryHandler : IRequestHandler<GetCampaignsQuery, BaseEntityResponse<CampainsDto>>
//     {
//         private readonly IRepository<Domain.Entity.Campaigns> _repository;
//
//         public GetCampaignsQueryHandler(IRepository<Domain.Entity.Campaigns> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<BaseEntityResponse<CampainsDto>> Handle(GetCampaignsQuery request,
//             CancellationToken cancellationToken)
//         {
//             var campaigns = await _repository.GetByIdAsync(request.Id);
//             var response = new BaseEntityResponse<CampainsDto>
//             {
//                 Data = campaigns.Map<CampainsDto>(),
//                 Status = true
//             };
//             return response;
//         }
//     }
// }