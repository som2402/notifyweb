#!/bin/bash

#####################################################################
#### NOTIFY_PRIVATE_API
#####################################################################

cat > gitlab-ci-private-api.yml <<HEREDOC
image: mcr.microsoft.com/dotnet/sdk:3.1

stages:
  - test
  - build
  - deploy

test_notify_private_api:
  before_script:
    - 'echo | dotnet --version'
  stage: test
  script:
    - dotnet test
  tags:
    - test

build_notify_private_api:
  stage: build
  needs:
    - test_notify_private_api
  script:
    - apt-get update -qq && apt-get install -y -qq zip
    - dotnet build
    - dotnet publish -c Release -o publish -r linux-x64 --self-contained false \$PROJECT_NOTIFY_PRIVATE
    - zip -r publish.zip publish
  artifacts:
    paths:
      - publish.zip
  tags:
    - build

deploy_notify_private_api:
  stage: deploy
  needs:
    - build_notify_private_api
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass unzip
    - sshpass -V
  script:
    - unzip publish.zip
    - cd publish
    - ls
    - sshpass -p \$USER_PASS ssh -o StrictHostKeyChecking=no \$USER_NAME@\$SERVER_IP /home/www/deploy-temp/notify-backend/notify-backend-private-api.sh
    - sshpass -p \$USER_PASS scp -o StrictHostKeyChecking=no -r * \$USER_NAME@\$SERVER_IP:/home/www/server/ez-notify-private-api
  when: manual
  tags:
    - deploy
HEREDOC


#####################################################################
#### NOTIFY_PUBLIC_API
#####################################################################

cat > gitlab-ci-public-api.yml <<HEREDOC
image: mcr.microsoft.com/dotnet/sdk:3.1

stages:
  - test
  - build
  - deploy

test_notify_public_api:
  before_script:
    - 'echo | dotnet --version'
  stage: test
  script:
    - dotnet test
  tags:
    - test

build_notify_public_api:
  stage: build
  needs:
    - test_notify_public_api
  script:
    - apt-get update -qq && apt-get install -y -qq zip
    - dotnet build
    - dotnet publish -c Release -o publish -r linux-x64 --self-contained false \$PROJECT_NOTIFY_PUBLIC
    - zip -r publish.zip publish
  artifacts:
    paths:
      - publish.zip
  tags:
    - build

deploy_notify_public_api:
  stage: deploy
  needs:
    - build_notify_public_api
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass unzip
    - sshpass -V
  script:
    - unzip publish.zip
    - cd publish
    - ls
    - sshpass -p \$USER_PASS ssh -o StrictHostKeyChecking=no \$USER_NAME@\$SERVER_IP /home/www/deploy-temp/notify-backend/notify-backend-public-api.sh
    - sshpass -p \$USER_PASS scp -o StrictHostKeyChecking=no -r * \$USER_NAME@\$SERVER_IP:/home/www/server/ez-notify-public-api
  when: manual
  tags:
    - deploy
HEREDOC


#####################################################################
#### NOTIFY_WEB_API
#####################################################################
cat > gitlab-ci-web-api.yml <<HEREDOC
image: mcr.microsoft.com/dotnet/sdk:3.1

stages:
  - test
  - build
  - deploy

test_notify_web_api:
  before_script:
    - 'echo | dotnet --version'
  stage: test
  script:
    - dotnet test
  tags:
    - test

build_notify_web_api:
  stage: build
  needs:
    - test_notify_web_api
  script:
    - apt-get update -qq && apt-get install -y -qq zip
    - dotnet build
    - dotnet publish -c Release -o publish -r linux-x64 --self-contained false \$PROJECT_NOTIFY_WEB
    - zip -r publish.zip publish
  artifacts:
    paths:
      - publish.zip
  tags:
    - build

deploy_notify_web_api:
  stage: deploy
  needs:
    - build_notify_web_api
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass unzip
    - sshpass -V
  script:
    - unzip publish.zip
    - cd publish
    - ls
    - sshpass -p \$USER_PASS ssh -o StrictHostKeyChecking=no \$USER_NAME@\$SERVER_IP /home/www/deploy-temp/notify-backend/notify-backend-web-api.sh
    - sshpass -p \$USER_PASS scp -o StrictHostKeyChecking=no -r * \$USER_NAME@\$SERVER_IP:/home/www/server/ez-notify-web-api
  when: manual
  tags:
    - deploy
HEREDOC

#####################################################################
#### NOTIFY_MQ_API
#####################################################################
cat > gitlab-ci-mq-api.yml <<HEREDOC
image: mcr.microsoft.com/dotnet/sdk:3.1

stages:
  - test
  - build
  - deploy

test_notify_mq_api:
  before_script:
    - 'echo | dotnet --version'
  stage: test
  script:
    - dotnet test
  tags:
    - test

build_notify_mq_api:
  stage: build
  needs:
    - test_notify_mq_api
  script:
    - apt-get update -qq && apt-get install -y -qq zip
    - dotnet build
    - dotnet publish -c Release -o publish -r linux-x64 --self-contained false \$PROJECT_NOTIFY_MQ_API
    - zip -r publish.zip publish
  artifacts:
    paths:
      - publish.zip
  tags:
    - build

deploy_notify_mq_api:
  stage: deploy
  needs:
    - build_notify_mq_api
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass unzip
    - sshpass -V
  script:
    - unzip publish.zip
    - cd publish
    - ls
    - sshpass -p \$USER_PASS ssh -o StrictHostKeyChecking=no \$USER_NAME@\$SERVER_IP /home/www/deploy-temp/notify-backend/notify-backend-mq-api.sh
    - sshpass -p \$USER_PASS scp -o StrictHostKeyChecking=no -r * \$USER_NAME@\$SERVER_IP:/home/www/server/ez-notify-mq-api
  when: manual
  tags:
    - deploy
HEREDOC
#####################################################################
#### NOTIFY_MQ_WORKER
#####################################################################
cat > gitlab-ci-mq-worker.yml <<HEREDOC
image: mcr.microsoft.com/dotnet/sdk:3.1

stages:
  - test
  - build
  - deploy

test_notify_mq_worker:
  before_script:
    - 'echo | dotnet --version'
  stage: test
  script:
    - dotnet test
  tags:
    - test

build_notify_mq_worker:
  stage: build
  needs:
    - test_notify_mq_worker
  script:
    - apt-get update -qq && apt-get install -y -qq zip
    - dotnet build
    - dotnet publish -c Release -o publish -r linux-x64 --self-contained false \$PROJECT_NOTIFY_MQ_WORKER
    - zip -r publish.zip publish
  artifacts:
    paths:
      - publish.zip
  tags:
    - build

deploy_notify_mq_worker:
  stage: deploy
  needs:
    - build_notify_mq_worker
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass unzip
    - sshpass -V
  script:
    - unzip publish.zip
    - cd publish
    - ls
    - sshpass -p \$USER_PASS ssh -o StrictHostKeyChecking=no \$USER_NAME@\$SERVER_IP /home/www/deploy-temp/notify-backend/notify-backend-mq-worker.sh
    - sshpass -p \$USER_PASS scp -o StrictHostKeyChecking=no -r * \$USER_NAME@\$SERVER_IP:/home/www/server/ez-notify-mq-worker
  when: manual
  tags:
    - deploy
HEREDOC