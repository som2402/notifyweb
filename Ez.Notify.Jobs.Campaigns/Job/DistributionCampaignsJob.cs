﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Ez.Notify.Common.Constants;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.AppNotifies.Repositories;
using Ez.NotifyAPI.Application.Campaigns.Repositories;
using Ez.NotifyAPI.Application.CampaignsLists.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.Lists.Repositories;
using Ez.NotifyAPI.Application.WebNotifies.Repositories;
using Quartz;
using Shared.EF.Interfaces;

namespace Ez.Notify.Jobs.Campaigns.Job
{
    [DisallowConcurrentExecution]
    public class DistributionCampaignsJob : IJob
    {
        private readonly IAppNotifyRepository _appNotifyRepository;
        private readonly ICampaignsListsRepository _campaignsListsRepository;
        private readonly ICampaignsRepository _campaignsRepository;
        private readonly IEmailNotifyRepository _emailNotifyRepository;
        private readonly IListsRepository _listsRepository;
        private readonly IWebNotifyRepository _webNotifyRepository;
        private readonly ILogger<DistributionCampaignsJob> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public DistributionCampaignsJob(IAppNotifyRepository appNotifyRepository,
            ICampaignsListsRepository campaignsListsRepository, ICampaignsRepository campaignsRepository,
            IEmailNotifyRepository emailNotifyRepository, IListsRepository listsRepository,
            IWebNotifyRepository webNotifyRepository, ILogger<DistributionCampaignsJob> logger, IUnitOfWork unitOfWork)
        {
            _appNotifyRepository = appNotifyRepository;
            _campaignsListsRepository = campaignsListsRepository;
            _campaignsRepository = campaignsRepository;
            _emailNotifyRepository = emailNotifyRepository;
            _listsRepository = listsRepository;
            _webNotifyRepository = webNotifyRepository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }


        public async Task Execute(IJobExecutionContext context)
        {
            var campaignLists =
                await _campaignsListsRepository.GetListByStatus(CampaignStatus.Delivering);
            if (campaignLists == null || !campaignLists.Any()) return;

            var getAllLists = await _listsRepository.Queryable.ToListAsync();
            var getAllCampaigns = await _campaignsRepository.Queryable.ToListAsync();
            var webNotifies = await _webNotifyRepository.Queryable.Where(x => !string.IsNullOrEmpty(x.CampaignList))
                .ToListAsync();
            var appNotifies = await _appNotifyRepository.Queryable.Where(x => !string.IsNullOrEmpty(x.CampaignList))
                .ToListAsync();
            var emailNotifies = await _emailNotifyRepository.Queryable.Where(x => !string.IsNullOrEmpty(x.CampaignList))
                .ToListAsync();
            foreach (var item in campaignLists)
            {
                var campaign = getAllCampaigns.FirstOrDefault(x => x.Id.Equals(item.CampaignId));
                var list = getAllLists.FirstOrDefault(x => x.Id.Equals(item.CampaignId));
                if (campaign != null)
                    switch (campaign.CampaignType)
                    {
                        case CampaignType.Web:
                            await ProcessWebNotify(list, campaign, item, webNotifies);
                            break;
                        case CampaignType.Email:
                            await ProcessEmailNotify(list, campaign, item, emailNotifies);
                            break;
                        case CampaignType.App:
                            await ProcessAppNotify(list, campaign, item, appNotifies);
                            break;
                    }
            }
        }

        private async Task ProcessWebNotify(Lists lists, Ez.Notify.Domain.Entity.Campaigns campaigns,
            CampaignsLists campaignsLists,
            IList<WebNotify> webNotifies)
        {
            var subcribes = lists.Subscribes;
            var addWebNotifies = new List<WebNotify>();
            foreach (var item in subcribes)
            {
                var webNotifiesByCondition = webNotifies.FirstOrDefault(x =>
                    x.CampaignList.Equals(campaignsLists.Id) && x.CustomerId.Equals(item.AccountId));
                if (webNotifiesByCondition == null)
                    addWebNotifies.Add(new WebNotify(campaigns.Title, campaigns.HtmlBody, string.Empty, string.Empty,
                        campaigns.CreatedBy, item.AccountId, campaigns.Code, RefTypeCampaigns.Web));
            }

            if (addWebNotifies.Count > 0)
            {
                await _webNotifyRepository.AddRangeAsync(addWebNotifies);
                campaignsLists.UpdateStatus(CampaignStatus.Delivered);
                await _campaignsListsRepository.UpdatePartialAsync(campaignsLists.Id, campaignsLists);
                await _unitOfWork.CommitAsync();
            }
        }

        private async Task ProcessEmailNotify(Lists lists, Ez.Notify.Domain.Entity.Campaigns campaigns,
            CampaignsLists campaignsLists,
            IList<EmailNotify> emailNotifies)
        {
            var subcribes = lists.Subscribes;
            var addEmailNotifies = new List<EmailNotify>();
            foreach (var item in subcribes)
            {
                var emailNotifiesByCondition = emailNotifies.FirstOrDefault(x =>
                    x.CampaignList.Equals(campaignsLists.Id) && x.AccountId.Equals(item.AccountId));
                if (emailNotifiesByCondition == null)
                    addEmailNotifies.Add(new EmailNotify(campaigns.Title, campaigns.HtmlBody, campaigns.HtmlBody,
                        item.Email, string.Empty, string.Empty, campaigns.Sender, campaigns.SenderTitle,
                        campaigns.SenderCode, campaigns.Code, RefTypeCampaigns.Email, DateTime.Now, campaigns.CreatedBy,
                        string.Empty, null, null, campaigns.Zone, item.AccountId));
            }

            if (addEmailNotifies.Count > 0)
            {
                await _emailNotifyRepository.AddRangeAsync(addEmailNotifies);
                campaignsLists.UpdateStatus(CampaignStatus.Delivered);
                await _campaignsListsRepository.UpdatePartialAsync(campaignsLists.Id, campaignsLists);
                await _unitOfWork.CommitAsync();
            }
        }

        private async Task ProcessAppNotify(Lists lists, Ez.Notify.Domain.Entity.Campaigns campaigns,
            CampaignsLists campaignsLists,
            IList<AppNotify> appNotifies)
        {
            var subcribes = lists.Subscribes;
            var addAppNotifies = new List<AppNotify>();
            foreach (var item in subcribes)
            {
                var appNotifiesByCondition = appNotifies.FirstOrDefault(x =>
                    x.CampaignList.Equals(campaignsLists.Id) && x.CustomerId.Equals(item.AccountId));
                if (appNotifiesByCondition == null)
                    addAppNotifies.Add(new AppNotify(campaigns.Title, campaigns.HtmlBody, campaigns.TextBody,
                        string.Empty, string.Empty,
                        campaigns.CreatedBy, item.AccountId, campaigns.Code, RefTypeCampaigns.Web));
            }

            if (addAppNotifies.Count > 0)
            {
                await _appNotifyRepository.AddRangeAsync(appNotifies);
                campaignsLists.UpdateStatus(CampaignStatus.Delivered);
                await _campaignsListsRepository.UpdatePartialAsync(campaignsLists.Id, campaignsLists);
                await _unitOfWork.CommitAsync();
            }
        }
    }
}