using Ez.Notify.Jobs.Campaigns.Job;
using Microsoft.Extensions.Hosting;
using Ez.Notify.Jobs.Campaigns.Helpers;
using Quartz;

namespace Ez.Notify.Jobs.Campaigns
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    // Add the required Quartz.NET services
                    services.AddQuartz(q =>
                    {
                        // Use a Scoped container to create jobs. I'll touch on this later
                        q.UseMicrosoftDependencyInjectionScopedJobFactory();
                        // Create a "key" for the job
                        // var jobKey = new JobKey("HelloWorldJob");
                        var jobKey = new JobKey("DistributionCampaignsJob");

                        // Register the job with the DI container
                        // q.AddJob<HelloWorldJob>(opts => opts.WithIdentity(jobKey));
                        q.AddJob<DistributionCampaignsJob>(opts => opts.WithIdentity(jobKey));
                        // Create a trigger for the job
                        q.AddTrigger(opts => opts
                            .ForJob(jobKey) // link to the HelloWorldJob
                            .WithIdentity("DistributionCampaignsJob-trigger") // give the trigger a unique name
                            .WithCronSchedule("0/5 * * * * ?")); // run every 5 seconds
                    });
                    // Add the Quartz.NET hosted service
                    services.AddQuartzHostedService(
                        q => q.WaitForJobsToComplete = true);
                    // other config
                    services.AddDbContext(hostContext.Configuration).AddRepositories().AddUnitOfWork();
                });
        }
    }
}