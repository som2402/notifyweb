﻿using Ez.Notify.Domain;
using Ez.NotifyAPI.Application.AppNotifies.Repositories;
using Ez.NotifyAPI.Application.Campaigns.Repositories;
using Ez.NotifyAPI.Application.CampaignsLists.Repositories;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using Ez.NotifyAPI.Application.Lists.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.WebNotifies.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Ioc;

namespace Ez.Notify.Jobs.Campaigns.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            });
            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddClassesInterfaces(typeof(INotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(INotifyConfigGroupRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICustomerNotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(IWebNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            services.AddClassesInterfaces(typeof(IAppNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICampaignsRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICampaignsListsRepository).Assembly);
            services.AddClassesInterfaces(typeof(IListsRepository).Assembly);
            return services;
        }
    }
}