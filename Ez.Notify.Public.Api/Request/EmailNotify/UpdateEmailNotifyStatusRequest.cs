﻿namespace Ez.Notify.Public.Api.Request.EmailNotify
{
    public class UpdateEmailNotifyStatusRequest
    {
        public int? Status { get; set; }
    }
}