﻿namespace Ez.Notify.Public.Api.Request.EmailNotify
{
    public class GetEmailByRefCodeRequest
    {
        public string RefCode { get; set; }
    }
}