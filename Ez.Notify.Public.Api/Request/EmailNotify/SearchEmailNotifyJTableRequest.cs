﻿using Shared.Common.JTable;

namespace Ez.Notify.Public.Api.Request.EmailNotify
{
    public class SearchEmailNotifyJTableRequest : JTableModel
    {
        public string Query { get; set; }
    }
}