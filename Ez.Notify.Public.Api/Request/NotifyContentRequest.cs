﻿using System.Collections.Generic;

namespace Ez.Notify.Public.Api.Request
{
    public class NotifyContentRequest
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string AccountId { get; set; }
        public string UrlAction { get; set; }
    }
}