﻿namespace Ez.Notify.Public.Api.Request.WebNotify
{
    public class SearchWebNotifyRequest
    {
        public string CustomerId { get; set; }
        public string Group { get; set; }
        public int Top { get; set; }
        public bool? Read { get; set; }
    }
}