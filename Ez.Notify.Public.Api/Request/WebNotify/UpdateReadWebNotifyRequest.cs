﻿using System.Collections.Generic;

namespace Ez.Notify.Public.Api.Request.WebNotify
{
    public class UpdateReadWebNotifyRequest
    {
        public IList<long> Ids { get; set; }
        public string CustomerId { get; set; }
    }
}