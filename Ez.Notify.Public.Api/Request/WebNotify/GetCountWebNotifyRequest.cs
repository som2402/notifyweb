﻿namespace Ez.Notify.Public.Api.Request.WebNotify
{
    public class GetCountWebNotifyRequest
    {
        public string CustomerId { get; set; }
        public string Group { get; set; }
        public bool? Read { get; set; }
    }
}