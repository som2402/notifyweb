﻿namespace Ez.Notify.Public.Api.Request.WebNotify
{
    public class SearchWebNotifyPagingRequest
    {
        public string CustomerId { get; set; }
        public string Group { get; set; }
        public bool? Read { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Query { get; set; }
    }
}