﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Newtonsoft.Json.Linq;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.EmailNotifies.Command;
// using Ez.Notify.Application.EmailNotifies.Queries;
// using Ez.Notify.Public.Api.Request.EmailNotify;
// using Shared.BaseModel;
// using Shared.Extensions;
//
// namespace Ez.Notify.Public.Api.Controllers
// {
//     public class EmailNotifyController : BaseController
//     {
//         public EmailNotifyController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpPost("get_jtable")]
//         [ProducesResponseType(typeof(JObject), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetJTableListEmailNotify([FromBody] SearchEmailNotifyJTableRequest request)
//         {
//             var result = await Mediator.Send(request.Map<GetJTableListEmailNotifyQuery>());
//             return Ok(result);
//         }
//
//         [HttpPost("send_email")]
//         [Consumes("multipart/form-data")]
//         public async Task<IActionResult> SendEmail([FromForm] SendEmailNotifyRequest request)
//         {
//             var result =
//                 await Mediator.Send(new SendEmailNotifyCommand(request.To,
//                     request.Bcc, request.Cc, request.SenderCode,
//                     request.SenderTitle, request.RefCode, request.RefType, request.Subject,
//                     request.Html,
//                     request.Attachments, request.Zone, request.AccountId));
//             return Ok(true);
//         }
//
//         [HttpPost("get_by_ref_code")]
//         [ProducesResponseType(typeof(BaseEntityResponse<List<EmailNotifyDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetByRefCode(GetEmailByRefCodeRequest request)
//         {
//             var result = await Mediator.Send(request.Map<GetEmaiNotifyByRefCodeQuery>());
//             return Ok(result);
//         }
//     }
// }