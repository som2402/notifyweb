﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Ez.Notify.Public.Api.Hubs;
using Shared.BaseModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ez.Notify.Public.Api.Request;
using Shared.Hubs;
using Microsoft.AspNetCore.Authorization;
using System;
using Ez.Notify.Public.Api.Helpers;
using Ez.NotifyAPI.Application.MessageFirebase.Commands;

namespace Ez.Notify.Public.Api.Controllers
{
    public class SendNotifyController : BaseController
    {
        private readonly IHubContext<NotificationHub> hubContext;
        private readonly IUserHubConnectionManager userConnectionManager;
        private readonly TokenConfig tokenConfig;

        public SendNotifyController(IHubContext<NotificationHub> hubContext,
            IUserHubConnectionManager userConnectionManager,
            TokenConfig tokenConfig)
        {
            this.hubContext = hubContext;
            this.userConnectionManager = userConnectionManager;
            this.tokenConfig = tokenConfig;
        }

        [HttpPost("signalr")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Signalr(NotifyContentRequest request)
        {
            var response = new BaseResponse();
            try
            {
                var basicAuthorization = string.Empty;
                if (Request.Headers.TryGetValue("Authorization", out var traceValue))
                {
                    basicAuthorization = traceValue.ToString().Replace("Bearer ", string.Empty);
                }
                //var encryptToken = $"ICHIBA-{request.AccountId}";
                //var token = Shared.Common.Utility.Encrypt(encryptToken);
                var token = tokenConfig.TokenSignalr;
                if (!token.Equals(basicAuthorization))
                {
                    response.Fail(null);
                    return Unauthorized();
                }
                List<string> connections;
                if (!string.IsNullOrWhiteSpace(request.AccountId))
                {
                    connections = userConnectionManager.GetUserConnections(request.AccountId);
                }
                else
                {
                    connections = userConnectionManager.GetConnections();
                }

                if (connections != null && connections.Count > 0)
                {
                    foreach (var connectionId in connections)
                    {
                        await hubContext.Clients.Client(connectionId).SendAsync("ReceiveMessage", request.Title, request.Body, request.UrlAction);
                    }
                }
                response.Successful();
                return Ok(response);
            }
            catch
            {
                response.Fail(null);
                return NotFound();
            }
        }

        [HttpPost("firebase")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> FireBase([FromBody] SendNotificationCommand request)
        {
            var result = await Mediator.Send(new SendNotificationCommand(request.Title, request.Body, request.UrlAction,
                request.AccountId));
            return Ok(result);
        }
    }
}
