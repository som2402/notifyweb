﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.WebNotifies.Commands;
// using Ez.Notify.Application.WebNotifies.Queries;
// using Ez.Notify.Public.Api.Request.WebNotify;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Public.Api.Controllers
// {
//     public class WebNotifyController : BaseController
//     {
//         public WebNotifyController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpPost("search")]
//         [ProducesResponseType(typeof(BaseEntityResponse<List<WebNotifyDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> Search(SearchWebNotifyRequest request)
//         {
//             var result =
//                 await Mediator.Send(new SearchWebNotifyQuery(request.CustomerId, request.Group, request.Top,
//                     request.Read));
//             return Ok(result);
//         }
//
//         [HttpPost("get_web_notify_paging")]
//         [ProducesResponseType(typeof(PagingResponse<List<WebNotifyDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> Gets(SearchWebNotifyPagingRequest request)
//         {
//             var result =
//                 await Mediator.Send(new SearchWebNotifyPagingQuery(request.PageIndex, request.PageSize, request.Query,
//                     null, request.CustomerId, request.Group, request.Read));
//             return Ok(result);
//         }
//
//
//         [HttpPost("get_count_web_notify")]
//         [ProducesResponseType(typeof(BaseEntityResponse<int>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetCountWebNotify(GetCountWebNotifyRequest request)
//         {
//             var result =
//                 await Mediator.Send(new GetCountWebNotifyQuery(request.CustomerId, request.Read, request.Group));
//             return Ok(result);
//         }
//
//
//         [HttpPost("update_read_web_notify")]
//         [ProducesResponseType(typeof(BaseEntityResponse<bool>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> UpdateReadWebNotify(UpdateReadWebNotifyRequest request)
//         {
//             var result = await Mediator.Send(new UpdateReadWebNotifyCommand(request.Ids, request.CustomerId));
//             return Ok(result);
//         }
//
//         [HttpPost("delete_web_notify")]
//         [ProducesResponseType(typeof(BaseEntityResponse<bool>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> DeleteWebNotify(UpdateReadWebNotifyRequest request)
//         {
//             var result = await Mediator.Send(new DeleteWebNotifyCommand(request.Ids, request.CustomerId));
//             return Ok(result);
//         }
//     }
// }