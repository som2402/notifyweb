﻿using AutoMapper;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.CampaignsLists.Queries;
using Ez.NotifyAPI.Application.EmailAttachments.Queries;
using Ez.NotifyAPI.Application.EmailNotifies.Queries;
using Ez.NotifyAPI.Application.Lists.Queries;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Queries;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;
using Ez.NotifyAPI.Application.Subcribes.Queries;
using Ez.NotifyAPI.Application.WebNotifies.Queries;

namespace Ez.Notify.Public.Api.AutoMappers
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<NotifyConfigGroup, NotifyConfigGroupDto>();
            CreateMap<NotifyConfigGroup, NotifyConfigAndNotifyConfigGroupDto>();
            CreateMap<NotifyConfig, NotifyConfigDto>();
            CreateMap<EmailNotify, EmailNotifyDto>();
            CreateMap<EmailAttachment, EmailAttachmentDto>();
            CreateMap<Lists, ListsDto>();
            CreateMap<Subscribes, SubscribesDto>();
            CreateMap<CampaignsLists, CampaignsListsDto>();
            CreateMap<WebNotify, WebNotifyDto>();
        }
    }
}