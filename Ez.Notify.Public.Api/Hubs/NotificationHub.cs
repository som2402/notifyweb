﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Shared.Hubs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ez.Notify.Public.Api.Hubs
{
    public class NotificationHub : Hub
    {
        public void SendChatMessage(string who, string message)
        {
            Clients.User(who).SendAsync("ReceiveMessage", who + ": " + message);
        }

        private readonly IUserHubConnectionManager _userConnectionManager;
        public NotificationHub(IUserHubConnectionManager userConnectionManager)
        {
            _userConnectionManager = userConnectionManager;
        }

        public override Task OnConnectedAsync()
        {
            //string name = Context.User.Identity.Name;
            var httpContext = this.Context.GetHttpContext();
            var signalToken = httpContext.Request.Query["signalToken"];
            if (signalToken.Any())
            {
                var userId = Shared.Common.Utility.Decrypt(signalToken);

                _userConnectionManager.KeepUserConnection(userId, Context.ConnectionId);
            }

            return base.OnConnectedAsync();
        }

        public string GetConnectionId()
        {
            //var httpContext = this.Context.GetHttpContext();
            //var userId = httpContext.Request.Query["signalToken"];
            //_userConnectionManager.KeepUserConnection(userId, Context.ConnectionId);

            return Context.ConnectionId;
        }

        public async override Task OnDisconnectedAsync(System.Exception exception)
        {
            //get the connectionId
            var connectionId = Context.ConnectionId;
            _userConnectionManager.RemoveUserConnection(connectionId);
            var value = await Task.FromResult(0);//adding dump code to follow the template of Hub > OnDisconnectedAsync
        }
    }
}
