﻿using System.Collections.Generic;

namespace Ez.Notify.Public.Api.Helpers
{
    public class TokenConfig
    {
        public string TokenSignalr { get; set; }
        public IList<string> CrossOriginDomain { get; set; }
    }
}