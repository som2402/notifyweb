﻿using System.Data;
using System.Reflection;
using Ez.IS4.API.Driver;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Customer.Driver;
using Ez.Notify.Domain;
using Ez.Notify.MQ.Driver;
using Ez.NotifyAPI.Application.Config;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.EmailActionClicks.Repositories;
using Ez.NotifyAPI.Application.EmailActionViews.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Shared.Behaviours;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Ioc;
using Shared.ResilientHttp;
using Shared.Services;
using Assembly = Ez.NotifyAPI.Application.Assembly;
using Shared.Hubs;
using System.Linq;
using Ez.Notify.Domain.Entity;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;

namespace Ez.Notify.Public.Api.Helpers
{
    public static class StartupHelpers
    {
        public static readonly string AssemblyName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                ProxiesExtensions.UseLazyLoadingProxies(opt)
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            }).AddScoped<IDbConnection>(sp =>
            {
                var config = sp.GetRequiredService<IConfiguration>();
                var connection =
                    new SqlConnection(config.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
                connection.Open();
                return connection;
            });
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            // services.AddTransient<IRepository<NotifyConfigGroup>, Repository<DataNotifyDbContext, NotifyConfigGroup>>();
            // services.AddTransient<IRepository<NotifyConfig>, Repository<DataNotifyDbContext, NotifyConfig>>();
            services.AddClassesInterfaces(typeof(INotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(INotifyConfigGroupRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICustomerNotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionClickRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionViewRepository).Assembly);
            // services.AddTransient<IRepository<EmailNotify>, Repository<DataNotifyDbContext, EmailNotify>>();
            // services.AddTransient<IRepository<EmailSender>, Repository<DataNotifyDbContext, EmailSender>>();
            // services.AddTransient<IRepository<EmailAttachment>, Repository<DataNotifyDbContext, EmailAttachment>>();
            // services.AddTransient<IRepository<WebNotify>, Repository<DataNotifyDbContext, WebNotify>>();
            //services.AddTransient<IRepository<CustomerNotifyConfig>, Repository<DataNotifyDbContext, CustomerNotifyConfig>>();
            // services.AddTransient<IRepository<AppNotify>, Repository<DataNotifyDbContext, AppNotify>>();
            // services.AddTransient<IRepository<Campaigns>, Repository<DataNotifyDbContext, Campaigns>>();
            // services.AddTransient<IRepository<CampaignsLists>, Repository<DataNotifyDbContext, CampaignsLists>>();
            // services.AddTransient<IRepository<Subscribes>, Repository<DataNotifyDbContext, Subscribes>>();
            // services.AddTransient<IRepository<Lists>, Repository<DataNotifyDbContext, Lists>>();

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddUserClaim(this IServiceCollection services)
        {
            services.AddScoped<IUserContext, UserClaim>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IAuthorizeClient, AuthorizeClientImplement>();
            services.AddTransient<MessageQueueClient>();
            services.AddTransient<IEmailService, EmailSenderService>();
            services.AddTransient<ISequenceService, SequenceService>();
            services.AddSingleton<IUserHubConnectionManager, UserHubConnectionManager>();
            services.AddScoped<CustomerClient>();
            return services;
        }

        public static IServiceCollection AddRabbitMq(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddEvents(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddMediatREvent(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Assembly).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(Shared.Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddBehaviour(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            return services;
        }

        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(typeof(Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            // var mapperConfig = new MapperConfiguration(cfg =>
            // {
            //     cfg.AddProfile(new DomainToDtoMappingProfile());
            //     cfg.AddProfile(new CommandToDomainMappingProfile());
            //     cfg.AddProfile(new RequestToQueryMappingProfile());
            // });
            // services.AddSingleton(mapperConfig.CreateMapper().RegisterMap());
            return services;
        }

        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("AppConfig").Get<AppConfig>());
            services.AddSingleton(sp => configuration.GetSection("IS4:AccessConfig").Get<AccessConfig>());
            services.AddSingleton(sp =>
                configuration.GetSection("MessageQueue:MessageQueueConfig").Get<MessageQueueConfig>());
            services.AddSingleton(sp => configuration.GetSection("Authorize").Get<AuthorizeConfig>());
            services.AddSingleton(sp => configuration.GetSection("CustomerApi").Get<CustomerConfig>());
            services.AddSingleton(configuration.GetSection("TokenConfig").Get<TokenConfig>());
            return services;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            var serviceProvider = services.BuildServiceProvider();
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    var authorizeConfig = serviceProvider.GetRequiredService<AuthorizeConfig>();
                    options.Authority = authorizeConfig.Issuer;
                    options.RequireHttpsMetadata = authorizeConfig.RequireHttpsMetadata;
                    options.ApiName = authorizeConfig.ApiName;
                });

            services.AddSingleton<IResilientHttpClientFactory, ResilientHttpClientFactory>(sp =>
            {
                const int retryCount = 3;
                const int exceptionsAllowedBeforeBreaking = 5;
                return new ResilientHttpClientFactory(null, exceptionsAllowedBeforeBreaking, retryCount);
            });
            services.AddSingleton<IHttpClient, ResilientHttpClient>(sp =>
                sp.GetService<IResilientHttpClientFactory>().CreateResilientHttpClient());
            return services;
        }

        public static IServiceCollection AddAuthorization(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddApiVersioning(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "iChiba CO private api v1",
                    Version = "v1"
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new[] {"Bearer"}
                    }
                });
                c.CustomSchemaIds(x => x.FullName);
            });
        }

        public static IServiceCollection AddCorsPolicy(this IServiceCollection services, IConfiguration configuration)
        {
            var domainCrosses = configuration.GetSection("TokenConfig").Get<TokenConfig>();

            services.AddCors(options =>
            {
                options.AddPolicy("MyAllowSpecificOrigins",
                    builder =>
                    {
                        builder
                            .WithOrigins(domainCrosses.CrossOriginDomain.ToArray())
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
            });
            return services;
        }

        public static IServiceCollection AddFirebaseAppMessaging(this IServiceCollection services, IConfiguration configuration)

        {
            var firebaseConfig = configuration.GetSection("FirebaseApp").Get<FirebaseConfig>();
            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile("serviceAccountKey.json")
                    .CreateScoped(firebaseConfig.GoogleCredential)
            });
            return services;
        }

        public static void UseCorsPolicy(this IApplicationBuilder app)
        {
            app.UseCors("MyAllowSpecificOrigins");
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "iChiba CO private api v1");
                c.DocumentTitle = "iChiba CO private api v1";
            });
        }
    }
}