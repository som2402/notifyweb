﻿using System.Threading.Tasks;
using Ez.IS4.API.Driver;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Ez.Notify.Public.Api
{
    public class AuthorizeClientImplement : IAuthorizeClient
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthorizeClientImplement(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> GetAuthorizeToken()
        {
            var isAuthenticated = _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
            string authorizationToken = null;

            if (isAuthenticated)
                authorizationToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            return authorizationToken;
        }
    }
}