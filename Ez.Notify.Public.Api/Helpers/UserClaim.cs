﻿using System;
using Ez.Notify.Common.Constants;
using Ez.Notify.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Ez.Notify.Public.Api.Helpers
{
    public class UserClaim : IUserContext
    {
        public UserClaim(IHttpContextAccessor contextAccessor)
        {
            var claims = contextAccessor.HttpContext?.User;
            UserId = claims == null ? Guid.Empty : Guid.Parse(claims?.FindFirst(ClaimType.UserId)?.Value!);
            Name = claims?.FindFirst(ClaimType.Name)?.Value;
            UserEmail = claims?.FindFirst(ClaimType.Email)?.Value;
        }

        public Guid UserId { get; }
        public string Name { get; }
        public string UserEmail { get; }
    }
}