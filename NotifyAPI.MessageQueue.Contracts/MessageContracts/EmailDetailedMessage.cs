﻿using System;

namespace Ez.Notify.MQ.Contracts.MessageContracts
{
    public class EmailDetailedMessage : IEmailDetailedMessage
    {
        public Guid MessageId { get; set; }
        public long EmailId { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public interface IEmailDetailedMessage
    {
        public Guid MessageId { get; set; }
        public long EmailId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}