﻿using System;
using System.Collections.Generic;
using Shared.Services;

namespace Ez.Notify.MQ.Contracts.MessageContracts
{
    public class EmailSentDto
    {
        public long EmailId { get; set; }
        public List<FileAttachment> Attachments { get; set; }
    }

    public class EmailSentMessage : IEmailSentMessage
    {
        public Guid MessageId { get; set; }
        public EmailSentDto EmailDto { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public interface IEmailSentMessage
    {
        Guid MessageId { get; set; }
        EmailSentDto EmailDto { get; set; }
        DateTime CreationDate { get; set; }
    }
}