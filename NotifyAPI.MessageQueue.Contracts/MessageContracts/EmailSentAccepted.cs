﻿using System;

namespace Ez.Notify.MQ.Contracts.MessageContracts
{
    public class EmailSentAccepted
    {
        public Guid MessageId { get; set; }

        public bool Accepted { get; set; }
    }
}