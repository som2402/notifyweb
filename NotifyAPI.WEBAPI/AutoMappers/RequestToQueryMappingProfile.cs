﻿// using AutoMapper;
// using Ez.Notify.Application.EmailNotifies.Queries;
// using Ez.Notify.Application.WebNotifies.Queries;
// using Ez.Notify.Web.Api.Request.EmailNotify;
// using Ez.Notify.Web.Api.Request.WebNotify;
//
// namespace Ez.Notify.Web.Api.AutoMappers
// {
//     public class RequestToQueryMappingProfile : Profile
//     {
//         public RequestToQueryMappingProfile()
//         {
//             CreateMap<SearchEmailNotifyJTableRequest, GetJTableListEmailNotifyQuery>();
//             CreateMap<SearchWebNotifyJTableRequest, GetJTableListWebNotifyQuery>();
//         }
//     }
// }