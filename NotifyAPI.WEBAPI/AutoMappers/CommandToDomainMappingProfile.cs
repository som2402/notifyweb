﻿// using AutoMapper;
// using Ez.Notify.Application.EmailNotifies.Command;
// using Ez.Notify.Application.NotifyConfigGroups.Commands;
// using Ez.Notify.Application.NotifyConfigs.Commands;
// using Ez.Notify.Domain.Entity;
//
// namespace Ez.Notify.Web.Api.AutoMappers
// {
//     public class CommandToDomainMappingProfile : Profile
//     {
//         public CommandToDomainMappingProfile()
//         {
//             CreateMap<AddNotifyConfigGroupCommand, NotifyConfigGroup>()
//                 .ConstructUsing(c => new NotifyConfigGroup(c.Title, c.Ord, c.AppId));
//             CreateMap<UpdateNotifyConfigGroupCommand, NotifyConfigGroup>();
//             CreateMap<AddNotifyConfigCommand, NotifyConfig>();
//             CreateMap<UpdateNotifyConfigCommand, NotifyConfig>();
//             CreateMap<AddEmailNotifyCommand, EmailNotify>();
//         }
//     }
// }