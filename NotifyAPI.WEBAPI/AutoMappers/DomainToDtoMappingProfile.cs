﻿using AutoMapper;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.Campaigns.Queries;
using Ez.NotifyAPI.Application.EmailAttachments.Queries;
using Ez.NotifyAPI.Application.EmailNotifies.Queries;
using Ez.NotifyAPI.Application.Lists.Queries;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Queries;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;
using Ez.NotifyAPI.Application.Subcribes.Queries;

namespace Ez.Notify.Web.Api.AutoMappers
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<NotifyConfigGroup, NotifyConfigGroupDto>();
            CreateMap<NotifyConfig, NotifyConfigDto>();
            CreateMap<EmailNotify, EmailNotifyDto>();
            CreateMap<EmailAttachment, EmailAttachmentDto>();
            CreateMap<Subscribes, SubscribesDto>();
            CreateMap<Lists, ListsDto>();
            CreateMap<Campaigns, CampainsDto>();
        }
    }
}