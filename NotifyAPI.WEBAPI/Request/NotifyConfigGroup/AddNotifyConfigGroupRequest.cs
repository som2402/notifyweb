﻿namespace Ez.Notify.Web.Api.Request.NotifyConfigGroup
{
    public class AddNotifyConfigGroupRequest
    {
        public string Title { get; set; }
        public int Ord { get; set; }
        public string AppId { get; set; }
    }
}