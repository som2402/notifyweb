﻿using Microsoft.AspNetCore.Http;

namespace Ez.Notify.Web.Api.Request.Lists
{
    public class ImportListSubcribeRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public IFormFile FormFile { get; set; }
    }
}