﻿namespace Ez.Notify.Web.Api.Request.Campains
{
    public class UpdateCampaignsRequest
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
        public int? Status { get; set; }
        public string CampaignType { get; set; }
        public string SenderCode { get; set; }
        public int? Zone { get; set; }
        public int ListId { get; set; }
    }
}