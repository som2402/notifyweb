﻿namespace Ez.Notify.Web.Api.Request.EmailNotify
{
    public class UpdateEmailNotifyStatusRequest
    {
        public int? Status { get; set; }
    }
}