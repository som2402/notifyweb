﻿using Shared.Common.JTable;

namespace Ez.Notify.Web.Api.Request.EmailNotify
{
    public class SearchEmailNotifyJTableRequest : JTableModel
    {
        public string Query { get; set; }
    }
}