﻿using Shared.Common.JTable;

namespace Ez.Notify.Web.Api.Request.WebNotify
{
    public class SearchWebNotifyJTableRequest : JTableModel
    {
        public string Query { get; set; }
    }
}