﻿using System.Data;
using System.Reflection;
using Core.Resilience.Http;
using Ez.IS4.API.Driver;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Ez.Notify.MQ.Driver;
using FluentValidation;
using Ichiba.Cdn.Client.Implement;
using Ichiba.Cdn.Client.Implement.Configs;
using Ichiba.Cdn.Client.Interface;
using iChibaShopping.Core.AppService.Implement;
using iChibaShopping.Core.AppService.Interface;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Services;
using Assembly = Ez.NotifyAPI.Application.Assembly;

namespace Ez.Notify.Web.Api.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            }).AddScoped<IDbConnection>(sp =>
            {
                var config = sp.GetRequiredService<IConfiguration>();
                var connection =
                    new SqlConnection(config.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
                connection.Open();
                return connection;
            });
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IRepository<NotifyConfigGroup>, Repository<DataNotifyDbContext, NotifyConfigGroup>>();
            services.AddTransient<IRepository<NotifyConfig>, Repository<DataNotifyDbContext, NotifyConfig>>();
            services.AddTransient<IRepository<EmailNotify>, Repository<DataNotifyDbContext, EmailNotify>>();
            services.AddTransient<IRepository<EmailAttachment>, Repository<DataNotifyDbContext, EmailAttachment>>();
            services.AddTransient<IRepository<WebNotify>, Repository<DataNotifyDbContext, WebNotify>>();
            services.AddTransient<IRepository<EmailSender>, Repository<DataNotifyDbContext, EmailSender>>();
            services.AddTransient<IRepository<AppNotify>, Repository<DataNotifyDbContext, AppNotify>>();
            services.AddTransient<IRepository<Campaigns>, Repository<DataNotifyDbContext, Campaigns>>();
            services.AddTransient<IRepository<CampaignsLists>, Repository<DataNotifyDbContext, CampaignsLists>>();
            services.AddTransient<IRepository<Subscribes>, Repository<DataNotifyDbContext, Subscribes>>();
            services.AddTransient<IRepository<Lists>, Repository<DataNotifyDbContext, Lists>>();
            services
                .AddTransient<IRepository<CustomerNotifyConfig>, Repository<DataNotifyDbContext, CustomerNotifyConfig>
                >();
            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddUserClaim(this IServiceCollection services)
        {
            services.AddScoped<IUserContext, UserClaim>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IResilientHttpClientFactory, ResilientHttpClientFactory>(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<ResilientHttpClient>>();
                var retryCount = 3;
                var exceptionsAllowedBeforeBreaking = 5;

                return new ResilientHttpClientFactory(logger, exceptionsAllowedBeforeBreaking, retryCount);
            });
            services.AddSingleton<IHttpClient, ResilientHttpClient>(sp =>
                sp.GetService<IResilientHttpClientFactory>().CreateResilientHttpClient());
            // services.AddTransient<AccessClient>();
            services.AddTransient<MessageQueueClient>();
            services.AddTransient<IEmailService, EmailSenderService>();
            services.AddTransient<ISequenceService, SequenceService>();
            services.AddTransient<IFileAppService, FileAppService>();
            services.AddTransient<IFileStorage, FileStorage>();
            return services;
        }


        public static IServiceCollection AddEvents(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddMediatREvent(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Assembly).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(Shared.Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(typeof(System.Reflection.Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            // var mapperConfig = new MapperConfiguration(cfg =>
            // {
            //     cfg.AddProfile(new DomainToDtoMappingProfile());
            //     cfg.AddProfile(new CommandToDomainMappingProfile());
            //     cfg.AddProfile(new RequestToQueryMappingProfile());
            // });
            // services.AddSingleton(mapperConfig.CreateMapper().RegisterMap());
            return services;
        }

        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<FileUploadConfig>(configuration.GetSection("FileStorage"));
            // services.AddSingleton(configuration.GetSection("AppConfig").Get<AppConfig>());
            services.AddSingleton(sp => configuration.GetSection("IS4:AccessConfig").Get<AccessConfig>());
            // services.AddSingleton(sp => configuration.GetSection("Authorize").Get<AuthorizeConfig>());
            services.AddSingleton(sp =>
                configuration.GetSection("MessageQueue:MessageQueueConfig").Get<MessageQueueConfig>());
            return services;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            // var serviceProvider = services.BuildServiceProvider();
            // services.AddAuthentication("Bearer")
            //     .AddIdentityServerAuthentication(options =>
            //     {
            //         var authorizeConfig = serviceProvider.GetRequiredService<AuthorizeConfig>();
            //         options.Authority = authorizeConfig.Issuer;
            //         options.RequireHttpsMetadata = authorizeConfig.RequireHttpsMetadata;
            //         options.ApiName = authorizeConfig.ApiName;
            //     });

            services
                .AddSingleton<Shared.ResilientHttp.IResilientHttpClientFactory,
                    Shared.ResilientHttp.ResilientHttpClientFactory>(sp =>
                {
                    const int retryCount = 3;
                    const int exceptionsAllowedBeforeBreaking = 5;
                    return new Shared.ResilientHttp.ResilientHttpClientFactory(null, exceptionsAllowedBeforeBreaking,
                        retryCount);
                });
            services.AddSingleton<Shared.ResilientHttp.IHttpClient, Shared.ResilientHttp.ResilientHttpClient>(sp =>
                sp.GetService<Shared.ResilientHttp.IResilientHttpClientFactory>().CreateResilientHttpClient());
            return services;
        }

        public static IServiceCollection AddAuthorization(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddApiVersioning(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API Notify api v1",
                    Version = "v1"
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new[] {"Bearer"}
                    }
                });
                c.CustomSchemaIds(x => x.FullName);
            });
        }

        public static IServiceCollection AddCorsPolicy(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("MyAllowSpecificOrigins",
                    builder =>
                    {
                        builder
                            .WithOrigins("http://localhost:4200")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
            });
            return services;
        }

        public static void UseCorsPolicy(this IApplicationBuilder app)
        {
            app.UseCors("MyAllowSpecificOrigins");
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "IC Notify Cms api v1");
                c.DocumentTitle = "IC Notify Cms api v1";
            });
        }
    }
}