using Microsoft.Extensions.Hosting;
using Shared.AspNetCore;
using Shared.Logging;


namespace Ez.Notify.Web.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Host.CreateDefaultBuilder(args).UseSerilog().Run<Startup>();
        }
    }
}