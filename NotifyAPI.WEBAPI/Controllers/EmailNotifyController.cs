﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Newtonsoft.Json.Linq;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.EmailNotifies.Command;
// using Ez.Notify.Application.EmailNotifies.Queries;
// using Ez.Notify.Domain.Entity;
// using Ez.Notify.Web.Api.Request.EmailNotify;
// using Shared.BaseModel;
// using Shared.Extensions;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class EmailNotifyController : BaseController
//     {
//         public EmailNotifyController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpPost]
//         [ProducesResponseType(typeof(BaseEntityResponse<EmailNotify>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Post(AddEmailNotifyRequest request)
//         {
//             var result = await Mediator.Send(new AddEmailNotifyCommand(request.Subject, request.TextBody,
//                 request.HtmlBody, request.To, request.Cc, request.Bcc, request.Sender, request.SenderTitle,
//                 request.SenderCode, request.RefCode, request.RefType, request.CustomerId, request.CreatedDate,
//                 request.CreatedBy, request.SendMessage, request.SentDate, request.SentCount));
//             return Ok(result);
//         }
//
//         [HttpPut("{emailNotifyId}")]
//         [ProducesResponseType(typeof(BaseEntityResponse<EmailNotify>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Put(long emailNotifyId, UpdateEmailNotifyRequest request)
//         {
//             var result = await Mediator.Send(new UpdateEmailNotifyCommand(emailNotifyId, request.Subject,
//                 request.TextBody,
//                 request.HtmlBody, request.To, request.Cc, request.Bcc, request.Sender, request.SenderTitle,
//                 request.SenderCode, request.RefCode, request.RefType, request.CustomerId, request.CreatedDate,
//                 request.CreatedBy, request.SendMessage, request.SentDate, request.SentCount));
//             return Ok(result);
//         }
//
//         [HttpPut("{emailNotifyId}/update_sending_status")]
//         [ProducesResponseType(typeof(BaseEntityResponse<EmailNotify>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> UpdateStatus(long emailNotifyId, UpdateEmailNotifyStatusRequest request)
//         {
//             var result = await Mediator.Send(new UpdateEmailNotifyStatusCommand(emailNotifyId, request.Status));
//             return Ok(result);
//         }
//
//         [HttpDelete("{emailNotifyId}")]
//         [ProducesResponseType(typeof(BaseEntityResponse<long>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Delete(long emailNotifyId)
//         {
//             var result = await Mediator.Send(new DeleteEmailNotifyCommand(emailNotifyId));
//             return Ok(result);
//         }
//
//         [HttpGet("get_all")]
//         [ProducesResponseType(typeof(PagingResponse<IList<EmailNotifyDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetAll()
//         {
//             var result = await Mediator.Send(new GetAllEmailNotifyQuery());
//             return Ok(result);
//         }
//
//         [HttpGet]
//         [ProducesResponseType(typeof(PagingResponse<IList<EmailNotifyDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetNotifyConfigs(string query, int pageIndex = 1, int pageSize = 10,
//             string sorts = null)
//         {
//             var result = await Mediator.Send(new GetListEmailNotifyQuery(pageIndex,
//                 pageSize, query, sorts));
//             return Ok(result);
//         }
//
//         [HttpPost("get_jtable")]
//         [ProducesResponseType(typeof(JObject), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetJTableListEmailNotify([FromBody] SearchEmailNotifyJTableRequest request)
//         {
//             var result = await Mediator.Send(request.Map<GetJTableListEmailNotifyQuery>());
//             return Ok(result);
//         }
//
//         [HttpGet("{emailNotifyId}")]
//         [ProducesResponseType(typeof(EmailNotifyDto), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetDetails(long emailNotifyId)
//         {
//             var result = await Mediator.Send(new GetEmailNotifyQuery(emailNotifyId));
//             return Ok(result);
//         }
//
//         [HttpPost("send_email")]
//         [Consumes("multipart/form-data")]
//         public async Task<IActionResult> SendEmail([FromForm] SendEmailNotifyRequest request)
//         {
//             var result =
//                 await Mediator.Send(new SendEmailNotifyCommand(request.To,
//                     request.Bcc, request.Cc, request.SenderCode,
//                     request.SenderTitle, request.RefCode, request.RefType, request.Subject,
//                     request.Html,
//                     request.Attachments, request.Zone, request.AccountId));
//             return Ok(result);
//         }
//     }
// }