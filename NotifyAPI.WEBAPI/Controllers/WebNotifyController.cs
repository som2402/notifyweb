﻿// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Newtonsoft.Json.Linq;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.WebNotifies.Queries;
// using Ez.Notify.Web.Api.Request.WebNotify;
// using Shared.Extensions;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class WebNotifyController : BaseController
//     {
//         public WebNotifyController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpPost("get_jtable")]
//         [ProducesResponseType(typeof(JObject), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetJTableListWebNotify([FromBody] SearchWebNotifyJTableRequest request)
//         {
//             var result = await Mediator.Send(request.Map<GetJTableListWebNotifyQuery>());
//             return Ok(result);
//         }
//     }
// }