﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using iChibaShopping.Core.AppService.Interface;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using Shared.Common;

namespace Ez.Notify.Web.Api.Controllers
{
    public class FileController : BaseController
    {
        private static readonly FormOptions DefaultFormOptions = new FormOptions();
        private readonly IFileAppService _fileAppService;

        public FileController(IFileAppService fileAppService)
        {
            _fileAppService = fileAppService;
        }


        [RequestSizeLimit(100000000)]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int) HttpStatusCode.Forbidden)]
        public async Task<IActionResult> UploadImage()
        {
            try
            {
                var boundary = MultipartRequestHelper.GetBoundary(MediaTypeHeaderValue.Parse(Request.ContentType),
                    DefaultFormOptions.MultipartBoundaryLengthLimit);
                var reader = new MultipartReader(boundary, HttpContext.Request.Body);
                var section = reader.ReadNextSectionAsync().Result;
                while (section != null)
                {
                    ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out var contentDisposition);
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        var fileNameUpload = HeaderUtilities.RemoveQuotes(contentDisposition.FileName).Value;
                        var bytes = StreamToBytes(section.Body);
                        var result = await _fileAppService.Upload(fileNameUpload, bytes);

                        return Ok(result);
                    }

                    section = await reader.ReadNextSectionAsync();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        private byte[] StreamToBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using var ms = new MemoryStream();
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
            return ms.ToArray();
        }
    }
}