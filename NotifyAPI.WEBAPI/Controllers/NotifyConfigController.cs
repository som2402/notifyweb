﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ez.Notify.Domain.Entity;
using Ez.Notify.Web.Api.Request.NotifyConfig;
using Ez.NotifyAPI.Application.NotifyConfigs.Commands;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.BaseModel;
using Shared.BaseModel.Grids;

namespace Ez.Notify.Web.Api.Controllers
{
    public class NotifyConfigController : BaseController
    {
        [HttpPost]
        [ProducesResponseType(typeof(BaseEntityResponse<NotifyConfig>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(AddNotifyConfigRequest addNotifyConfigRequest)
        {
            var result = await Mediator.Send(new AddNotifyConfigCommand(addNotifyConfigRequest.Code,
                addNotifyConfigRequest.Name, addNotifyConfigRequest.GroupId, addNotifyConfigRequest.Description,
                addNotifyConfigRequest.Active, addNotifyConfigRequest.Required, addNotifyConfigRequest.DesktopRequired,
                addNotifyConfigRequest.MobileRequired, addNotifyConfigRequest.WebRequired, addNotifyConfigRequest.Ord));
            return Ok(result);
        }

        [HttpPut("{notifyConfigId}")]
        [ProducesResponseType(typeof(BaseEntityResponse<NotifyConfig>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int notifyConfigId, UpdateNotifyConfigRequest updateNotifyConfigRequest)
        {
            var result = await Mediator.Send(new UpdateNotifyConfigCommand(notifyConfigId,
                updateNotifyConfigRequest.Code,
                updateNotifyConfigRequest.Name, updateNotifyConfigRequest.GroupId,
                updateNotifyConfigRequest.Description,
                updateNotifyConfigRequest.Active, updateNotifyConfigRequest.Required,
                updateNotifyConfigRequest.DesktopRequired, updateNotifyConfigRequest.MobileRequired,
                updateNotifyConfigRequest.WebRequired, updateNotifyConfigRequest.Ord));
            return Ok(result);
        }

        [HttpDelete("{notifyConfigId}")]
        [ProducesResponseType(typeof(BaseEntityResponse<int>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int notifyConfigId)
        {
            var result = await Mediator.Send(new DeleteNotifyConfigCommand(notifyConfigId));
            return Ok(result);
        }

        [HttpGet("get_all")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var result = await Mediator.Send(new GetAllNotifyConfigQuery());
            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(typeof(PagingResponse<IList<NotifyConfigDto>>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetNotifyConfigGrids(GridParameters gridParameters)
        {
            var result = await Mediator.Send(new GetGridNotifyConfigQuery(gridParameters));
            return Ok(result);
        }

        [HttpGet("{notifyConfigId}")]
        [ProducesResponseType(typeof(NotifyConfigDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDetails(int notifyConfigId)
        {
            var result = await Mediator.Send(new GetNotifyConfigQuery(notifyConfigId));
            return Ok(result);
        }
    }
}