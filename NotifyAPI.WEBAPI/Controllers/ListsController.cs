﻿// using System.Collections.Generic;
// using System.IO;
// using System.Net.Http.Headers;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.Lists.Commands;
// using Ez.Notify.Application.Lists.Queries;
// using Ez.Notify.Application.Subcribes.Queries;
// using Ez.Notify.Domain.Entity;
// using Ez.Notify.Web.Api.Request.Lists;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class ListsController : BaseController
//     {
//         private readonly IWebHostEnvironment _hostingEnvironment;
//
//         public ListsController(AccessClient accessClient, AppConfig appConfig, IWebHostEnvironment hostingEnvironment) :
//             base(accessClient, appConfig)
//         {
//             _hostingEnvironment = hostingEnvironment;
//         }
//
//         [HttpPost]
//         [ProducesResponseType(typeof(BaseEntityResponse<List<Lists>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [Consumes("multipart/form-data")]
//         public async Task<IActionResult> Post([FromForm] ImportListSubcribeRequest importListSubcribeRequest)
//         {
//             var filename = ContentDispositionHeaderValue
//                 .Parse(importListSubcribeRequest.FormFile.ContentDisposition)
//                 .FileName
//                 .Trim('"');
//
//             var folder = _hostingEnvironment.WebRootPath + @"\uploaded\excels\subcribes";
//             if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
//
//             var filePath = Path.Combine(folder, filename);
//
//             await using (var fs = System.IO.File.Create(filePath))
//             {
//                 await importListSubcribeRequest.FormFile.CopyToAsync(fs);
//                 fs.Flush();
//             }
//
//             var result = await Mediator.Send(new ImportListsCommand(filePath, importListSubcribeRequest.Title,
//                 importListSubcribeRequest.Description, importListSubcribeRequest.Code));
//             return Ok(result);
//         }
//
//         [HttpGet("get_subcribes_by_listid")]
//         [ProducesResponseType(typeof(PagingResponse<IList<SubscribesDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetSubcribeByListId(string query, int listId, int pageIndex = 1,
//             int pageSize = 10,
//             string sorts = null)
//         {
//             var result = await Mediator.Send(new GetSubcribesByListIdQuery(pageIndex,
//                 pageSize, query, sorts, listId));
//             return Ok(result);
//         }
//
//         [HttpGet]
//         [ProducesResponseType(typeof(PagingResponse<IList<ListsDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> Get(string query, int pageIndex = 1,
//             int pageSize = 10,
//             string sorts = null)
//         {
//             var result = await Mediator.Send(new GetListsQuery(pageIndex,
//                 pageSize, query, sorts));
//             return Ok(result);
//         }
//
//         [HttpGet("get_all")]
//         [ProducesResponseType(typeof(BaseEntityResponse<List<ListsDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetAll()
//         {
//             var result = await Mediator.Send(new GetAllListQuery());
//             return Ok(result);
//         }
//     }
// }