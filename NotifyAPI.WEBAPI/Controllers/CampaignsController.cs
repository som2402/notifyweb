﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.Campaigns.Command;
// using Ez.Notify.Application.Campaigns.Queries;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Web.Api.Request.Campains;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class CampaignsController : BaseController
//     {
//         public CampaignsController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpPost]
//         [ProducesResponseType(typeof(BaseEntityResponse<List<CampainsDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Post(AddCampainsRequest addCampainsRequest)
//         {
//             var result = await Mediator.Send(new AddCampaignsCommand(addCampainsRequest.Code, addCampainsRequest.Title,
//                 addCampainsRequest.Description, addCampainsRequest.HtmlBody, addCampainsRequest.TextBody,
//                 addCampainsRequest.Status, addCampainsRequest.CampaignType, addCampainsRequest.SenderCode,
//                 addCampainsRequest.Zone,
//                 addCampainsRequest.ListId));
//             return Ok(result);
//         }
//
//         [HttpGet("{campaignsId}")]
//         [ProducesResponseType(typeof(CampainsDto), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetDetails(int campaignsId)
//         {
//             var result = await Mediator.Send(new GetCampaignsQuery(campaignsId));
//             return Ok(result);
//         }
//
//         [HttpPut("{campaignsId}")]
//         public async Task<IActionResult> Put(int campaignsId, UpdateCampaignsRequest request)
//         {
//             var result = await Mediator.Send(new UpdateCampaignsCommand(request.Code, request.Title,
//                 request.Description, request.HtmlBody, request.TextBody, request.Status, request.CampaignType,
//                 request.SenderCode, request.Zone, request.ListId, campaignsId));
//             return Ok(result);
//         }
//
//         [HttpGet]
//         [ProducesResponseType(typeof(PagingResponse<IList<CampainsDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> Get(string query, int pageIndex = 1,
//             int pageSize = 10,
//             string sorts = null)
//         {
//             var result = await Mediator.Send(new GetListCampaignsQuery(pageIndex, pageSize, query, sorts));
//             return Ok(result);
//         }
//     }
// }