﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.EmailSenders.Queries;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class EmailSenderController : BaseController
//     {
//         public EmailSenderController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpGet("get_all")]
//         [ProducesResponseType(typeof(List<EmailSenderDto>), StatusCodes.Status200OK)]
//         [ProducesResponseType(
//             StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(
//             StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetAll()
//         {
//             var result = await Mediator.Send(new GetAllEmailSenderQuery());
//             return Ok(result);
//         }
//     }
// }