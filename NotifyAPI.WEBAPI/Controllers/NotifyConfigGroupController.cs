﻿// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.Config;
// using Ez.Notify.Application.NotifyConfigGroups.Commands;
// using Ez.Notify.Application.NotifyConfigGroups.Queries;
// using Ez.Notify.Domain.Entity;
// using Ez.Notify.Web.Api.Request.NotifyConfigGroup;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class NotifyConfigGroupController : BaseController
//     {
//         public NotifyConfigGroupController(AccessClient accessClient, AppConfig appConfig) : base(accessClient,
//             appConfig)
//         {
//         }
//
//         [HttpPost]
//         [ProducesResponseType(typeof(BaseEntityResponse<NotifyConfigGroup>), StatusCodes.Status204NoContent)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Post(AddNotifyConfigGroupRequest configGroupRequest)
//         {
//             var result = await Mediator.Send(new AddNotifyConfigGroupCommand(configGroupRequest.Title,
//                 configGroupRequest.Ord, configGroupRequest.AppId));
//             return Ok(result);
//         }
//
//         [HttpPut("{notifyConfigGroupId}")]
//         [ProducesResponseType(StatusCodes.Status204NoContent)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Update(int notifyConfigGroupId,
//             UpdateNotifyConfigGroupRequest updateNotifyConfigGroupRequest)
//         {
//             var result = await Mediator.Send(new UpdateNotifyConfigGroupCommand(notifyConfigGroupId,
//                 updateNotifyConfigGroupRequest.Title,
//                 updateNotifyConfigGroupRequest.Ord, updateNotifyConfigGroupRequest.AppId));
//             return Ok(result);
//         }
//
//         [HttpDelete("{notifyConfigGroupId}")]
//         [ProducesResponseType(StatusCodes.Status204NoContent)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         public async Task<IActionResult> Delete(int notifyConfigGroupId)
//         {
//             var result = await Mediator.Send(new DeleteNotifyConfigGroupCommand(notifyConfigGroupId));
//             return NoContent();
//         }
//
//         [HttpGet("{notifyConfigGroupId}")]
//         [ProducesResponseType(typeof(NotifyConfigGroupDto), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetDetails(int notifyConfigGroupId)
//         {
//             var result = await Mediator.Send(new GetNotifyConfigGroupQuery(notifyConfigGroupId));
//             return Ok(result);
//         }
//
//         [HttpGet]
//         [ProducesResponseType(typeof(PagingResponse<IList<NotifyConfigGroupDto>>), StatusCodes.Status200OK)]
//         [ProducesResponseType(StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetNotifyConfigGroups(string query, int pageIndex = 1, int pageSize = 10,
//             string sorts = null)
//         {
//             var result = await Mediator.Send(new GetListNotifyConfigGroupQuery(pageIndex,
//                 pageSize, query, sorts));
//             return Ok(result);
//         }
//
//         [HttpGet("get_all")]
//         [ProducesResponseType(typeof(List<NotifyConfigGroupDto>), StatusCodes.Status200OK)]
//         [ProducesResponseType(
//             StatusCodes.Status400BadRequest)]
//         [ProducesResponseType(
//             StatusCodes.Status500InternalServerError)]
//         public async Task<IActionResult> GetAll()
//         {
//             var result = await Mediator.Send(new GetAllNotifyConfigGroupQuery());
//             return Ok(result);
//         }
//     }
// }