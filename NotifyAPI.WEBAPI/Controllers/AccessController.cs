﻿// using System.Collections.Generic;
// using System.Net;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using Ez.IS4.API.Driver.Models.Response;
// using Microsoft.AspNetCore.Mvc;
// using Ez.Notify.Application.AcessApps;
// using Ez.Notify.Application.Config;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Web.Api.Controllers
// {
//     public class AccessController : BaseController
//     {
//         public AccessController(AccessClient accessClient, AppConfig appConfig) : base(accessClient, appConfig)
//         {
//         }
//
//         [HttpGet("get_resources")]
//         [ProducesResponseType((int) HttpStatusCode.BadRequest)]
//         [ProducesResponseType((int) HttpStatusCode.Unauthorized)]
//         [ProducesResponseType((int) HttpStatusCode.Forbidden)]
//         [ProducesResponseType((int) HttpStatusCode.OK, Type = typeof(BaseEntityResponse<IList<Resource>>))]
//         public async Task<IActionResult> GetResources()
//         {
//             var response = await Mediator.Send(new GetResourceQuery());
//             return Ok(response);
//         }
//
//         [HttpGet("check_permission")]
//         [ProducesResponseType((int) HttpStatusCode.BadRequest)]
//         [ProducesResponseType((int) HttpStatusCode.Unauthorized)]
//         [ProducesResponseType((int) HttpStatusCode.Forbidden)]
//         [ProducesResponseType((int) HttpStatusCode.OK, Type = typeof(BaseResponse))]
//         public async Task<IActionResult> CheckPermission(string resourceKey, string action)
//         {
//             var response = await Mediator.Send(new CheckPermissionQuery(resourceKey, action));
//             return Ok(response);
//         }
//     }
// }