﻿using System.IO;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Ez.Notify.Application.Config;
using Ez.Notify.Application.EmailActionClicks.Command;
using Ez.Notify.Application.EmailActionViews.Command;
using Shared.Common;

namespace Ez.Notify.Tracking.WebApplication.Controllers
{
    public class TrackingController : Controller
    {
        private readonly IMediator _mediator;
        private readonly AppConfig _appConfig;
        private readonly IHostingEnvironment _host;

        public TrackingController(IMediator mediator, AppConfig appConfig, IHostingEnvironment host)
        {
            _mediator = mediator;
            _appConfig = appConfig;
            _host = host;
        }

        [Route("Tracking/TrackPressLink")]
        public async Task<IActionResult> TrackPressLink(int eId, string returnUrl, string hash)
        {
            // if (HashValidLink(eId, hash, returnUrl))
            // {
                var result = await _mediator.Send(new AddEmailActionClickCommand(eId, returnUrl));
            // }

            return Redirect(returnUrl);
        }

        [Route("Tracking/OpenEmail")]
        public async Task<IActionResult> OpenEmail(int eId, string hash)
        {
            // if (HashValidOpenImage(eId, hash))
            // {
                var result = await _mediator.Send(new AddEmailActionViewCommand(eId, "test"));
            // }
            var dir = $"{_host.ContentRootPath}/wwwroot/images";
            var imagePath = Path.Combine(dir, "logo.png");
            return File(await System.IO.File.ReadAllBytesAsync(imagePath), "image/png");
        }


        private bool HashValidLink(int eId, string hash, string returnUrl)
        {
            var hashInput = string.Join('|', eId, returnUrl);
            var result = Utility.BCryptIsMatch(hashInput, _appConfig.ClickTrackingPrivateKeyPepper, hash);
            return result;
        }

        private bool HashValidOpenImage(int eId, string hash)
        {
            var result = Utility.BCryptIsMatch(eId.ToString(), _appConfig.ClickTrackingPrivateKeyPepper, hash);
            return result;
        }
    }
}