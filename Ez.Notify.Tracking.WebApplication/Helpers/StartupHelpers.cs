﻿using System.Data;
using System.Reflection;
using Ez.Notify.Application.Config;
using Ez.Notify.Application.CustomerNotifyConfigs.Repositories;
using Ez.Notify.Application.EmailActionClicks.Command;
using Ez.Notify.Application.EmailActionClicks.Repositories;
using Ez.Notify.Application.EmailActionViews.Command;
using Ez.Notify.Application.EmailActionViews.Repositories;
using Ez.Notify.Application.EmailNotifies.Repositories;
using Ez.Notify.Application.EmailSenders.Repositories;
using Ez.Notify.Application.NotifyConfigGroups.Repositories;
using Ez.Notify.Application.NotifyConfigs.Repositories;
using Ez.Notify.Domain;
using FluentValidation;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Behaviours;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Ioc;

namespace Ez.Notify.Tracking.WebApplication.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            }).AddScoped<IDbConnection>(sp =>
            {
                var config = sp.GetRequiredService<IConfiguration>();
                var connection =
                    new SqlConnection(config.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
                connection.Open();
                return connection;
            });
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddClassesInterfaces(typeof(INotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(INotifyConfigGroupRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICustomerNotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionClickRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionViewRepository).Assembly);

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddUserClaim(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddMediatREvent(this IServiceCollection services)
        {
            // services.AddMediatR(typeof(Application.Assembly).GetTypeInfo().Assembly);
            services
                .AddTransient<IRequestHandler<AddEmailActionClickCommand, bool>, AddEmailActionClickCommandHandler>();
            services.AddTransient<IRequestHandler<AddEmailActionViewCommand, bool>, AddEmailActionViewCommandHandler>();
            // services.AddMediatR(typeof(Shared.Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddBehaviour(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            return services;
        }

        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(typeof(Assembly).GetTypeInfo().Assembly);
            return services;
        }


        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("AppConfig").Get<AppConfig>());
            return services;
        }
    }
}