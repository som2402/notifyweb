﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ez.Notify.MQ.Contracts.MessageContracts;

namespace Notify.MQ.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmailNotifyController : ControllerBase
    {
        private readonly IPublishEndpoint _endpoint;
        private readonly ILogger<EmailNotifyController> _logger;

        public EmailNotifyController(IPublishEndpoint endpoint, ILogger<EmailNotifyController> logger)
        {
            _endpoint = endpoint;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Submit([FromBody] EmailSentDto emailSentDto,
            CancellationToken cancellationToken)
        {
            try
            {
                var mailChangedRequest = new EmailSentMessage
                {
                    CreationDate = DateTime.Now,
                    EmailDto = emailSentDto,
                    MessageId = new Guid()
                };
                await _endpoint.Publish<IEmailSentMessage>(mailChangedRequest, cancellationToken);
                return Ok();
                // var tasks = Enumerable.Range(0, 10).Select(item =>
                // {
                //     var mailChangedRequest = new EmailSentMessage
                //     {
                //         CreationDate = DateTime.Now,
                //         EmailDto = emailSentDto,
                //         MessageId = new Guid()
                //     };
                //
                //     return _endpoint.Publish<IEmailSentMessage>(mailChangedRequest, cancellationToken);
                // }).ToArray();
                // Task.WaitAll(tasks);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok();
        }
    }
}