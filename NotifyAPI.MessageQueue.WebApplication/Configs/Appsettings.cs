﻿namespace Notify.MQ.Api.Configs
{
    public class Appsettings
    {
        public Appsettings()
        {
        }

        public Appsettings(QueueSettings queueSettings)
        {
            QueueSettings = queueSettings;
        }

        public QueueSettings QueueSettings { get; set; }
    }
}