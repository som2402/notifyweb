﻿using System;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Notify.MQ.Api.Configs;
using Ez.Notify.MQ.Contracts.MessageContracts;

namespace Notify.MQ.Api.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection RegisterQueueServices(this IServiceCollection services, IConfiguration section)
        {
            // var appSettingsSection = section.GetSection("AppSettings");
            // var appSettings = appSettingsSection.Get<Appsettings>();
            //
            // services.AddSingleton(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
            // {
            //     cfg.Host(appSettings.QueueSettings.HostName, appSettings.QueueSettings.VirtualHost,
            //         h =>
            //         {
            //             h.Username(appSettings.QueueSettings.UserName);
            //             h.Password(appSettings.QueueSettings.Password);
            //         });
            //
            //     cfg.ExchangeType = ExchangeType.Direct;
            // }));
            // services.AddSingleton<IPublishEndpoint>(provider => provider.GetRequiredService<IBusControl>());
            // services.AddSingleton<ISendEndpointProvider>(provider => provider.GetRequiredService<IBusControl>());
            // services.AddSingleton<IBus>(provider => provider.GetRequiredService<IBusControl>());
            // return services;

            var appsettings = section.GetSection("AppSettings").Get<Appsettings>();
            var queueSettings = appsettings.QueueSettings;

            services.AddSingleton(appsettings);
            services.AddMassTransit(c =>
            {
                c.AddBus(serviceProvider =>
                    Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        cfg.Host(queueSettings.HostName, queueSettings.VirtualHost, h =>
                        {
                            h.Username(queueSettings.UserName);
                            h.Password(queueSettings.Password);
                        });
                    }));
            });

            EndpointConvention.Map<IEmailSentMessage>(new Uri(queueSettings.EndPointSendEmail));

            return services;
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Notify MQ Api",
                    Version = "v1"
                });
            });
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Notify MQ Api");
                c.DocumentTitle = "Notify MQ Api";
            });
        }
    }
}