﻿using System.Threading.Tasks;
using Ez.IS4.API.Driver;
using Ez.Notify.Common.Models;
using Shared.ResilientHttp;

namespace Ez.Notify.MQ.Driver
{
    public class MessageQueueConfig
    {
        private const string SendEmailPath = "/api/EmailNotify";
        private string _sendEmailPath;

        public MessageQueueConfig()
        {
            GetSendEmailPath = null;
        }

        public string BaseApi { get; set; }

        public string GetSendEmailPath
        {
            get => _sendEmailPath;
            set => _sendEmailPath = BuildFullUrl(ValueOrDefault(value, SendEmailPath));
        }

        private string BuildFullUrl(string path)
        {
            return $"{BaseApi}{path}";
        }

        private string ValueOrDefault(string value, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(value) ? defaultValue : value;
        }
    }

    public class MessageQueueClient : BaseClient
    {
        private readonly MessageQueueConfig _messageQueueConfig;

        public MessageQueueClient(IHttpClient httpClient, IAuthorizeClient authorizeClient,
            MessageQueueConfig messageQueueConfig) : base(httpClient,
            authorizeClient)
        {
            _messageQueueConfig = messageQueueConfig;
        }

        public async Task SendEmail(SendEmailNotifyToMessageQueueRequest messageQueueRequest)
        {
            var url = $"{_messageQueueConfig.GetSendEmailPath}";
            var result = await PostAsyncNoAuthen(url, messageQueueRequest);
        }
    }
}