﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailActionClickConfiguration : IEntityTypeConfiguration<EmailActionClick>
    {
        public void Configure(EntityTypeBuilder<EmailActionClick> entity)
        {
            entity.ToTable("EMAIL_ACTION_CLICK");

            entity.Property(e => e.ClickDate).HasColumnType("datetime");

            entity.Property(e => e.Link)
                .IsRequired()
                .HasMaxLength(255);

            entity.HasOne(d => d.Email)
                .WithMany(p => p.EmailActionClick)
                .HasForeignKey(d => d.EmailId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EMAIL_ACTION_CLICK_EMAIL_NOTIFY");
        }
    }
}