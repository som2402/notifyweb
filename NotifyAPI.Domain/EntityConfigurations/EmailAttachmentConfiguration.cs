﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailAttachmentConfiguration : IEntityTypeConfiguration<EmailAttachment>
    {
        public void Configure(EntityTypeBuilder<EmailAttachment> entity)
        {
            entity.ToTable("EMAIL_ATTACHMENT");

            entity.Property(e => e.FileBinary).IsRequired();

            entity.Property(e => e.FileName)
                .IsRequired()
                .HasMaxLength(255);

            entity.HasOne(d => d.Email)
                .WithMany(p => p.EmailAttachment)
                .HasForeignKey(d => d.EmailId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EMAIL_ATTACHMENT_EMAIL_NOTIFY");
        }
    }
}