﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class NotifyConfigGroupConfiguration : IEntityTypeConfiguration<NotifyConfigGroup>
    {
        public void Configure(EntityTypeBuilder<NotifyConfigGroup> entity)
        {
            entity.ToTable("NOTIFY_CONFIG_GROUP");

            entity.Property(e => e.AppId)
                .IsRequired()
                .HasMaxLength(255);

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}