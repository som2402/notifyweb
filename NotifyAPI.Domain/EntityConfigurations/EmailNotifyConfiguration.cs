﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailNotifyConfiguration : IEntityTypeConfiguration<EmailNotify>
    {
        public void Configure(EntityTypeBuilder<EmailNotify> entity)
        {
            entity.ToTable("EMAIL_NOTIFY");

            entity.Property(e => e.AccountId).HasMaxLength(50);

            entity.Property(e => e.Bcc).HasMaxLength(255);

            entity.Property(e => e.CampaignList).HasMaxLength(255);

            entity.Property(e => e.Cc).HasMaxLength(255);

            entity.Property(e => e.CreatedBy).HasMaxLength(255);

            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            entity.Property(e => e.HtmlBody).HasColumnType("ntext");

            entity.Property(e => e.RefCode).HasMaxLength(255);

            entity.Property(e => e.RefType).HasMaxLength(255);

            entity.Property(e => e.Sender).HasMaxLength(255);

            entity.Property(e => e.SenderCode).HasMaxLength(255);

            entity.Property(e => e.SenderTitle).HasMaxLength(255);

            entity.Property(e => e.SentDate).HasColumnType("datetime");

            entity.Property(e => e.SentFailCount).HasDefaultValueSql("((0))");

            entity.Property(e => e.Subject)
                .IsRequired()
                .HasMaxLength(255);

            entity.Property(e => e.TextBody).HasColumnType("ntext");

            entity.Property(e => e.To).HasMaxLength(255);

            entity.Property(e => e.UID).HasColumnName("UID");
        }
    }
}