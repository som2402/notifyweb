﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailSenderConfiguration : IEntityTypeConfiguration<EmailSender>
    {
        public void Configure(EntityTypeBuilder<EmailSender> entity)
        {
            entity.ToTable("EMAIL_SENDER");

            entity.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(255);

            entity.Property(e => e.Imap)
                .HasColumnName("IMAP")
                .HasMaxLength(255);

            entity.Property(e => e.Imapport).HasColumnName("IMAPPort");

            entity.Property(e => e.Imapssl).HasColumnName("IMAPSSL");

            entity.Property(e => e.Password).HasMaxLength(1000);

            entity.Property(e => e.Pop3)
                .HasColumnName("POP3")
                .HasMaxLength(255);

            entity.Property(e => e.Pop3port).HasColumnName("POP3Port");

            entity.Property(e => e.Pop3ssl).HasColumnName("POP3SSL");

            entity.Property(e => e.Smtp)
                .HasColumnName("SMTP")
                .HasMaxLength(255);

            entity.Property(e => e.Smtpport).HasColumnName("SMTPPort");

            entity.Property(e => e.Smtpssl).HasColumnName("SMTPSSL");

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);

            entity.Property(e => e.UserName).HasMaxLength(255);
        }
    }
}