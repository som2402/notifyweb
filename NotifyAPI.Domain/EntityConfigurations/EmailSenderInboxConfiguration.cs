﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailSenderInboxConfiguration : IEntityTypeConfiguration<EmailSenderInbox>
    {
        public void Configure(EntityTypeBuilder<EmailSenderInbox> entity)
        {
            entity.ToTable("EMAIL_SENDER_INBOX");

            entity.Property(e => e.Body).HasColumnType("ntext");

            entity.Property(e => e.BodyParts).HasColumnType("ntext");

            entity.Property(e => e.Date).HasColumnType("datetime");

            entity.Property(e => e.Folder).HasMaxLength(255);

            entity.Property(e => e.Headers).HasColumnType("ntext");

            entity.Property(e => e.HtmlBody).HasColumnType("ntext");

            entity.Property(e => e.Importance).HasMaxLength(50);

            entity.Property(e => e.MailBox).HasMaxLength(255);

            entity.Property(e => e.MessageId).HasMaxLength(255);

            entity.Property(e => e.MimeVersion).HasMaxLength(50);

            entity.Property(e => e.Priority).HasMaxLength(50);

            entity.Property(e => e.References).HasMaxLength(250);

            entity.Property(e => e.ResentDate).HasColumnType("datetime");

            entity.Property(e => e.ResentMessageId).HasMaxLength(255);

            entity.Property(e => e.ResentSender).HasMaxLength(255);

            entity.Property(e => e.SendDate).HasColumnType("datetime");

            entity.Property(e => e.Sender).HasMaxLength(255);

            entity.Property(e => e.TextBody).HasColumnType("ntext");
        }
    }
}