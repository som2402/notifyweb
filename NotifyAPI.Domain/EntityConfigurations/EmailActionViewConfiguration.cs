﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class EmailActionViewConfiguration : IEntityTypeConfiguration<EmailActionView>
    {
        public void Configure(EntityTypeBuilder<EmailActionView> entity)
        {
            entity.ToTable("EMAIL_ACTION_VIEW");

            entity.Property(e => e.UserAgent)
                .HasColumnName("User-Agent")
                .HasMaxLength(500);

            entity.Property(e => e.ViewDate).HasColumnType("datetime");

            entity.HasOne(d => d.Email)
                .WithMany(p => p.EmailActionView)
                .HasForeignKey(d => d.EmailId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EMAIL_ACTION_VIEW_EMAIL_NOTIFY");
        }
    }
}