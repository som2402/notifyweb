﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class CampaignsListsConfiguration : IEntityTypeConfiguration<CampaignsLists>
    {
        public void Configure(EntityTypeBuilder<CampaignsLists> entity)
        {
            entity.HasKey(e => new {e.CampaignId, e.ListId});

            entity.ToTable("CAMPAIGNS_LISTS");

            entity.Property(e => e.Id).HasMaxLength(50);

            entity.HasOne(d => d.Campaign)
                .WithMany(p => p.CampaignsLists)
                .HasForeignKey(d => d.CampaignId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CAMPAIGNS_LISTS_CAMPAIGNS");

            entity.HasOne(d => d.List)
                .WithMany(p => p.CampaignsLists)
                .HasForeignKey(d => d.ListId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CAMPAIGNS_LISTS_LISTS");
        }
    }
}