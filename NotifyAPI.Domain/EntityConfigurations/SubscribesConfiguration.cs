﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class SubscribesConfiguration : IEntityTypeConfiguration<Subscribes>
    {
        public void Configure(EntityTypeBuilder<Subscribes> entity)
        {
            entity.ToTable("SUBSCRIBES");
            entity.Property(e => e.AccountId).HasMaxLength(50);

            entity.Property(e => e.Code).HasMaxLength(255);

            entity.Property(e => e.Email).HasMaxLength(255);

            entity.Property(e => e.Name).HasMaxLength(255);

            entity.HasOne(d => d.List)
                .WithMany(p => p.Subscribes)
                .HasForeignKey(d => d.ListId)
                .HasConstraintName("FK_SUBSCRIBES_LISTS");
        }
    }
}