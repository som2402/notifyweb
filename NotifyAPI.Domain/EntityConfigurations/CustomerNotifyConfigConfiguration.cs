﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class CustomerNotifyConfigConfiguration : IEntityTypeConfiguration<CustomerNotifyConfig>
    {
        public void Configure(EntityTypeBuilder<CustomerNotifyConfig> entity)
        {
            entity.ToTable("CUSTOMER_NOTIFY_CONFIG");

            entity.Property(e => e.AccountId)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasComment("CustomerId");

            entity.Property(e => e.AppId)
                .IsRequired()
                .HasMaxLength(450)
                .HasComment("Mã App (VD: ichiba-portal-web)");

            entity.Property(e => e.NotifyConfigCode)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}