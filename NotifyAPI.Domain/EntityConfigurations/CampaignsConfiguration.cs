﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class CampaignsConfiguration : IEntityTypeConfiguration<Campaigns>
    {
        public void Configure(EntityTypeBuilder<Campaigns> entity)
        {
            entity.ToTable("CAMPAIGNS");

            entity.Property(e => e.CampaignType).HasMaxLength(255);

            entity.Property(e => e.Code).HasMaxLength(255);

            entity.Property(e => e.CreatedBy).HasMaxLength(255);

            entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            entity.Property(e => e.HtmlBody).HasColumnType("ntext");

            entity.Property(e => e.Sender).HasMaxLength(255);

            entity.Property(e => e.SenderCode).HasMaxLength(255);

            entity.Property(e => e.SenderTitle).HasMaxLength(255);

            entity.Property(e => e.TextBody).HasColumnType("ntext");

            entity.Property(e => e.Title).HasMaxLength(255);
        }
    }
}