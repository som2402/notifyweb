﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class WebNotifyConfiguration : IEntityTypeConfiguration<WebNotify>
    {
        public void Configure(EntityTypeBuilder<WebNotify> entity)
        {
            entity.ToTable("WEB_NOTIFY");

            entity.Property(e => e.Body).HasColumnType("ntext");

            entity.Property(e => e.CampaignList).HasMaxLength(255);

            entity.Property(e => e.CreatedBy).HasMaxLength(255);

            entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            entity.Property(e => e.CustomerId).HasMaxLength(50);

            entity.Property(e => e.Group).HasMaxLength(255);

            entity.Property(e => e.HtmlBody).HasColumnType("ntext");

            entity.Property(e => e.Link).HasMaxLength(255);

            entity.Property(e => e.RefCode).HasMaxLength(255);

            entity.Property(e => e.RefType).HasMaxLength(255);

            entity.Property(e => e.TextBody).HasColumnType("ntext");

            entity.Property(e => e.Title).HasMaxLength(255);

            entity.Property(e => e.Uid).HasColumnName("UID");
        }
    }
}