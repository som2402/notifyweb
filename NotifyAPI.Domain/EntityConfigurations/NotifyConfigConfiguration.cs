﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class NotifyConfigConfiguration : IEntityTypeConfiguration<NotifyConfig>
    {
        public void Configure(EntityTypeBuilder<NotifyConfig> entity)
        {
            entity.ToTable("NOTIFY_CONFIG");

            entity.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(255);

            entity.Property(e => e.Description).HasMaxLength(4000);

            entity.Property(e => e.GroupId).HasComment("Nhóm cấu hình (Enum: NotifyConfigGroup)");

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255);

            entity.HasOne(d => d.Group)
                .WithMany(p => p.NotifyConfig)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NOTIFY_CONFIG_NOTIFY_CONFIG_GROUP");
        }
    }
}