﻿using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ez.Notify.Domain.EntityConfigurations
{
    public class ListsConfiguration : IEntityTypeConfiguration<Lists>
    {
        public void Configure(EntityTypeBuilder<Lists> entity)
        {
            entity.ToTable("LISTS");
            entity.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(50);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}