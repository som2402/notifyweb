﻿using System.Reflection;
using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Ez.Notify.Domain
{
    public class DataNotifyDbContext : DbContext
    {
        public DataNotifyDbContext()
        {
        }

        public DataNotifyDbContext(DbContextOptions<DataNotifyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CustomerNotifyConfig> CustomerNotifyConfig { get; set; }
        public virtual DbSet<EmailActionClick> EmailActionClick { get; set; }
        public virtual DbSet<EmailActionView> EmailActionView { get; set; }
        public virtual DbSet<EmailAttachment> EmailAttachment { get; set; }
        public virtual DbSet<EmailNotify> EmailNotify { get; set; }
        public virtual DbSet<EmailSender> EmailSender { get; set; }
        public virtual DbSet<EmailSenderInbox> EmailSenderInbox { get; set; }
        public virtual DbSet<Emailtemplate> Emailtemplate { get; set; }
        public virtual DbSet<Entity.Notify> Notify { get; set; }
        public virtual DbSet<NotifyConfig> NotifyConfig { get; set; }
        public virtual DbSet<NotifyConfigGroup> NotifyConfigGroup { get; set; }
        public virtual DbSet<SubscribeNotify> SubscribeNotify { get; set; }
        public virtual DbSet<AppNotify> AppNotify { get; set; }
        public virtual DbSet<Campaigns> Campaigns { get; set; }
        public virtual DbSet<CampaignsLists> CampaignsLists { get; set; }
        public virtual DbSet<Lists> Lists { get; set; }
        public virtual DbSet<Subscribes> Subscribes { get; set; }
        public virtual DbSet<WebNotify> WebNotify { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.HasSequence("DataNotifySequence")
                .StartsAt(0)
                .HasMin(0);
            OnModelCreatingPartial(modelBuilder);
        }

        private void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
        }
    }
}