﻿namespace Ez.Notify.Domain.Entity
{
    public class EmailAttachment
    {
        public EmailAttachment(string fileName, byte[] fileBinary)
        {
            FileName = fileName;
            FileBinary = fileBinary;
        }

        public long Id { get; set; }
        public long EmailId { get; }
        public string FileName { get; }
        public byte[] FileBinary { get; }

        public virtual EmailNotify Email { get; set; }
    }
}