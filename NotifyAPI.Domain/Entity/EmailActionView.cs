﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class EmailActionView
    {
        public EmailActionView(long emailId, string userAgent)
        {
            EmailId = emailId;
            ViewDate = DateTime.UtcNow;
            UserAgent = userAgent;
        }

        public long Id { get; set; }
        public long EmailId { get; private set; }
        public DateTime ViewDate { get; private set; }
        public string UserAgent { get; private set; }

        public virtual EmailNotify Email { get; set; }
    }
}