﻿namespace Ez.Notify.Domain.Entity
{
    public class NotifyConfig
    {
        public NotifyConfig(string code, string name, int groupId, string description, bool active, int? required,
            int? desktopRequired, int? mobileRequired, int? webRequired, int? ord)
        {
            Code = code;
            Name = name;
            GroupId = groupId;
            Description = description;
            Active = active;
            Required = required;
            DesktopRequired = desktopRequired;
            MobileRequired = mobileRequired;
            WebRequired = webRequired;
            Ord = ord;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        /// <summary>
        ///     Nhóm cấu hình (Enum: NotifyConfigGroup)
        /// </summary>
        public int GroupId { get; set; }

        public string Description { get; set; }
        public bool Active { get; set; }
        public int? Required { get; set; }
        public int? WebRequired { get; set; }
        public int? MobileRequired { get; set; }
        public int? DesktopRequired { get; set; }
        public int? Ord { get; set; }
        public virtual NotifyConfigGroup Group { get; set; }

        public void Update(string code, string name, int groupId, string description, bool active, int? required,
            int? desktopRequired, int? mobileRequired, int? webRequired, int? ord)
        {
            Code = code;
            Name = name;
            GroupId = groupId;
            Description = description;
            Active = active;
            Required = required;
            DesktopRequired = desktopRequired;
            MobileRequired = mobileRequired;
            WebRequired = webRequired;
            Ord = ord;
        }
    }
}