﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class AppNotify
    {
        public AppNotify(string title, string htmlBody, string textBody, string link, string group, string createdBy,
            string customerId, string refCode, string refType)
        {
            Title = title;
            HtmlBody = htmlBody;
            TextBody = textBody;
            Link = link;
            Group = group;
            CreatedDate = DateTime.Now;
            CreatedBy = createdBy;
            CustomerId = customerId;
            RefCode = refCode;
            RefType = refType;
            Uid = Guid.NewGuid();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Group { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerId { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
        public bool? Read { get; set; }
        public Guid? Uid { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
        public string CampaignList { get; set; }

        public void UpdateRead()
        {
            Read = true;
        }
    }
}