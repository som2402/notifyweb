﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class EmailActionClick
    {
        public EmailActionClick(long emailId, string link)
        {
            EmailId = emailId;
            Link = link;
            ClickDate = DateTime.UtcNow;
        }

        public void Update()
        {
            ClickDate = DateTime.UtcNow;
        }

        public long Id { get; set; }
        public long EmailId { get; private set; }
        public string Link { get; private set; }
        public DateTime ClickDate { get; private set; }

        public virtual EmailNotify Email { get; set; }
    }
}