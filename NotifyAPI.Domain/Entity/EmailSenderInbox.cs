﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class EmailSenderInbox
    {
        public long Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime? SendDate { get; set; }
        public int? IdEmail { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string Headers { get; set; }
        public string Importance { get; set; }
        public string InReplyTo { get; set; }
        public string MimeVersion { get; set; }
        public string Priority { get; set; }
        public string BodyParts { get; set; }
        public string HtmlBody { get; set; }
        public string References { get; set; }
        public string MessageId { get; set; }
        public string MailBox { get; set; }
        public bool? IsAttactment { get; set; }
        public string Folder { get; set; }
        public string Sender { get; set; }
        public string ResentSender { get; set; }
        public string ResentFrom { get; set; }
        public string ReplyTo { get; set; }
        public string ResentReplyTo { get; set; }
        public string ResentTo { get; set; }
        public string ResentCc { get; set; }
        public string ResentBcc { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? ResentDate { get; set; }
        public string ResentMessageId { get; set; }
        public string TextBody { get; set; }
    }
}