﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class WebNotify
    {
        public WebNotify(string title, string body, string link, string group, string createdBy,
            string customerId, string refCode, string refType)
        {
            Title = title;
            Body = body;
            Link = link;
            Group = group;
            CreatedDate = DateTime.Now;
            CreatedBy = createdBy;
            CustomerId = customerId;
            RefCode = refCode;
            RefType = refType;
            Uid = Guid.NewGuid();
        }

        public long Id { get; private set; }
        public string Title { get; }
        public string Body { get; }
        public string Link { get; }
        public string Group { get; }
        public DateTime? CreatedDate { get; }
        public string CreatedBy { get; }
        public string CustomerId { get; }
        public string RefCode { get; }
        public string RefType { get; }
        public bool? Read { get; private set; }
        public Guid? Uid { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
        public string CampaignList { get; set; }

        public void UpdateRead()
        {
            Read = true;
        }
    }
}