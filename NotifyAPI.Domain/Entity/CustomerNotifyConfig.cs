﻿namespace Ez.Notify.Domain.Entity
{
    public class CustomerNotifyConfig
    {
        public CustomerNotifyConfig(string appId, string accountId, string notifyConfigCode, int sendEmail,
            int sendWeb, int sendMobile, int sendDesktop)
        {
            AppId = appId;
            AccountId = accountId;
            NotifyConfigCode = notifyConfigCode;
            SendEmail = sendEmail;
            SendWeb = sendWeb;
            SendMobile = sendMobile;
            SendDesktop = sendDesktop;
        }

        public int Id { get; set; }

        /// <summary>
        ///     Mã App (VD: ichiba-portal-web)
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        ///     CustomerId
        /// </summary>
        public string AccountId { get; set; }

        public string NotifyConfigCode { get; set; }
        public int SendEmail { get; set; }
        public int SendWeb { get; set; }
        public int SendMobile { get; set; }
        public int SendDesktop { get; set; }

        public void Update(string appId, string accountId, string notifyConfigCode, int sendEmail,
            int sendWeb, int sendMobile, int sendDesktop)
        {
            AppId = appId;
            AccountId = accountId;
            NotifyConfigCode = notifyConfigCode;
            SendEmail = sendEmail;
            SendWeb = sendWeb;
            SendMobile = sendMobile;
            SendDesktop = sendDesktop;
        }
    }
}