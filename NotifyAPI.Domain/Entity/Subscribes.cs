﻿namespace Ez.Notify.Domain.Entity
{
    public class Subscribes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public string AccountId { get; set; }
        public int? ListId { get; set; }

        public virtual Lists List { get; set; }
    }
}