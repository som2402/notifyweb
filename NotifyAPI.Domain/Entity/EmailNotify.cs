﻿using System;
using System.Collections.Generic;

namespace Ez.Notify.Domain.Entity
{
    public class EmailNotify
    {
        public EmailNotify(string subject, string textBody, string htmlBody, string to, string cc, string bcc,
            string sender, string senderTitle, string senderCode, string refCode, string refType,
            DateTime? createdDate, string createdBy, string sendMessage, DateTime? sentDate, int? sentFailCount,
            int? zone, string accountId)
        {
            Subject = subject;
            TextBody = textBody;
            HtmlBody = htmlBody;
            To = to;
            Cc = cc;
            Bcc = bcc;
            Sender = sender;
            SenderTitle = senderTitle;
            SenderCode = senderCode;
            RefCode = refCode;
            RefType = refType;
            CreatedDate = createdDate;
            CreatedBy = createdBy;
            SendMessage = sendMessage;
            SentDate = sentDate;
            SentFailCount = sentFailCount;
            UID = Guid.NewGuid();
            Zone = zone;
            AccountId = accountId;
        }

        public void UpdateHtmlBody(string htmlBody)
        {
            HtmlBody = htmlBody;
        }

        public long Id { get; set; }
        public string Subject { get; private set; }
        public string TextBody { get; private set; }
        public string HtmlBody { get; private set; }
        public string To { get; private set; }
        public string Cc { get; private set; }
        public string Bcc { get; private set; }
        public string Sender { get; private set; }
        public string SenderTitle { get; private set; }
        public string SenderCode { get; private set; }
        public string RefCode { get; private set; }
        public string RefType { get; private set; }
        public DateTime? CreatedDate { get; private set; }
        public string CreatedBy { get; private set; }
        public int? SendStatus { get; private set; }
        public string SendMessage { get; private set; }
        public DateTime? SentDate { get; private set; }
        public int? SentFailCount { get; private set; }
        public Guid UID { get; set; }
        public int? Zone { get; set; }
        public string AccountId { get; set; }
        public string CampaignList { get; set; }
        /// <summary>
        /// 0 = tin chưa xem
        /// 1 = đã xem
        /// 2 = đã xóa
        /// </summary>
        public int? Status { get; set; }


        public virtual ICollection<EmailActionClick> EmailActionClick { get; set; }
        public virtual ICollection<EmailActionView> EmailActionView { get; set; }
        public virtual ICollection<EmailAttachment> EmailAttachment { get; set; }

        public void Update(string subject, string textBody, string htmlBody, string to, string cc, string bcc,
            string sender, string senderTitle, string senderCode, string refCode, string refType,
            DateTime? createdDate, string createdBy, string sendMessage, DateTime? sentDate, int? sentFailCount)
        {
            Subject = subject;
            TextBody = textBody;
            HtmlBody = htmlBody;
            To = to;
            Cc = cc;
            Bcc = bcc;
            Sender = sender;
            SenderTitle = senderTitle;
            SenderCode = senderCode;
            RefCode = refCode;
            RefType = refType;
            CreatedDate = createdDate;
            CreatedBy = createdBy;
            SendMessage = sendMessage;
            SentDate = sentDate;
            SentFailCount = sentFailCount;
        }

        public void UpdateStatus(int? sendStatus)
        {
            SendStatus = sendStatus;
            SentDate = DateTime.UtcNow;
        }

        public void UpdateError(string message, int? sendStatus)
        {
            SentFailCount += 1;
            SendMessage = message;
            SendStatus = sendStatus;
        }
    }
}