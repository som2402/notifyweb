﻿using System;
using System.Collections.Generic;

namespace Ez.Notify.Domain.Entity
{
    public class Campaigns
    {
        public Campaigns()
        {
            CampaignsLists = new HashSet<CampaignsLists>();
        }

        public Campaigns(string code, string title, string description, string htmlBody, string textBody, int? status,
            DateTime? createdDate, string createdBy, string campaignType, string sender, string senderTitle,
            string senderCode, int? zone)
        {
            Code = code;
            Title = title;
            Description = description;
            HtmlBody = htmlBody;
            TextBody = textBody;
            Status = status;
            CreatedDate = createdDate;
            CreatedBy = createdBy;
            CampaignType = campaignType;
            Sender = sender;
            SenderTitle = senderTitle;
            SenderCode = senderCode;
            Zone = zone;
        }

        public int Id { get; private set; }
        public string Code { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string HtmlBody { get; private set; }
        public string TextBody { get; private set; }
        public int? Status { get; private set; }
        public DateTime? CreatedDate { get; }
        public string CreatedBy { get; }
        public string CampaignType { get; private set; }
        public string Sender { get; private set; }
        public string SenderTitle { get; private set; }
        public string SenderCode { get; private set; }
        public int? Zone { get; private set; }

        public virtual ICollection<CampaignsLists> CampaignsLists { get; set; }

        public void Update(string code, string title, string description, string htmlBody, string textBody, int? status,
            string campaignType, string sender, string senderTitle,
            string senderCode, int? zone)
        {
            Code = code;
            Title = title;
            Description = description;
            HtmlBody = htmlBody;
            TextBody = textBody;
            Status = status;
            CampaignType = campaignType;
            Sender = sender;
            SenderTitle = senderTitle;
            SenderCode = senderCode;
            Zone = zone;
        }
    }
}