﻿using System.Collections.Generic;

namespace Ez.Notify.Domain.Entity
{
    public class Lists
    {
        public Lists()
        {
            CampaignsLists = new List<CampaignsLists>();
            Subscribes = new List<Subscribes>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Sub { get; set; }

        public virtual List<CampaignsLists> CampaignsLists { get; set; }

        public virtual List<Subscribes> Subscribes { get; set; }
    }
}