﻿using System;

namespace Ez.Notify.Domain.Entity
{
    public class CampaignsLists
    {
        public CampaignsLists(int campaignId, int listId, int? status, int? sub)
        {
            Id = Guid.NewGuid().ToString();
            CampaignId = campaignId;
            ListId = listId;
            Status = status;
            Sub = sub;
        }

        public int CampaignId { get; }
        public int ListId { get; }
        public int? Status { get; private set; }
        public int? Sub { get; private set; }
        public string Id { get; set; }

        public virtual Campaigns Campaign { get; set; }
        public virtual Lists List { get; set; }

        public void UpdateStatus(int? status)
        {
            Status = status;
        }
    }
}