﻿using System.Threading.Tasks;

namespace Ez.IS4.API.Driver
{
    public interface IAuthorizeClient
    {
        Task<string> GetAuthorizeToken();
    }
}