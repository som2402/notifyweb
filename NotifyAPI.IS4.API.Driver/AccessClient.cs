﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ez.IS4.API.Driver.Models.Response;
using Shared.ResilientHttp;

namespace Ez.IS4.API.Driver
{
    public class AccessConfig
    {
        private const string GetResourcesPath = "/Access/GetResources";

        private const string CheckPermissionPath = "/Access/CheckPermission";
        private string _checkPermission;
        private string _getResources;

        public AccessConfig()
        {
            GetResources = null;
            CheckPermission = null;
        }

        public string BaseApi { get; set; }

        public string GetResources
        {
            get => _getResources;
            set => _getResources = BuildFullUrl(ValueOrDefault(value, GetResourcesPath));
        }

        public string CheckPermission
        {
            get => _checkPermission;
            set => _checkPermission = BuildFullUrl(ValueOrDefault(value, CheckPermissionPath));
        }

        private string BuildFullUrl(string path)
        {
            return $"{BaseApi}{path}";
        }

        private string ValueOrDefault(string value, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(value) ? defaultValue : value;
        }
    }

    public class AccessClient : BaseClient
    {
        private readonly AccessConfig _accessConfig;

        public AccessClient(IHttpClient httpClient, IAuthorizeClient authorizeClient, AccessConfig accessConfig) : base(
            httpClient, authorizeClient)
        {
            _accessConfig = accessConfig;
        }

        public async Task<IList<Resource>> GetResources(string groupResourceKey)
        {
            var url = $"{_accessConfig.GetResources}/{groupResourceKey}";
            var response = await Get<IList<Resource>>(url);

            return response;
        }

        public async Task<bool> CheckPermission(string groupResourceKey, string resourceKey, params string[] actions)
        {
            var action = string.Join(',', actions);
            var url = $"{_accessConfig.CheckPermission}/{groupResourceKey}/{resourceKey}/{action}";
            var response = await Get<bool>(url);

            return response;
        }
    }
}