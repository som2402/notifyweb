﻿namespace Ez.Notify.Common.Enums
{
    public enum SendStatus
    {
        Sent,
        Unsent
    }
}