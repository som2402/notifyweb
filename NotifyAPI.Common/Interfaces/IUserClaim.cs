﻿using System;

namespace Ez.Notify.Common.Interfaces
{
    public interface IUserContext
    {
        Guid UserId { get; }
        string Name { get; }
        string UserEmail { get; }
    }
}