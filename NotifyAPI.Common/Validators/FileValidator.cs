﻿using System.Collections.Generic;
using System.IO;
using Ez.Notify.Common.Constants;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Shared.Validators;

namespace Ez.Notify.Common.Validators
{
    public class FileValidator : BaseValidator<IFormFile>
    {
        public FileValidator()
        {
            RuleFor(x => Path.GetExtension(x.FileName)).NotNull()
                .Must(x => !FileExtentions.ListFileExtensions.Contains(x))
                .WithState(x => new Dictionary<string, string> {{"FILE_EXTENSIONS_WRONG", x.ContentType}})
                .WithErrorCode("FILE_EXTENSIONS_WRONG");
        }
    }
}