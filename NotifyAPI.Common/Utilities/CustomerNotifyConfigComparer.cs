﻿using System;
using System.Collections.Generic;
using Ez.Notify.Domain.Entity;

namespace Ez.Notify.Common.Utilities
{
    public class CustomerNotifyConfigComparer : IEqualityComparer<CustomerNotifyConfig>
    {
        public bool Equals(CustomerNotifyConfig x, CustomerNotifyConfig y)
        {
            // Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            // Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;
            return x.AppId == y.AppId && x.AccountId == y.AccountId &&
                   x.NotifyConfigCode == y.NotifyConfigCode && x.SendEmail == y.SendEmail && x.SendWeb == y.SendWeb &&
                   x.SendMobile == y.SendMobile && x.SendDesktop == y.SendDesktop;
        }

        public int GetHashCode(CustomerNotifyConfig obj)
        {
            return HashCode.Combine(obj.Id, obj.AppId, obj.AccountId, obj.NotifyConfigCode, obj.SendEmail, obj.SendWeb,
                obj.SendMobile, obj.SendDesktop);
        }
    }
}