﻿using System.Collections.Generic;
using Shared.Services;

namespace Ez.Notify.Common.Models
{
    public class SendEmailNotifyToMessageQueueRequest
    {
        public SendEmailNotifyToMessageQueueRequest(long emailId, List<FileAttachment> attachments)
        {
            EmailId = emailId;
            Attachments = attachments;
        }

        public long EmailId { get; }
        public List<FileAttachment> Attachments { get; }
    }
}