﻿namespace Ez.Notify.Common.Constants
{
    public static class ClaimType
    {
        public const string UserId = "sub";
        public const string Email = "email";
        public const string Name = "name";
    }
}