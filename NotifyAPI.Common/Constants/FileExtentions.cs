﻿using System.Collections.Generic;

namespace Ez.Notify.Common.Constants
{
    public static class FileExtentions
    {
        public static List<string> ListFileExtensions = new List<string>
        {
            ".ade", ".adp", ".apk", ".appx", ".appxbundle", ".bat", ".cab", ".chm", ".cmd", ".com", ".cpl", ".dll",
            ".dmg", ".exe", ".hta", ".ins", ".isp", ".iso", ".jar", ".js", ".jse", ".lib", ".lnk", ".mde", ".msc",
            ".msi", ".msix",
            ".msixbundle", ".msp", ".mst", ".nsh", ".pif", ".ps1", ".scr", ".sct", ".shb", ".sys", ".vb", ".vbe",
            ".vbs", ".vxd",
            ".wsc", ".wsf", ".wsh"
        };
    }
}