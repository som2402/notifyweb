﻿namespace Ez.Notify.Common.Constants
{
    public static class EmailNotifyStatus
    {
        public const int UnRead = 0;
        public const int Read = 1;
        public const int Deleted = 2;
    }
}