﻿namespace Ez.Notify.Common.Constants
{
    public static class SendStatusConstant
    {
        public const int SubmittedSuccessfully = 1;
        public const int SubmitAnError = 2;
        public const int UserRefusedToSubmit = 3;
    }
}