﻿namespace Ez.Notify.Common.Constants
{
    public class CampaignType
    {
        public const string Web = "WEB";
        public const string App = "APP";
        public const string Email = "EMAIL";
    }
}