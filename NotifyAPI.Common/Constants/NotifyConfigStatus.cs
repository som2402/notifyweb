﻿namespace Ez.Notify.Common.Constants
{
    public static class NotifyConfigStatus
    {
        public const int Active = 1;
        public const int Off = 4;
        public const int InActive = 3;
        public const int Developing = 2;
    }
}