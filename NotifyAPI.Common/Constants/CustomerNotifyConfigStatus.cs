﻿namespace Ez.Notify.Common.Constants
{
    public class CustomerNotifyConfigStatus
    {
        public const int Active = 1;
        public const int InActive = 3;
        public const int Developing = 2;
    }
}