﻿namespace Ez.Notify.Common.Constants
{
    public static class RefTypeCampaigns
    {
        public const string Web = "CAMPAIGNS_WEB";
        public const string App = "CAMPAIGNS_APP";
        public const string Email = "CAMPAIGNS_EMAIL";
    }
}