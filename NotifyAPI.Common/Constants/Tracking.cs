﻿namespace Ez.Notify.Common.Constants
{
    public static class Tracking
    {
        public const string FormatLinkClickTracking = "{0}/{1}?eId={2}&returnUrl={3}&hash={4}";
        public const string LinkTracking = "Tracking/TrackPressLink";
        
        
        public const string FormatOpenEmailTracking = "{0}/{1}?eId={2}&hash={3}";
        public const string LinkOpenEmailTracking = "Tracking/OpenEmail";
    }
}