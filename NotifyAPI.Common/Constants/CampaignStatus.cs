﻿namespace Ez.Notify.Common.Constants
{
    public static class CampaignStatus
    {
        public const int CreateNew = 0;
        public const int Delivering = 1;
        public const int Delivered = 2;
    }
}