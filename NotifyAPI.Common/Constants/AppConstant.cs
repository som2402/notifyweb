﻿namespace Ez.Notify.Common.Constants
{
    public static class AppConstant
    {
        public const string IchibaAppId = "ichiba-web";
        public const string EzbuyAppId = "ezbuy-web";
    }
}