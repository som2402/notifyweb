﻿using Shared.Common.JTable;

namespace Ez.Notify.Private.Api.Request.EmailNotify
{
    public class SearchEmailNotifyJTableRequest : JTableModel
    {
        public string Query { get; set; }
    }
}