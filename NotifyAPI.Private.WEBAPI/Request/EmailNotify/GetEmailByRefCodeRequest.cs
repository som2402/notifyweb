﻿namespace Ez.Notify.Private.Api.Request.EmailNotify
{
    public class GetEmailByRefCodeRequest
    {
        public string RefCode { get; set; }
    }
}