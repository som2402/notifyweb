﻿using Shared.Common;

namespace Ez.Notify.Private.Api.Request.EmailNotify
{
    public class GetListEmailNotifyRequest : SortModel
    {
        public string RefType { get; set; }
        public string AccountId { get; set; }
        public int? Status { get; set; }
    }
}