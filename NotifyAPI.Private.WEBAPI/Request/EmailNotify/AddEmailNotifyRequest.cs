﻿using System;

namespace Ez.Notify.Private.Api.Request.EmailNotify
{
    public class AddEmailNotifyRequest
    {
        public string Subject { get; set; }
        public string TextBody { get; set; }
        public string HtmlBody { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Sender { get; set; }
        public string SenderTitle { get; set; }
        public string SenderCode { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
        public string CustomerId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string SendMessage { get; set; }
        public DateTime? SentDate { get; set; }

        public int? SentCount { get; set; }
    }
}