﻿namespace Ez.Notify.Private.Api.Request.EmailNotify
{
    public class UpdateEmailNotifyStatusRequest
    {
        public int? Status { get; set; }
        public string AccountId { get; set; }
        public long[] EmailNotifyIds { get; set; }
    }
}