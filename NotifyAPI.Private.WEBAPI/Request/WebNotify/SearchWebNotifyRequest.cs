﻿namespace Ez.Notify.Private.Api.Request.WebNotify
{
    public class SearchWebNotifyRequest
    {
        public string CustomerId { get; set; }
        public string Group { get; set; }
    }
}