﻿namespace Ez.Notify.Private.Api.Request.WebNotify
{
    public class AddWebNotifyRequest
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Link { get; set; }
        public string Group { get; set; }
        public string CustomerId { get; set; }
        public string CreateBy { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
    }
}