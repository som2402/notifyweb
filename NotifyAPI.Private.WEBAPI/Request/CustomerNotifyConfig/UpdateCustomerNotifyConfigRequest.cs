﻿using System.Collections.Generic;

namespace Ez.Notify.Private.Api.Request.CustomerNotifyConfig
{
    public class UpdateCustomerNotifyConfigRequest
    {
        public string AppId { get; set; }
        public string AccountId { get; set; }
        public List<Domain.Entity.CustomerNotifyConfig> CustomerNotifyConfigs { get; set; }
    }
}