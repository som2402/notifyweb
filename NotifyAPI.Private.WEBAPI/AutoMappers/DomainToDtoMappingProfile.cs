﻿using AutoMapper;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.EmailAttachments.Queries;
using Ez.NotifyAPI.Application.EmailNotifies.Queries;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Queries;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;
using Ez.NotifyAPI.Application.WebNotifies.Queries;

namespace Ez.Notify.Private.Api.AutoMappers
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<NotifyConfigGroup, NotifyConfigGroupDto>();
            CreateMap<NotifyConfig, NotifyConfigDto>();
            CreateMap<EmailNotify, EmailNotifyDto>();
            CreateMap<EmailAttachment, EmailAttachmentDto>();
            CreateMap<WebNotify, WebNotifyDto>();
        }
    }
}