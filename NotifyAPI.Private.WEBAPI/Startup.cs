using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ez.Notify.Private.Api.Helpers;
using Shared.Filters;
using System;

namespace Ez.Notify.Private.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext(Configuration)
                .AddConfig(Configuration)
                .AddHttpClient(Configuration)
                .AddAuthentication(Configuration)
                .AddAuthorization(Configuration)
                .AddRabbitMq(Configuration)
                .AddEvents(Configuration)
                .AddServices()
                .AddMediatREvent()
                .AddRepositories()
                .AddUnitOfWork()
                .AddUserClaim()
                .AddBehaviour()
                .AddMapper()
                .AddValidator()
                .AddCorsPolicy()
                .AddSwagger();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddControllers(options => options.Filters.Add(new ApiExceptionFilter())).AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            app.UseCorsPolicy();
            app.UseSwaggerUi();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}