﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ez.IS4.API.Driver;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ez.Notify.Application.CustomerNotifyConfigs.Command;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Ez.Notify.Private.Api.Request.CustomerNotifyConfig;

namespace Ez.Notify.Private.Api.Controllers
{
    public class CustomerNotifyConfigController : BaseController
    {
        public CustomerNotifyConfigController()
        {
        }

        [HttpPost("add-update-customer-notify")]
        [ProducesResponseType(typeof(BaseEntityResponse<List<CustomerNotifyConfig>>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(UpdateCustomerNotifyConfigRequest request)
        {
            var result = await Mediator.Send(new AddCustomerNotifyConfigCommand(request.AppId,
                request.AccountId, request.CustomerNotifyConfigs));
            return Ok(result);
        }
    }
}