﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ez.Notify.Private.Api.Request.EmailNotify;
using Ez.NotifyAPI.Application.EmailNotifies.Command;
using Ez.NotifyAPI.Application.EmailNotifies.Command.Request;
using Ez.NotifyAPI.Application.EmailNotifies.Queries;
using Shared.BaseModel;
using Shared.Extensions;
using Shared.Validators.Regexs;

namespace Ez.Notify.Private.Api.Controllers
{
    public class EmailNotifyController : BaseController
    {
        [HttpPost("send_email")]
        [Consumes("multipart/form-data")]
        [AllowAnonymous]
        public async Task<IActionResult> SendEmail([FromForm] SendEmailNotifyRequest request)
        {
            Regex emailRegex = new Regex(Regexes.Email);
            var bcc = new string[] { };
            var cc = new string[] { };
            if (!string.IsNullOrEmpty(request.Bcc))
            {
                bcc = request.Bcc.Split(",");
            }

            if (!string.IsNullOrEmpty(request.Cc))
            {
                cc = request.Cc.Split(",");
            }

            bcc = bcc.Where(x => emailRegex.Match(x).Success).ToArray();
            cc = cc.Where(x => emailRegex.Match(x).Success).ToArray();

            var result =
                await Mediator.Send(new SendEmailNotifyCommand(request.To,
                    bcc, cc, request.SenderCode,
                    request.SenderTitle, request.RefCode, request.RefType, request.Subject,
                    request.Html,
                    request.Attachments, request.Zone, request.AccountId));
            return Ok(true);
        }

        [HttpPost("send_emails")]
        [Consumes("multipart/form-data")]
        [AllowAnonymous]
        public async Task<IActionResult> SendEmail([FromForm] List<SendEmailNotifyRequest> request)
        {
            var result =
                await Mediator.Send(new SendListEmailNotifyCommand(request));
            return Ok(true);
        }

        [HttpPost("get_by_ref_code")]
        [ProducesResponseType(typeof(BaseEntityResponse<List<EmailNotifyDto>>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByRefCode(GetEmailByRefCodeRequest request)
        {
            var result = await Mediator.Send(request.Map<GetEmailNotifyByRefCodeQuery>());
            return Ok(result);
        }

        [HttpGet("{customerId}")]
        [ProducesResponseType(typeof(BaseEntityResponse<List<EmailNotifyDto>>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByCustomerId(string customerId)
        {
            var result = await Mediator.Send(new GetEmailNotifyByCustomerIdQuery(customerId));
            return Ok(result);
        }

        [HttpPost("list-by-accountid")]
        [ProducesResponseType(typeof(PagingResponse<IList<EmailNotifyListDto>>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetList(GetListEmailNotifyRequest request)
        {
            var result = await Mediator.Send(new GetListEmailNotifyQuery(request.PageIndex, request.PageSize,
                request.AccountId, request.RefType, request.Status, request.Sorts));
            return Ok(result);
        }

        [HttpGet("get-by-id/{id}/{accountId}")]
        [ProducesResponseType(typeof(BaseEntityResponse<EmailNotifyDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(long id, string accountId)
        {
            var result = await Mediator.Send(new GetEmailNotifyQuery(id, accountId));
            return Ok(result);
        }

        [HttpPost("update-status")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateStatus(UpdateEmailNotifyStatusRequest request)
        {
            var result = await Mediator.Send(request.Map<UpdateEmailNotifyStatusCommand>());
            return Ok(result);
        }
    }
}