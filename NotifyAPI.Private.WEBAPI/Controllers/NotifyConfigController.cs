﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ez.IS4.API.Driver;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.BaseModel;
using Shared.BaseModel.Grids;

namespace Ez.Notify.Private.Api.Controllers
{
    public class NotifyConfigController : BaseController
    {
      
        [HttpGet("{accountId}")]
        [ProducesResponseType(typeof(BaseEntityResponse<List<NotifyConfigAndNotifyConfigGroupDto>>),
            StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(string accountId)
        {
            var result = await Mediator.Send(new GetNotifyConfigAndNotifyConfigGroupQuery(accountId));
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetGrid(GridParameters parameters)
        {
            var result = await Mediator.Send(new GetGridNotifyConfigQuery(parameters));
            return Ok(result);
        }
    }
}