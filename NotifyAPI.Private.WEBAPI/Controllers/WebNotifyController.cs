﻿using System.Threading.Tasks;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Private.Api.Request.WebNotify;
using Ez.NotifyAPI.Application.WebNotifies.Commands;
using Ez.NotifyAPI.Application.WebNotifies.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Ez.Notify.Private.Api.Controllers
{
    public class WebNotifyController : BaseController
    {
        private readonly IUserContext _userContext;
        public WebNotifyController()
        {
        }
        [HttpPost]
        public async Task<IActionResult> Post(AddWebNotifyRequest request)
        {
            var result = await Mediator.Send(new AddWebNotifyCommand(request.Title, request.Body, request.Link,
                request.Group, _userContext.UserId.ToString(), request.CustomerId, request.RefCode, request.RefType));
            return Ok(result);
        }

        [HttpPost("add")]

        public async Task<IActionResult> Add(AddWebNotifyRequest request)
        {
            var result = await Mediator.Send(new AddWebNotifyCommand(request.Title, request.Body, request.Link,
                request.Group, request.CreateBy, request.CustomerId, request.RefCode, request.RefType));
            return Ok(result);
        }

        [HttpGet("get_by_customer_id")]
        public async Task<IActionResult> GetByCustomerId(string customerId)
        {
            var result = await Mediator.Send(new GetListWebNotifyByCustomerIdQuery(customerId));
            return Ok(result);
        }


    }
}
