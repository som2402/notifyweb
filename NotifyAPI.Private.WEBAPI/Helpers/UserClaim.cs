﻿using System;
using Ez.Notify.Common.Constants;
using Ez.Notify.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Ez.Notify.Private.Api.Helpers
{
    public class UserClaim : IUserContext
    {
        public UserClaim(IHttpContextAccessor contextAccessor)
        {
            var claims = contextAccessor.HttpContext?.User;
            if (claims == null)
                UserId = Guid.Empty;
            else if (claims?.FindFirst(ClaimType.UserId)?.Value == null)
                UserId = Guid.Empty;
            else
                UserId = Guid.Parse(claims?.FindFirst(ClaimType.UserId)?.Value!);

            Name = claims?.FindFirst(ClaimType.Name)?.Value != null
                ? claims?.FindFirst(ClaimType.Name)?.Value
                : string.Empty;
            UserEmail = claims?.FindFirst(ClaimType.Email)?.Value != null
                ? claims?.FindFirst(ClaimType.Email)?.Value
                : string.Empty;
        }

        public Guid UserId { get; }
        public string Name { get; }
        public string UserEmail { get; }
    }
}