﻿using System;
using System.Data;
using System.Net.Http;
using System.Reflection;
using Ez.IS4.API.Driver;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Customer.Driver;
using Ez.Notify.Domain;
using Ez.Notify.MQ.Driver;
using Ez.NotifyAPI.Application.Config;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.EmailActionClicks.Repositories;
using Ez.NotifyAPI.Application.EmailActionViews.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Shared.Behaviours;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Ioc;
using Shared.ResilientHttp;
using Shared.Services;
using Assembly = Ez.NotifyAPI.Application.Assembly;

namespace Ez.Notify.Private.Api.Helpers
{
    public static class StartupHelpers
    {
        public static readonly string AssemblyName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                ProxiesExtensions.UseLazyLoadingProxies(opt)
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            }).AddScoped<IDbConnection>(sp =>
            {
                var config = sp.GetRequiredService<IConfiguration>();
                var connection =
                    new SqlConnection(config.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
                connection.Open();
                return connection;
            });
            return services;
        }

        public static IServiceCollection AddHttpClient(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddHttpClient(configuration["ClientApi"]).ConfigurePrimaryHttpMessageHandler(() =>
            //{
            //    var handler = new HttpClientHandler
            //    {
            //        ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true
            //    };
            //    return handler;
            //});
            //services.AddTransient<IBaseApiClient, BaseApiClient>();
            services.AddScoped<CustomerClient>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddClassesInterfaces(typeof(INotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(INotifyConfigGroupRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            services.AddClassesInterfaces(typeof(ICustomerNotifyConfigRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionClickRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailActionViewRepository).Assembly);
            // services.AddTransient<IRepository<NotifyConfigGroup>, Repository<DataNotifyDbContext, NotifyConfigGroup>>();
            // services.AddTransient<IRepository<NotifyConfig>, Repository<DataNotifyDbContext, NotifyConfig>>();
            // services.AddTransient<IRepository<EmailNotify>, Repository<DataNotifyDbContext, EmailNotify>>();
            // services.AddTransient<IRepository<EmailAttachment>, Repository<DataNotifyDbContext, EmailAttachment>>();
            // services.AddTransient<IRepository<WebNotify>, Repository<DataNotifyDbContext, WebNotify>>();
            // services.AddTransient<IRepository<EmailSender>, Repository<DataNotifyDbContext, EmailSender>>();
            // services.AddTransient<IRepository<AppNotify>, Repository<DataNotifyDbContext, AppNotify>>();
            // services.AddTransient<IRepository<Campaigns>, Repository<DataNotifyDbContext, Campaigns>>();
            // services.AddTransient<IRepository<CampaignsLists>, Repository<DataNotifyDbContext, CampaignsLists>>();
            // services.AddTransient<IRepository<Subscribes>, Repository<DataNotifyDbContext, Subscribes>>();
            // services.AddTransient<IRepository<Lists>, Repository<DataNotifyDbContext, Lists>>();
            // services
            //     .AddTransient<IRepository<CustomerNotifyConfig>, Repository<DataNotifyDbContext, CustomerNotifyConfig>
            //     >();

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddUserClaim(this IServiceCollection services)
        {
            services.AddScoped<IUserContext, UserClaim>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            // services.AddTransient<AccessClient>();
            services.AddTransient<MessageQueueClient>();
            services.AddSingleton<IAuthorizeClient, AuthorizeClientImplement>();
            services.AddTransient<IEmailService, EmailSenderService>();
            services.AddTransient<ISequenceService, SequenceService>();
            return services;
        }

        public static IServiceCollection AddRabbitMq(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddEvents(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddMediatREvent(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Assembly).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(Shared.Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddBehaviour(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            return services;
        }

        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(typeof(Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            // var mapperConfig = new MapperConfiguration(cfg =>
            // {
            //     cfg.AddProfile(new DomainToDtoMappingProfile());
            //     cfg.AddProfile(new CommandToDomainMappingProfile());
            //     cfg.AddProfile(new RequestToQueryMappingProfile());
            // });
            // services.AddSingleton(mapperConfig.CreateMapper().RegisterMap());
            return services;
        }

        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(sp =>
                configuration.GetSection("MessageQueue:MessageQueueConfig").Get<MessageQueueConfig>());
            services.AddSingleton(sp => configuration.GetSection("Authorize").Get<AuthorizeConfig>());
            services.AddSingleton(sp => configuration.GetSection("CustomerApi:CustomerApiConfig").Get<CustomerConfig>());
            return services;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {


            services.AddSingleton<IResilientHttpClientFactory, ResilientHttpClientFactory>(sp =>
            {
                const int retryCount = 3;
                const int exceptionsAllowedBeforeBreaking = 5;
                return new ResilientHttpClientFactory(null, exceptionsAllowedBeforeBreaking, retryCount);
            });
            services.AddSingleton<IHttpClient, ResilientHttpClient>(sp =>
                sp.GetService<IResilientHttpClientFactory>().CreateResilientHttpClient());
            return services;
        }

        public static IServiceCollection AddAuthorization(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static IServiceCollection AddApiVersioning(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "IC Notify private api v1",
                    Version = "v1"
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new[] {"Bearer"}
                    }
                });
                c.CustomSchemaIds(x => x.FullName);
            });
        }

        public static IServiceCollection AddCorsPolicy(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("MyAllowSpecificOrigins",
                    builder =>
                    {
                        builder
                            .WithOrigins("http://localhost:4200", "http://so.ichibajp.com")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                            .SetIsOriginAllowed((hosts) => true);
                    });
            });

            //services.AddSignalR(options => { options.KeepAliveInterval = TimeSpan.FromSeconds(5); }).AddMessagePackProtocol();

            return services;
        }

        public static void UseCorsPolicy(this IApplicationBuilder app)
        {
            app.UseCors("MyAllowSpecificOrigins");
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "IC Notify private api v1");
                c.DocumentTitle = "IC Notify private api v1";
            });
        }
    }
}