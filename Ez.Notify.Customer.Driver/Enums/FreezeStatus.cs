﻿namespace Ez.Notify.Customer.Driver.Enums
{
    public enum FreezeStatus
    {
        Freeze = 1,
        Cancel = 0
    }
}
