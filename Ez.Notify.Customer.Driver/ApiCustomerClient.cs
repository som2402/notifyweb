﻿using System.Threading.Tasks;
using Ez.IS4.API.Driver;
using Ez.Notify.Customer.Driver.Response;
using Shared.BaseModel;
using Shared.ResilientHttp;

namespace Ez.Notify.Customer.Driver
{
    public class CustomerConfig
    {
        private const string GetCustomerByAccountIdPath = "/Customer/GetByAccountId";
        private string _getCustomerByAccountId;

        public CustomerConfig()
        {
            GetCustomerByAccountId = null;
        }

        public string BaseApi { get; set; }

        public string GetCustomerByAccountId
        {
            get => _getCustomerByAccountId;
            set => _getCustomerByAccountId = BuildFullUrl(ValueOrDefault(value, GetCustomerByAccountIdPath));
        }

        private string BuildFullUrl(string path)
        {
            return $"{BaseApi}{path}";
        }

        private string ValueOrDefault(string value, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(value) ? defaultValue : value;
        }
    }

    public class CustomerClient : BaseClient
    {
        private readonly CustomerConfig customerConfig;

        public CustomerClient(IHttpClient httpClient, IAuthorizeClient authorizeClient,
            CustomerConfig customerConfig) : base(httpClient, authorizeClient)
        {
            this.customerConfig = customerConfig;
        }


        public async Task<BaseEntityResponse<CustomerDto>> GetCustomerByAccountId(string accountId)
        {
            var url = customerConfig.GetCustomerByAccountId + "/" + accountId;
            var response = await Get<BaseEntityResponse<CustomerDto>>(url);
            return response;
        }
    }
}