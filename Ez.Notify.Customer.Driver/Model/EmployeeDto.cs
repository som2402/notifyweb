﻿namespace Ez.Notify.Customer.Driver.Model
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string AccountId { get; set; }
        public string Roles { get; set; }
    }
}
