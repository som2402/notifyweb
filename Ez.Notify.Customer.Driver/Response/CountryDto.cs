﻿namespace Ez.Notify.Customer.Driver.Response
{
    public class CountryDto
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
