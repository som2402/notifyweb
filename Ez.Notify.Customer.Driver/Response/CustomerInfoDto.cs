﻿using System;
using System.Collections.Generic;

namespace Ez.Notify.Customer.Driver.Response
{
    public class CustomerInfoDto
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        public string AccountId { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public DateTime? Birthday { get; set; }
        public string Avatar { get; set; }
        public string Website { get; set; }
        public string IdImages { get; set; }
        public string IdNumber { get; set; }
        public string IdName { get; set; }
        public string IdAddress { get; set; }
        public string IdPermanentAddress { get; set; }
        public DateTime? IdBirthDay { get; set; }
        public DateTime? IdIssuedDate { get; set; }
        public string IdIssuedBy { get; set; }
        public string Country { get; set; }
        public string CountryDisplay { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CareBy { get; set; }
        public bool? BidActive { get; set; }
        public string Code { get; set; }
        public DateTime? CodeCreatedDate { get; set; }
        public string Group { get; set; }
        public string TranCode { get; set; }
        public bool? TranActive { get; set; }
        public int? SyncAccStatus { get; set; }
        public DateTime? SyncAccDate { get; set; }
        public string SyncAccCode { get; set; }
        public string Domain { get; set; }
        public string LevelDisplay { get; set; }
        public int? Level { get; set; }
        public string SupporterDisplay { get; set; }
        public LevelDto LevelDto { get; set; }
        public IList<CustomerAddressDto> CustomerAddressDtos { get; set; }
        public CustomerWalletDto CustomeWalletDto { get; set; }
        public bool IsTempDepositVip { get; set; }
        public bool AllowBid{ get; set; }
    }
    public class CustomerWalletDto
    {
        public string AccountId { get; set; }
        public string WalletId { get; set; }
        public long Cash { get; set; }
        public long CashFreeze { get; set; }
    }
    public class CustomerAddressDto
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public bool Active { get; set; }
        public string PostalCode { get; set; }
        public string Ward { get; set; }
        public string CountryCode { get; set; }
        public string Note { get; set; }
    }
    public class LevelDto
    {
        public int CustomerId { get; set; }
        public int LevelId { get; set; }
        public string Title { get; set; }
        public long FeeStandard { get; set; }
        public long FeeStandardAuc { get; set; }
        public long FeeStandardMer { get; set; }
        public long FeeStandardAmz { get; set; }
        public long FeeStandardRak { get; set; }
        public long FeeStandardYah { get; set; }
        public long FeeStandardWeblink { get; set; }
        public long FeeStandardP { get; set; }
        public long FeeStandardAucP { get; set; }
        public long FeeStandardMerP { get; set; }
        public long FeeStandardAmzP { get; set; }
        public long FeeStandardRakP { get; set; }
        public long FeebuyWeblink { get; set; }
        public long FeeStandardYahP { get; set; }
        public long FeeStandardWeblinkP { get; set; }
        public int AucMaxBidLink { get; set; }
        public long AucFeeCancel { get; set; }
        public string UserUpdate { get; set; }
        public string AllowBid { get; set; }
        public long FeePayment { get; set; }
        public long FeePaymentAuc { get; set; }
        public long FeePaymentMer { get; set; }
        public long FeePaymentAmz { get; set; }
        public long FeePaymentRak { get; set; }
        public long FeePaymentYah { get; set; }
        public long FeePaymentWeblink { get; set; }

    }
}
