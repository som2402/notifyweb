﻿using System.Collections.Generic;

namespace Ez.Notify.Customer.Driver.Response
{
    public class CustomerListResponse
    {
        public CustomerListResponse()
        {
            Data = new List<CustomerDto>();
        }
        public IList<CustomerDto> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
