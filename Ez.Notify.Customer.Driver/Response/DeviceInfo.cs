﻿namespace Ez.Notify.Customer.Driver.Response
{
    public class DeviceInfo
    {
        public OsInfo Os { get; set; }
        public string Device { get; set; }
        public string Brand { get; set; }
        public string Name { get; set; }

    }
    public class OsInfo
    {
        public OsInfo(string name, string platform, string shortName, string version)
        {
            Name = name;
            Platform = platform;
            ShortName = shortName;
            Version = version;
        }
        public string Name { get; set; }
        public string Platform { get; set; }
        public string ShortName { get; set; }
        public string Version { get; set; }
    }

}
