﻿using System;

namespace Ez.Notify.Customer.Driver.Response
{
    public class AccessHistoryDto
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string IpAddress { get; set; }
        public string Url { get; set; }
        public string Device { get; set; }
        public string Status { get; set; }
        public string Request { get; set; }
        public string BodyByte { get; set; }
        public string RemoteAddress { get; set; }
        public string Host { get; set; }
        public string Referer { get; set; }
        public string Method { get; set; }
        public string RequestMethod { get; set; }
        public string Message { get; set; }
        public DateTime AccessTime { get; set; }
        public DeviceInfo DeviceInfo { get; set; }
        public string FullName { get; set; }
        public string[] DeviceTypes { get; set; }
    }
}
