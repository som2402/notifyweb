﻿namespace Ez.Notify.Customer.Driver.Response
{
    public class MemberShipLevelDto
    {
        public string AccountId { get; set; }
        public string aucFeecancel { get; set; }
        public string aucMaxbidlink { get; set; }
        public int levelId { get; set; }
        public string title { get; set; }
        public string fee_standard { get; set; }
        public string fee_standard_auc { get; set; }
        public string fee_standard_mer { get; set; }
        public string fee_standard_amz { get; set; }
        public string fee_standard_rak { get; set; }
        public string fee_standard_yah { get; set; }
        public string fee_standard_auc_p { get; set; }
        public string fee_standard_mer_p { get; set; }
        public string fee_standard_amz_p { get; set; }
        public string fee_standard_rak_p { get; set; }
        public string fee_standard_yah_p { get; set; }
    }
}
