﻿using System;

namespace Ez.Notify.Customer.Driver.Response
{
    public class CustomerDto
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        public string AccountId { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public DateTime? Birthday { get; set; }
        public string Avatar { get; set; }
        public string Website { get; set; }
        public string IdImages { get; set; }
        public string IdNumber { get; set; }
        public string IdName { get; set; }
        public string IdAddress { get; set; }
        public string IdPermanentAddress { get; set; }
        public DateTime? IdBirthDay { get; set; }
        public DateTime? IdIssuedDate { get; set; }
        public string IdIssuedBy { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedDateDisplay => CreatedDate == null ? string.Empty : CreatedDate.Value.ToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CareBy { get; set; }
        public bool? BidActive { get; set; }
        public string Code { get; set; }
        public DateTime? CodeCreatedDate { get; set; }
        public string CodeCreatedDateDisplay => CodeCreatedDate == null ? string.Empty : CodeCreatedDate.Value.ToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
        public string Group { get; set; }
        public string TranCode { get; set; }
        public bool? TranActive { get; set; }
        public int? SyncAccStatus { get; set; }
        public DateTime? SyncAccDate { get; set; }
        public string SyncAccCode { get; set; }
        public string Domain { get; set; }
        public string LevelDisplay { get; set; }
        public string CountryDisplay { get; set; }
        public string SupporterDisplay { get; set; }
        public bool AllowBid { get; set; }
        public bool IsTempDepositVip { get; set; }
        public string DeviceToken { get; set; }
    }
}