﻿using Microsoft.Extensions.DependencyInjection;
using Scrutor;

namespace Shared.Ioc
{
    public static class Extensions
    {
        public static void AddClassesInterfaces(this IServiceCollection services, params System.Reflection.Assembly[] assemblies)
        {
            services.Scan(scan => scan
                .FromAssemblies(assemblies)
                .AddClasses()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsMatchingInterface()
                .WithScopedLifetime());
        }
    }
}