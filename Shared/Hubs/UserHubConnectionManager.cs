﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shared.Hubs
{
    public class UserHubConnectionManager : IUserHubConnectionManager
    {
        private static Dictionary<string, List<string>> userConnectionMap = new Dictionary<string, List<string>>();
        private static string userConnectionMapLocker = string.Empty;

        public void KeepUserConnection(string userId, string connectionId)
        {
            lock (userConnectionMapLocker)
            {
                if (!userConnectionMap.ContainsKey(userId))
                {
                    userConnectionMap[userId] = new List<string>();
                }
                userConnectionMap[userId].Add(connectionId);
            }
        }

        public void RemoveUserConnection(string connectionId)
        {
            //Remove the connectionId of user 
            lock (userConnectionMapLocker)
            {
                foreach (var userId in userConnectionMap.Keys)
                {
                    if (userConnectionMap.ContainsKey(userId))
                    {
                        if (userConnectionMap[userId].Contains(connectionId))
                        {
                            userConnectionMap[userId].Remove(connectionId);
                            break;
                        }
                    }
                }
            }
        }
        public List<string> GetUserConnections(string userId)
        {
            var conn = new List<string>();
            try
            {
                lock (userConnectionMapLocker)
                {
                    conn = userConnectionMap[userId];
                }
            }
            catch
            {
            }
            return conn;
        }

        public string GetUserConnection(string userId)
        {
            var conn = string.Empty;
            try
            {
                lock (userConnectionMapLocker)
                {
                    conn = userConnectionMap[userId]?.FirstOrDefault();
                }
            }
            catch
            {
            }

            return conn;
        }

        public List<string> GetConnections()
        {
            var conn = new List<string>();
            try
            {
                lock (userConnectionMapLocker)
                {
                    conn = userConnectionMap.Select(g => g.Value).SelectMany(x => x).ToList();
                }
            }
            catch
            {
            }
            return conn;
        }
    }
}
