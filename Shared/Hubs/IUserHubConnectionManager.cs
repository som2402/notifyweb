﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Hubs
{
    public interface IUserHubConnectionManager
    {
        void KeepUserConnection(string userId, string connectionId);
        void RemoveUserConnection(string connectionId);
        List<string> GetUserConnections(string userId);
        string GetUserConnection(string userId);
        List<string> GetConnections();
    }

}
