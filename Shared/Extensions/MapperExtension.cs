﻿// using AutoMapper;

using System.Linq;
using AgileObjects.AgileMapper;

namespace Shared.Extensions
{
    public static class MapperExtension
    {
        // private static IMapper _mapper;
        //
        // public static IMapper RegisterMap(this IMapper mapper)
        // {
        //     _mapper = mapper;
        //     return mapper;
        // }
        //
        // public static T To<T>(this object source)
        // {
        //     return _mapper.Map<T>(source);
        // }
        //
        // public static T To<T>(this object source, T dest)
        // {
        //     return _mapper.Map(source, dest);
        // }

        public static TSource Clone<TSource>(this TSource source)
        {
            return Mapper.DeepClone(source);
        }

        public static TDestination Map<TSource, TDestination>(this TSource source)
        {
            return Mapper.Map(source).ToANew<TDestination>();
        }

        public static TDestination Map<TDestination>(this object source)
        {
            return Mapper.Map(source).ToANew<TDestination>();
        }

        public static TDestination Map<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return Mapper.Map(source).OnTo(destination);
        }

        public static IQueryable<TDestination> Project<TSource, TDestination>(this IQueryable<TSource> queryable)
        {
            return queryable.Project().To<TDestination>();
        }
    }
}