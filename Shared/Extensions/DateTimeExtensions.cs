﻿using System;

namespace Shared.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime ConvertGmt7(this DateTime dateTime)
        {
            return dateTime.Add(TimeSpan.FromHours(7));
        }
    }
}