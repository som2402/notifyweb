﻿using Microsoft.EntityFrameworkCore;
using Shared.EF.Interfaces;

namespace Shared.EF.Implement
{
    public class EfRepository<TDbContext, T> : Repository<TDbContext, T> where T : class where TDbContext : DbContext
    {
        public EfRepository(TDbContext context) : base(new EfCommandRepository<TDbContext, T>(context),
            new EfQueryRepository<TDbContext, T>(context))
        {
        }
    }
}