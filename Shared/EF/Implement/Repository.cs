﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Common.Sort;
using Shared.Dto;
using Shared.EF.Interfaces;

namespace Shared.EF.Implement
{
    // public class Repository<TDbContext, T> : IRepository<T> where T : class where TDbContext : DbContext
    // {
    //     private readonly TDbContext _dbContext;
    //
    //     public Repository(TDbContext dbContext)
    //     {
    //         _dbContext = dbContext ?? throw new ArgumentException(nameof(dbContext));
    //     }
    //
    //     public DbSet<T> GetDbSet()
    //     {
    //         return _dbContext.Set<T>();
    //     }
    //
    //     public async Task<QueryResult<T>> QueryAsync(Expression<Func<T, bool>> predicate, int skip, int take,
    //         Sorts sort)
    //     {
    //         var queryable = _dbContext.Set<T>().Where(predicate);
    //         return await queryable.ToQueryResultAsync(skip, take, sort);
    //     }
    //
    //     public async Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate)
    //     {
    //         return await _dbContext.Set<T>().Where(predicate).ToListAsync();
    //     }
    //
    //     public async Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate, int take)
    //     {
    //         return await _dbContext.Set<T>().Where(predicate).Take(take).ToListAsync();
    //     }
    //
    //     public async Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate, int take,
    //         Expression<Func<T, object>> order = null)
    //     {
    //         var queryable = _dbContext.Set<T>().Where(predicate);
    //         if (order != null) queryable = queryable.OrderByDescending(order);
    //
    //         return await queryable.Take(take).ToListAsync();
    //     }
    //
    //     public async Task<IList<T>> GetAllAsync()
    //     {
    //         return await _dbContext.Set<T>().ToListAsync();
    //     }
    //
    //     public async Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate)
    //     {
    //         return await _dbContext.Set<T>().FirstOrDefaultAsync(predicate);
    //     }
    //
    //     public async Task<TType> GetSingleAsync<TType>(Expression<Func<T, bool>> predicate,
    //         Expression<Func<T, TType>> select = null) where TType : class
    //     {
    //         if (select != null) return await _dbContext.Set<T>().Where(predicate).Select(select).FirstOrDefaultAsync();
    //
    //         var entity = await _dbContext.Set<T>().Where(predicate).FirstOrDefaultAsync();
    //         return entity.Map<TType>();
    //     }
    //
    //     public async Task<T> GetByIdAsync(Guid entityId)
    //     {
    //         return await _dbContext.Set<T>().FindAsync(entityId);
    //     }
    //
    //     public async Task<T> GetByIdAsync<TKey>(TKey entityId)
    //     {
    //         return await _dbContext.Set<T>().FindAsync(entityId);
    //     }
    //
    //     public async Task<bool> ExistsAsync(Expression<Func<T, bool>> predicate)
    //     {
    //         return await _dbContext.Set<T>().AnyAsync(predicate);
    //     }
    //
    //     public virtual T Add(T entity)
    //     {
    //         _dbContext.Set<T>().Add(entity);
    //         return entity;
    //     }
    //
    //     public virtual void AddRange(IEnumerable<T> entities)
    //     {
    //         _dbContext.Set<T>().AddRange(entities);
    //     }
    //
    //     public T Update(T entity)
    //     {
    //         _dbContext.Entry(entity).State = EntityState.Modified;
    //         return entity;
    //     }
    //
    //     public void UpdateRange(IEnumerable<T> entities)
    //     {
    //         _dbContext.Set<T>().UpdateRange(entities);
    //     }
    //
    //     public T Delete(T entity)
    //     {
    //         _dbContext.Entry(entity).State = EntityState.Deleted;
    //         return entity;
    //     }
    //
    //     public TKey Delete<TKey>(TKey entityId)
    //     {
    //         var entity = _dbContext.Find<T>(entityId);
    //         _dbContext.Entry(entity).State = EntityState.Deleted;
    //         return entityId;
    //     }
    //
    //     public virtual void DeleteMany(IEnumerable<T> entities)
    //     {
    //         _dbContext.Set<T>().RemoveRange(entities);
    //     }
    //
    //     public async Task<int> CountAllAsync()
    //     {
    //         return await _dbContext.Set<T>().CountAsync();
    //     }
    //
    //     public async Task<int> CountAsync(Expression<Func<T, bool>> predicate)
    //     {
    //         return await _dbContext.Set<T>().CountAsync(predicate);
    //     }
    //
    //     public async Task<T> GetRandom()
    //     {
    //         return await _dbContext.Set<T>().OrderBy(x => Guid.NewGuid()).FirstOrDefaultAsync();
    //     }
    //
    //     public async Task<IList<TType>> GetAsync<TType>(Expression<Func<T, bool>> predicate,
    //         Expression<Func<T, TType>> select) where TType : class
    //     {
    //         return await _dbContext.Set<T>().Where(predicate).Select(select).ToListAsync();
    //     }
    //
    //     public async Task<IList<TType>> GetAsync<TType>(Expression<Func<T, TType>> select) where TType : class
    //     {
    //         return await _dbContext.Set<T>().Select(select).ToListAsync();
    //     }
    //
    //     public async Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate, int skip, int take)
    //     {
    //         return await _dbContext.Set<T>().Where(predicate)
    //             .Skip(skip)
    //             .Take(take)
    //             .ToListAsync();
    //     }
    //
    //     public async Task<T> GetSingleAsync()
    //     {
    //         return await _dbContext.Set<T>().FirstOrDefaultAsync();
    //     }
    // }

    public abstract class Repository<TDbContext, T> : IRepository<T> where T : class where TDbContext : DbContext
    {
        private readonly ICommandRepository<T> _commandRepository;
        private readonly IQueryRepository<T> _queryRepository;

        protected Repository(ICommandRepository<T> commandRepository, IQueryRepository<T> queryRepository)
        {
            _commandRepository = commandRepository;
            _queryRepository = queryRepository;
        }

        public IQueryable<T> Queryable => _queryRepository.Queryable;

        public void Add(T item)
        {
            _commandRepository.Add(item);
        }

        public Task AddAsync(T item)
        {
            return _commandRepository.AddAsync(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            _commandRepository.AddRange(items);
        }

        public Task AddRangeAsync(IEnumerable<T> items)
        {
            return _commandRepository.AddRangeAsync(items);
        }

        public bool Any()
        {
            return _queryRepository.Any();
        }

        public bool Any(Expression<Func<T, bool>> where)
        {
            return _queryRepository.Any(where);
        }

        public Task<bool> AnyAsync()
        {
            return _queryRepository.AnyAsync();
        }

        public Task<bool> AnyAsync(Expression<Func<T, bool>> where)
        {
            return _queryRepository.AnyAsync(where);
        }

        public long Count()
        {
            return _queryRepository.Count();
        }

        public long Count(Expression<Func<T, bool>> where)
        {
            return _queryRepository.Count(where);
        }

        public Task<long> CountAsync()
        {
            return _queryRepository.CountAsync();
        }

        public Task<long> CountAsync(Expression<Func<T, bool>> where)
        {
            return _queryRepository.CountAsync(where);
        }

        public void Delete(object key)
        {
            _commandRepository.Delete(key);
        }

        public void Delete(Expression<Func<T, bool>> where)
        {
            _commandRepository.Delete(where);
        }

        public Task DeleteAsync(object key)
        {
            return _commandRepository.DeleteAsync(key);
        }

        public Task DeleteAsync(Expression<Func<T, bool>> where)
        {
            return _commandRepository.DeleteAsync(where);
        }

        public T Get(object key)
        {
            return _queryRepository.Get(key);
        }

        public Task<T> GetAsync(object key)
        {
            return _queryRepository.GetAsync(key);
        }

        public IEnumerable<T> List()
        {
            return _queryRepository.List();
        }

        public Task<IEnumerable<T>> ListAsync()
        {
            return _queryRepository.ListAsync();
        }

        public Task<QueryResult<T>> QueryAsync(Expression<Func<T, bool>> predicate, int skip, int take,
            Sorts sort = null)
        {
            return _queryRepository.QueryAsync(predicate, skip, take, sort);
        }


        public void Update(object key, T item)
        {
            _commandRepository.Update(key, item);
        }

        public Task UpdateAsync(object key, T item)
        {
            return _commandRepository.UpdateAsync(key, item);
        }

        public void UpdatePartial(object key, object item)
        {
            _commandRepository.UpdatePartial(key, item);
        }

        public Task UpdatePartialAsync(object key, object item)
        {
            return _commandRepository.UpdatePartialAsync(key, item);
        }

        public void UpdateRange(IEnumerable<T> items)
        {
            _commandRepository.UpdateRange(items);
        }

        public Task UpdateRangeAsync(IEnumerable<T> items)
        {
            return _commandRepository.UpdateRangeAsync(items);
        }
    }
}