﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Common.Sort;
using Shared.Dto;
using Shared.EF.Interfaces;

namespace Shared.EF.Implement
{
    public class EfQueryRepository<TDbContext, T> : IQueryRepository<T> where T : class where TDbContext : DbContext
    {
        private readonly TDbContext _context;

        public EfQueryRepository(TDbContext dbContext)
        {
            _context = dbContext ?? throw new ArgumentException(nameof(dbContext));
        }

        public IQueryable<T> Queryable => _context.QuerySet<T>();

        public bool Any()
        {
            return Queryable.Any();
        }

        public bool Any(Expression<Func<T, bool>> where)
        {
            return Queryable.Any(where);
        }

        public Task<bool> AnyAsync()
        {
            return Queryable.AnyAsync();
        }

        public Task<bool> AnyAsync(Expression<Func<T, bool>> where)
        {
            return Queryable.AnyAsync(where);
        }

        public long Count()
        {
            return Queryable.LongCount();
        }

        public long Count(Expression<Func<T, bool>> where)
        {
            return Queryable.LongCount(where);
        }

        public Task<long> CountAsync()
        {
            return Queryable.LongCountAsync();
        }

        public Task<long> CountAsync(Expression<Func<T, bool>> where)
        {
            return Queryable.LongCountAsync(where);
        }

        public T Get(object key)
        {
            return _context.DetectChangesLazyLoading(false).Set<T>().Find(key);
        }

        public Task<T> GetAsync(object key)
        {
            return _context.DetectChangesLazyLoading(false).Set<T>().FindAsync(key).AsTask();
        }

        public IEnumerable<T> List()
        {
            return Queryable.ToList();
        }

        public async Task<IEnumerable<T>> ListAsync()
        {
            return await Queryable.ToListAsync().ConfigureAwait(false);
        }

        public async Task<QueryResult<T>> QueryAsync(Expression<Func<T, bool>> predicate, int skip, int take,
            Sorts sort = null)
        {
            return await Queryable.Where(predicate).ToQueryResultAsync(skip, take, sort);
        }
    }
}