﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Shared.MongoDb.Dal.Entities
{
    public abstract class BaseObjectIdMongoEntity : IMongoEntity<ObjectId>
    {
        [BsonId] public virtual ObjectId Id { get; set; }
    }
}