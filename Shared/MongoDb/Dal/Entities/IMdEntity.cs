﻿namespace Shared.MongoDb.Dal.Entities
{
    public interface IMongoEntity<T>
    {
        T Id { get; set; }
    }
}