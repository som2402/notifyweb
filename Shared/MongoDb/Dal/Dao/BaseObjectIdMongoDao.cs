﻿using MongoDB.Bson;
using Shared.MongoDb.Dal.Dao.Interfaces;
using Shared.MongoDb.Dal.Entities;

namespace Shared.MongoDb.Dal.Dao
{
    public abstract class BaseObjectIdMongoDao<T> : BaseMongoDao<T, ObjectId>, IObjectIdMongoDao<T>
        where T : BaseObjectIdMongoEntity
    {
        public BaseObjectIdMongoDao(IMongoDbFactory databaseFactory) : base(databaseFactory)
        {
        }
    }
}