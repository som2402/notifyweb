﻿using Shared.MongoDb.Dal.Dao.Interfaces;
using Shared.MongoDb.Dal.Entities;

namespace Shared.MongoDb.Dal.Dao
{
    public abstract class BaseStringIdMongoDao<T> : BaseMongoDao<T, string>, IStringIdMongoDao<T>
        where T : BaseStringIdMongoEntity
    {
        public BaseStringIdMongoDao(IMongoDbFactory databaseFactory) : base(databaseFactory)
        {
        }
    }
}