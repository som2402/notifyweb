﻿using MongoDB.Bson;
using Shared.MongoDb.Dal.Entities;

namespace Shared.MongoDb.Dal.Dao.Interfaces
{
    public interface IObjectIdMongoDao<T> : IMongoDao<T, ObjectId> where T : BaseObjectIdMongoEntity
    {
    }
}