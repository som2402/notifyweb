﻿using Shared.MongoDb.Dal.Entities;

namespace Shared.MongoDb.Dal.Dao.Interfaces
{
    public interface IStringIdMongoDao<T> : IMongoDao<T, string> where T : BaseStringIdMongoEntity
    {
    }
}