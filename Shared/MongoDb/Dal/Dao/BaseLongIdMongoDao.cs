﻿using Shared.MongoDb.Dal.Dao.Interfaces;
using Shared.MongoDb.Dal.Entities;

namespace Shared.MongoDb.Dal.Dao
{
    public abstract class BaseLongIdMongoDao<T> : BaseMongoDao<T, long>, ILongIdMongoDao<T>
        where T : BaseLongIdMongoEntity
    {
        public BaseLongIdMongoDao(IMongoDbFactory databaseFactory) : base(databaseFactory)
        {
        }
    }
}