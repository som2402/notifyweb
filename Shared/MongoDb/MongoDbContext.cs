﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Shared.MongoDb.Config;

namespace Shared.MongoDb
{
    public interface IMongoDbContext
    {
        IMongoDatabase Database { get; }
    }

    public class MongoDbContext : IMongoDbContext
    {
        public MongoDbContext(IOptions<MongoSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                Database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoDatabase Database { get; }
    }
}