﻿using MongoDB.Driver;

namespace Shared.MongoDb
{
    public interface IMongoDbFactory
    {
        IMongoDatabase GetMongoDatabase<T>();
        string GetCollectionName<T>();
    }
}