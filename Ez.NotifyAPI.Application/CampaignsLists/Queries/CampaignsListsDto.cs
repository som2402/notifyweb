﻿namespace Ez.NotifyAPI.Application.CampaignsLists.Queries
{
    public class CampaignsListsDto
    {
        public int CampaignId { get; set; }
        public int ListId { get; set; }
        public int? Status { get; set; }
        public int? Sub { get; set; }
        public string Id { get; set; }
    }
}