﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.NotifyConfigs.Queries;

namespace Ez.NotifyAPI.Application.NotifyConfigs.Repositories
{
    public static class NotifyConfigExpression
    {
        public static Expression<Func<NotifyConfig, bool>> Id(int id) => notifyConfig => notifyConfig.Id == id;

        public static Expression<Func<NotifyConfig, bool>> Code(string code) =>
            notifyConfig => notifyConfig.Code == code;

        public static Expression<Func<NotifyConfig, NotifyConfigDto>> Model =>
            notifyConfig => new NotifyConfigDto()
            {
                Id = notifyConfig.Id,
                Active = notifyConfig.Active,
                Code = notifyConfig.Code,
                Description = notifyConfig.Description,
                Name = notifyConfig.Name,
                Ord = notifyConfig.Ord,
                Required = notifyConfig.Required,
                DesktopRequired = notifyConfig.DesktopRequired,
                GroupId = notifyConfig.GroupId,
                MobileRequired = notifyConfig.MobileRequired,
                WebRequired = notifyConfig.WebRequired
            };
    }
}