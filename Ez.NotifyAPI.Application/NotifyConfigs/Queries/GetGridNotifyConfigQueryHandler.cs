﻿using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using MediatR;
using Shared.BaseModel;
using Shared.BaseModel.Grids;

namespace Ez.NotifyAPI.Application.NotifyConfigs.Queries
{
    public class GetGridNotifyConfigQuery : IRequest<BaseEntityResponse<Grid<NotifyConfigDto>>>
    {
        public GetGridNotifyConfigQuery(GridParameters gridParameters)
        {
            GridParameters = gridParameters;
        }

        public GridParameters GridParameters { get; private set; }
    }

    public class
        GetGridNotifyConfigQueryHandler : IRequestHandler<GetGridNotifyConfigQuery,
            BaseEntityResponse<Grid<NotifyConfigDto>>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;

        public GetGridNotifyConfigQueryHandler(INotifyConfigRepository notifyConfigRepository)
        {
            _notifyConfigRepository = notifyConfigRepository;
        }

        public async Task<BaseEntityResponse<Grid<NotifyConfigDto>>> Handle(GetGridNotifyConfigQuery request,
            CancellationToken cancellationToken)
        {
            var response = new BaseEntityResponse<Grid<NotifyConfigDto>>();
            var result = await _notifyConfigRepository.GridAsync(request.GridParameters);
            response.Data = result;
            response.Status = true;
            // response.DetailErrors = new List<DetailError>()
            // {
            //     new DetailError("500", new Dictionary<string, string> {{"Code", "CODE_ERROR"}})
            // };
            return response;
        }
    }
}