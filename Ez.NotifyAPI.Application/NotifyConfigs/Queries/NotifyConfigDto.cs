﻿namespace Ez.NotifyAPI.Application.NotifyConfigs.Queries
{
    public class NotifyConfigDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int? Required { get; set; }
        public int? WebRequired { get; set; }
        public int? MobileRequired { get; set; }
        public int? DesktopRequired { get; set; }
        public int? Ord { get; set; }
    }
}