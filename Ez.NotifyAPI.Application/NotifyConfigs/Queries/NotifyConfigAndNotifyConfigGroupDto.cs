﻿using System.Collections.Generic;

namespace Ez.NotifyAPI.Application.NotifyConfigs.Queries
{
    public class NotifyConfigAndNotifyConfigGroupDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Ord { get; set; }
        public string AppId { get; set; }

        public List<NotifyConfigAndCustomerNotifyConfigDto> NotifyConfigAndCustomerNotifyConfigDtos { get; set; } =
            new List<NotifyConfigAndCustomerNotifyConfigDto>();
    }
}