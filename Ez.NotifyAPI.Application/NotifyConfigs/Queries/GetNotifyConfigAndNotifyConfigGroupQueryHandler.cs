﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Queries;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.Extensions;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Ez.NotifyAPI.Application.NotifyConfigs.Queries
{
    public class GetNotifyConfigAndNotifyConfigGroupQuery : IRequest<List<NotifyConfigAndNotifyConfigGroupDto>>
    {
        public GetNotifyConfigAndNotifyConfigGroupQuery(string accountId)
        {
            AccountId = accountId;
        }

        public string AccountId { get; }
    }

    public class GetNotifyConfigAndNotifyConfigGroupQueryHandler : IRequestHandler<
        GetNotifyConfigAndNotifyConfigGroupQuery, List<NotifyConfigAndNotifyConfigGroupDto>>
    {
        private readonly ICustomerNotifyConfigRepository _customerNotifyConfigRepository;
        private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;
        private readonly INotifyConfigRepository _notifyConfigRepository;

        public GetNotifyConfigAndNotifyConfigGroupQueryHandler(
            ICustomerNotifyConfigRepository customerNotifyConfigRepository,
            INotifyConfigGroupRepository notifyConfigGroupRepository, INotifyConfigRepository notifyConfigRepository)
        {
            _customerNotifyConfigRepository = customerNotifyConfigRepository;
            _notifyConfigGroupRepository = notifyConfigGroupRepository;
            _notifyConfigRepository = notifyConfigRepository;
        }


        public async Task<List<NotifyConfigAndNotifyConfigGroupDto>> Handle(
            GetNotifyConfigAndNotifyConfigGroupQuery request,
            CancellationToken cancellationToken)
        {
            var notifyConfigGroups =
                await _notifyConfigGroupRepository.Queryable.ToListAsync(cancellationToken: cancellationToken);
            var customerNotifyConfig =
                await _customerNotifyConfigRepository.GetByAccountId(request.AccountId);
            var allNotifyConfig =
                await _notifyConfigRepository.Queryable.ToListAsync(cancellationToken: cancellationToken);
            var result = new List<NotifyConfigAndNotifyConfigGroupDto>();
            foreach (var item in notifyConfigGroups)
            {
                var notifyConfigAndNotifyConfigGroupDto = new NotifyConfigAndNotifyConfigGroupDto();
                notifyConfigAndNotifyConfigGroupDto.Id = item.Id;
                notifyConfigAndNotifyConfigGroupDto.Ord = item.Id;
                notifyConfigAndNotifyConfigGroupDto.Title = item.Title;
                notifyConfigAndNotifyConfigGroupDto.AppId = item.AppId;
                notifyConfigAndNotifyConfigGroupDto.NotifyConfigAndCustomerNotifyConfigDtos =
                    new List<NotifyConfigAndCustomerNotifyConfigDto>();
                var notifyConfigs = allNotifyConfig.Where(i => i.Active && i.GroupId == item.Id);
                if (customerNotifyConfig.Any())
                    foreach (var itemj in notifyConfigs)
                    {
                        var notifyConfigAndCustomerNotifyConfigDto = new NotifyConfigAndCustomerNotifyConfigDto
                        {
                            Active = itemj.Active,
                            Code = itemj.Code,
                            Description = itemj.Description,
                            Id = itemj.Id,
                            Name = itemj.Name,
                            Required = itemj.Required,
                            GroupId = itemj.GroupId,
                            DesktopRequired = itemj.DesktopRequired,
                            MobileRequired = itemj.MobileRequired,
                            WebRequired = itemj.WebRequired,
                            Ord = itemj.Ord,
                            CustomerNotifyConfigDto = new CustomerNotifyConfigDto()
                        };

                        foreach (var itemz in customerNotifyConfig)
                        {
                            if (!itemz.NotifyConfigCode.Equals(itemj.Code)) continue;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.Id = itemz.Id;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.AccountId =
                                itemz.AccountId;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.AppId = itemz.AppId;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.SendDesktop =
                                itemz.SendDesktop;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.SendEmail =
                                itemz.SendEmail;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.SendWeb = itemz.SendWeb;
                            notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto.SendMobile =
                                itemz.SendMobile;
                        }

                        notifyConfigAndNotifyConfigGroupDto.NotifyConfigAndCustomerNotifyConfigDtos.Add(
                            notifyConfigAndCustomerNotifyConfigDto);
                    }
                else
                    foreach (var itemj in notifyConfigs)
                    {
                        var notifyConfigAndCustomerNotifyConfigDto = new NotifyConfigAndCustomerNotifyConfigDto
                        {
                            Active = itemj.Active,
                            Code = itemj.Code,
                            Description = itemj.Description,
                            Id = itemj.Id,
                            Name = itemj.Name,
                            Required = itemj.Required,
                            GroupId = itemj.GroupId,
                            DesktopRequired = itemj.DesktopRequired,
                            MobileRequired = itemj.MobileRequired,
                            WebRequired = itemj.WebRequired,
                            Ord = itemj.Ord
                        };
                        notifyConfigAndCustomerNotifyConfigDto.CustomerNotifyConfigDto
                            .MapStatusNotifyConfigToCustomerConfig(itemj, request.AccountId);
                        notifyConfigAndNotifyConfigGroupDto.NotifyConfigAndCustomerNotifyConfigDtos.Add(
                            notifyConfigAndCustomerNotifyConfigDto);
                    }

                result.Add(notifyConfigAndNotifyConfigGroupDto);
            }

            return result;
        }
    }
}