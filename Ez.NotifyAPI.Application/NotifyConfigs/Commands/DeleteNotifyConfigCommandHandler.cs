﻿using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Validators;

namespace Ez.NotifyAPI.Application.NotifyConfigs.Commands
{
    public class DeleteNotifyConfigCommand : IRequest<BaseEntityResponse<int>>
    {
        public DeleteNotifyConfigCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }

    public class DeleteNotifyConfigCommandValidator : BaseValidator<DeleteNotifyConfigCommand>
    {
        public DeleteNotifyConfigCommandValidator(INotifyConfigRepository notifyConfigRepository)
        {
            RuleFor(x => x.Id).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfig = await notifyConfigRepository.GetNotifyConfigById(x);
                return notifyConfig != null;
            }).WithErrorCode("ID_NOT_FOUND");
        }
    }

    public class DeleteNotifyConfigCommandHandler : IRequestHandler<DeleteNotifyConfigCommand, BaseEntityResponse<int>>
    {
        private readonly INotifyConfigRepository _notifyConfigRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteNotifyConfigCommandHandler(INotifyConfigRepository notifyConfigRepository, IUnitOfWork unitOfWork)
        {
            _notifyConfigRepository = notifyConfigRepository;
            _unitOfWork = unitOfWork;
        }


        public async Task<BaseEntityResponse<int>> Handle(DeleteNotifyConfigCommand request,
            CancellationToken cancellationToken)
        {
            await _notifyConfigRepository.DeleteAsync(await _notifyConfigRepository.GetNotifyConfigById(request.Id));
            await _unitOfWork.CommitAsync(cancellationToken);
            var response = new BaseEntityResponse<int> {Data = request.Id, Status = true};
            return response;
        }
    }
}