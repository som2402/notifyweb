﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Ez.Notify.Common.Utilities;
using Ez.Notify.Domain.Entity;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Validators;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;

namespace Ez.Notify.Application.CustomerNotifyConfigs.Command
{
    public class AddCustomerNotifyConfigCommand : IRequest<BaseResponse>
    {
        public AddCustomerNotifyConfigCommand(string appId, string accountId,
            List<CustomerNotifyConfig> customerNotifyConfigs)
        {
            AppId = appId;
            AccountId = accountId;
            CustomerNotifyConfigs = customerNotifyConfigs;
        }

        public string AppId { get; }
        public string AccountId { get; }
        public List<CustomerNotifyConfig> CustomerNotifyConfigs { get; }
    }

    public class AddCustomerNotifyConfigCommandHandler : IRequestHandler<AddCustomerNotifyConfigCommand, BaseResponse>
    {
        private readonly ICustomerNotifyConfigRepository customerNotifyConfigRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddCustomerNotifyConfigCommandHandler(ICustomerNotifyConfigRepository customerNotifyConfigRepository
            , IUnitOfWork unitOfWork)
        {
            this.customerNotifyConfigRepository = customerNotifyConfigRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseResponse> Handle(AddCustomerNotifyConfigCommand request,
            CancellationToken cancellationToken)
        {
            var response = new BaseResponse();

            var customerNotifyConfigs = await customerNotifyConfigRepository.GetListCustomerNotifyConfigs(request.AppId, request.AccountId);

            var changeCustomerNotifyConfigs = request.CustomerNotifyConfigs.Except(customerNotifyConfigs, new CustomerNotifyConfigComparer())
                    .ToList();

            var newCustomerNotifyConfigs = new List<CustomerNotifyConfig>();
            if (customerNotifyConfigs.Count() > 0)
            {
                var editCustomerNotifyConfigs = changeCustomerNotifyConfigs.Where(item => item.Id != 0).ToList();
                if (editCustomerNotifyConfigs.Count > 0)
                {
                    customerNotifyConfigRepository.UpdateRange(editCustomerNotifyConfigs);

                    newCustomerNotifyConfigs.AddRange(changeCustomerNotifyConfigs.Where(item => item.Id == 0));
                }
            }
            else
            {
                newCustomerNotifyConfigs = changeCustomerNotifyConfigs;
            }

            if (newCustomerNotifyConfigs.Count > 0) customerNotifyConfigRepository.AddRange(newCustomerNotifyConfigs);

            await _unitOfWork.CommitAsync(cancellationToken);

            response.Successful();
            return response;
        }
    }
}