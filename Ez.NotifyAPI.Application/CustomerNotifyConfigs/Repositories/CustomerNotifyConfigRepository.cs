﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories
{
    public interface ICustomerNotifyConfigRepository : IRepository<CustomerNotifyConfig>
    {
        Task<CustomerNotifyConfig> GetByIdAsync(int id);
        Task<IEnumerable<CustomerNotifyConfig>> GetByAccountId(string accountId);
        Task<CustomerNotifyConfig> FindByExpression(string appId, string accountId, string notifyConfigCode);
        Task<List<CustomerNotifyConfig>> GetListCustomerNotifyConfigs(string appId, string accountId);
        Task<List<CustomerNotifyConfig>> GetListByIds(params int[] ids);
    }

    public class CustomerNotifyConfigRepository : EfRepository<DataNotifyDbContext, CustomerNotifyConfig>,
        ICustomerNotifyConfigRepository
    {
        public CustomerNotifyConfigRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<CustomerNotifyConfig> GetByIdAsync(int id)
        {
            return await Queryable.Where(CustomerNotifyConfigExpression.Id(id)).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<CustomerNotifyConfig>> GetByAccountId(string accountId)
        {
            return await Queryable.Where(CustomerNotifyConfigExpression.AccountId(accountId)).ToListAsync();
        }

        public async Task<CustomerNotifyConfig> FindByExpression(string appId, string accountId,
            string notifyConfigCode)
        {
            return await Queryable
                .Where(CustomerNotifyConfigExpression.FindByCondition(appId, accountId, notifyConfigCode))
                .SingleOrDefaultAsync();
        }

        public async Task<List<CustomerNotifyConfig>> GetListCustomerNotifyConfigs(string appId, string accountId)
        {
            return await Queryable
                .Where(x => x.AccountId.Equals(accountId) && x.AppId.Equals(appId))
                .ToListAsync();
        }

        public async Task<List<CustomerNotifyConfig>> GetListByIds(params int[] ids)
        {
            return await Queryable.Where(x => ids.Contains(x.Id)).ToListAsync();
        }
    }
}