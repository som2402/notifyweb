﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Queries;

namespace Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories
{
    public static class NotifyConfigGroupExpression
    {
        public static Expression<Func<NotifyConfigGroup, bool>> Id(int id) =>
            notifyConfigGroup => notifyConfigGroup.Id == id;

        public static Expression<Func<NotifyConfigGroup, NotifyConfigGroupDto>> Model => notifyConfigGroup =>
            new NotifyConfigGroupDto()
            {
                Id = notifyConfigGroup.Id,
                Ord = notifyConfigGroup.Ord,
                Title = notifyConfigGroup.Title,
                AppId = notifyConfigGroup.AppId
            };
    }
}