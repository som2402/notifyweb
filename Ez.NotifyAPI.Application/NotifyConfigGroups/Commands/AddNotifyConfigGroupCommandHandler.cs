﻿// using System.Collections.Generic;
// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.NotifyConfigGroups.Commands
// {
//     public class AddNotifyConfigGroupCommand : IRequest<BaseEntityResponse<NotifyConfigGroup>>
//     {
//         public AddNotifyConfigGroupCommand(string title, int ord, string appId)
//         {
//             Title = title;
//             Ord = ord;
//             AppId = appId;
//         }
//
//         public string Title { get; }
//         public int Ord { get; }
//         public string AppId { get; }
//     }
//
//     public class AddNotifyConfigGroupCommandValidator : BaseValidator<AddNotifyConfigGroupCommand>
//     {
//         public AddNotifyConfigGroupCommandValidator()
//         {
//             RuleFor(x => x.Title)
//                 .NotEmpty()
//                 .WithErrorCode("TITLE_NOT_ALLOW_NULL")
//                 .MaximumLength(100)
//                 .WithErrorCode("TITLE_LENGTH_OVER_LIMIT")
//                 .WithState(_ => new Dictionary<string, string> {{"TITLE_MAX_LENGTH", "100"}});
//             RuleFor(x => x.Ord)
//                 .NotNull()
//                 .WithErrorCode("ORDER_NOT_ALLOW_NULL")
//                 .GreaterThan(0)
//                 .WithErrorCode("ORD_VALUE_INVALID")
//                 .WithState(_ => new Dictionary<string, string> {{"ORD_MIN_VALUE", "1"}});
//             RuleFor(x => x.AppId)
//                 .NotEmpty()
//                 .WithErrorCode("APP_ID_NOT_ALLOW_NULL");
//         }
//     }
//
//     public class
//         AddNotifyConfigGroupCommandHandler : IRequestHandler<AddNotifyConfigGroupCommand,
//             BaseEntityResponse<NotifyConfigGroup>>
//     {
//         private readonly IRepository<NotifyConfigGroup> _notifyConfigGroupRepository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public AddNotifyConfigGroupCommandHandler(IRepository<NotifyConfigGroup> notifyConfigGroupRepository,
//             IUnitOfWork unitOfWork)
//         {
//             _notifyConfigGroupRepository = notifyConfigGroupRepository;
//             _unitOfWork = unitOfWork;
//         }
//
//
//         public async Task<BaseEntityResponse<NotifyConfigGroup>> Handle(AddNotifyConfigGroupCommand request,
//             CancellationToken cancellationToken)
//         {
//             var response =
//                 new BaseEntityResponse<NotifyConfigGroup>(true, new List<DetailError> {new DetailError(null, null)});
//             var data = _notifyConfigGroupRepository.Add(new NotifyConfigGroup(request.Title, request.Ord,
//                 request.AppId));
//             await _unitOfWork.CommitAsync(cancellationToken);
//             response.SetData(data);
//             return response;
//         }
//     }
// }