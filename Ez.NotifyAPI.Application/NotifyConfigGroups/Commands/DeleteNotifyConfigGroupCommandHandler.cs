﻿// using System.Threading;
// using System.Threading.Tasks;
// using FluentValidation;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.EF.Interfaces;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.NotifyConfigGroups.Commands
// {
//     public class DeleteNotifyConfigGroupCommand : IRequest
//     {
//         public DeleteNotifyConfigGroupCommand(int id)
//         {
//             Id = id;
//         }
//
//         public int Id { get; private set; }
//     }
//
//     public class DeleteNotifyConfigGroupCommandValidator : BaseValidator<DeleteNotifyConfigGroupCommand>
//     {
//         public DeleteNotifyConfigGroupCommandValidator(IRepository<NotifyConfigGroup> notifyConfigGroupRepository)
//         {
//             RuleFor(x => x.Id).NotEmpty().WithMessage("Id is not null");
//
//             RuleFor(command => command).Custom((command, context) =>
//             {
//                 var notifyConfigGroup = notifyConfigGroupRepository.GetByIdAsync(command.Id).GetAwaiter().GetResult();
//                 if (notifyConfigGroup == null)
//                     context.AddFailure($"{nameof(command.Id)}", $"NotifyConfigGroup#{command.Id} could not be found.");
//             });
//         }
//     }
//
//     public class DeleteNotifyConfigGroupCommandHandler : IRequestHandler<DeleteNotifyConfigGroupCommand>
//     {
//         // private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;
//         private readonly IRepository<NotifyConfigGroup> _notifyConfigGroupRepository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public DeleteNotifyConfigGroupCommandHandler(IRepository<NotifyConfigGroup> notifyConfigGroupRepository,
//             IUnitOfWork unitOfWork)
//         {
//             _notifyConfigGroupRepository = notifyConfigGroupRepository;
//             _unitOfWork = unitOfWork;
//         }
//
//
//         public async Task<Unit> Handle(DeleteNotifyConfigGroupCommand request, CancellationToken cancellationToken)
//         {
//             _notifyConfigGroupRepository.Delete(request.Id);
//             await _unitOfWork.CommitAsync(cancellationToken);
//             return Unit.Value;
//         }
//     }
// }