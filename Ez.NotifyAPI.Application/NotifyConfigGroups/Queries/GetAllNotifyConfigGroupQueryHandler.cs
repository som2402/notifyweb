﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.NotifyConfigGroups.Repositories;
using MediatR;
using Shared.BaseModel;

namespace Ez.NotifyAPI.Application.NotifyConfigGroups.Queries
{
    public class GetAllNotifyConfigGroupQuery : IRequest<BaseEntityResponse<List<NotifyConfigGroupDto>>>
    {
    }

    public class
        GetAllNotifyConfigGroupQueryHandler : IRequestHandler<GetAllNotifyConfigGroupQuery,
            BaseEntityResponse<List<NotifyConfigGroupDto>>>
    {
        private readonly INotifyConfigGroupRepository _notifyConfigGroupRepository;

        public GetAllNotifyConfigGroupQueryHandler(INotifyConfigGroupRepository notifyConfigGroupRepository)
        {
            _notifyConfigGroupRepository = notifyConfigGroupRepository;
        }


        public async Task<BaseEntityResponse<List<NotifyConfigGroupDto>>> Handle(GetAllNotifyConfigGroupQuery request,
            CancellationToken cancellationToken)
        {
            var result = await _notifyConfigGroupRepository.GetAllAsync();
            var response = new BaseEntityResponse<List<NotifyConfigGroupDto>>
            {
                Data = result.ToList(),
                Status = true
            };
            return response;
        }
    }
}