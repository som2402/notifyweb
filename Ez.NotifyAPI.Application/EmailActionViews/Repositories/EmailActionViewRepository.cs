﻿using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.EmailActionViews.Repositories
{
    public interface IEmailActionViewRepository : IRepository<EmailActionView>
    {
    }

    public class EmailActionViewRepository :EfRepository<DataNotifyDbContext, EmailActionView>,
        IEmailActionViewRepository
    {
        public EmailActionViewRepository(DataNotifyDbContext context) : base(context)
        {
        }
    }
}