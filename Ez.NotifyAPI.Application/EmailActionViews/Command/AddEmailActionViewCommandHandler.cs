﻿using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.EmailActionViews.Repositories;
using MediatR;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.EmailActionViews.Command
{
    public class AddEmailActionViewCommand : IRequest<bool>
    {
        public AddEmailActionViewCommand(int emailId, string userAgent)
        {
            EmailId = emailId;
            UserAgent = userAgent;
        }

        public int EmailId { get; private set; }
        public string UserAgent { get; private set; }
    }

    public class AddEmailActionViewCommandHandler : IRequestHandler<AddEmailActionViewCommand, bool>
    {
        private readonly IEmailActionViewRepository _emailActionViewRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddEmailActionViewCommandHandler(IEmailActionViewRepository emailActionViewRepository,
            IUnitOfWork unitOfWork)
        {
            _emailActionViewRepository = emailActionViewRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddEmailActionViewCommand request, CancellationToken cancellationToken)
        {
            await _emailActionViewRepository.AddAsync(new EmailActionView(request.EmailId, request.UserAgent));
            await _unitOfWork.CommitAsync(cancellationToken);
            return true;
        }
    }
}