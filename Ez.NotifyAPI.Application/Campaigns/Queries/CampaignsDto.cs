﻿using System;

namespace Ez.NotifyAPI.Application.Campaigns.Queries
{
    public class CampainsDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CampaignType { get; set; }
        public string Sender { get; set; }
        public string SenderTitle { get; set; }
        public string SenderCode { get; set; }
        public int? Zone { get; set; }
        public int ListId { get; set; }
    }
}