﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.Queries;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.Campaigns.Queries
// {
//     public class GetListCampaignsQuery : ListQuery, IRequest<PagingResponse<IList<CampainsDto>>>
//     {
//         public GetListCampaignsQuery(int pageIndex, int pageSize, string query, string sorts) : base(pageIndex,
//             pageSize, query, sorts)
//         {
//         }
//     }
//
//     public class GetListCampaignsQueryHandler : IRequestHandler<GetListCampaignsQuery,
//         PagingResponse<IList<CampainsDto>>>
//     {
//         private readonly IRepository<Domain.Entity.CampaignsLists> _listsRepository;
//         private readonly IRepository<Domain.Entity.Campaigns> _repository;
//
//         public GetListCampaignsQueryHandler(IRepository<Domain.Entity.Campaigns> repository,
//             IRepository<Domain.Entity.CampaignsLists> listsRepository)
//         {
//             _repository = repository;
//             _listsRepository = listsRepository;
//         }
//
//         public async Task<PagingResponse<IList<CampainsDto>>> Handle(GetListCampaignsQuery request,
//             CancellationToken cancellationToken)
//         {
//             var lists = await _listsRepository.GetAllAsync();
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _repository.QueryAsync(x =>
//                             string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<CampainsDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<CampainsDto>>(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var query = await
//                 _repository.QueryAsync(x =>
//                         string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%"),
//                     request.PageIndex, request.PageSize);
//             var response =
//                 new PagingResponse<IList<CampainsDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = ConvertListCompainToListCampainsDtos(query.Items.ToList(), lists.ToList()),
//                     Total = query.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//
//         private List<CampainsDto> ConvertListCompainToListCampainsDtos(List<Domain.Entity.Campaigns> campaignses,
//             List<Domain.Entity.CampaignsLists> list)
//         {
//             var result = new List<CampainsDto>();
//             foreach (var item in campaignses)
//             {
//                 var campaignsList = list.FirstOrDefault(x => x.CampaignId == item.Id);
//                 if (campaignsList != null)
//                     result.Add(new CampainsDto
//                     {
//                         Code = item.Code,
//                         Description = item.Description,
//                         Id = item.Id,
//                         Sender = item.Sender,
//                         Status = item.Status,
//                         Title = item.Title,
//                         Zone = item.Zone,
//                         CampaignType = item.CampaignType,
//                         CreatedBy = item.CreatedBy,
//                         CreatedDate = item.CreatedDate,
//                         HtmlBody = item.HtmlBody,
//                         SenderCode = item.SenderCode,
//                         SenderTitle = item.SenderTitle,
//                         TextBody = item.TextBody,
//                         ListId = campaignsList.ListId
//                     });
//                 else
//                     result.Add(new CampainsDto
//                     {
//                         Code = item.Code,
//                         Description = item.Description,
//                         Id = item.Id,
//                         Sender = item.Sender,
//                         Status = item.Status,
//                         Title = item.Title,
//                         Zone = item.Zone,
//                         CampaignType = item.CampaignType,
//                         CreatedBy = item.CreatedBy,
//                         CreatedDate = item.CreatedDate,
//                         HtmlBody = item.HtmlBody,
//                         SenderCode = item.SenderCode,
//                         SenderTitle = item.SenderTitle,
//                         TextBody = item.TextBody,
//                         ListId = 0
//                     });
//             }
//
//             return result;
//         }
//     }
// }