﻿using System;
using System.Linq.Expressions;
using Ez.NotifyAPI.Application.Campaigns.Queries;

namespace Ez.NotifyAPI.Application.Campaigns.Repositories
{
    public class CampaignsExpression
    {
        public static Expression<Func<global::Ez.Notify.Domain.Entity.Campaigns, CampainsDto>> Model => campaigns => new CampainsDto();

        public static Expression<Func<global::Ez.Notify.Domain.Entity.Campaigns, bool>> Id(int id)
        {
            return campaigns => campaigns.Id == id;
        }
    }
}