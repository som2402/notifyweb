﻿using System;
using System.Linq.Expressions;
using Ez.Notify.Domain.Entity;

namespace Ez.NotifyAPI.Application.EmailSenders.Repositories
{
    public static class EmailSenderExpression
    {
        public static Expression<Func<EmailSender, bool>> Id(int id) => emailSender => emailSender.Id.Equals(id);

        public static Expression<Func<EmailSender, bool>> Code(string code) =>
            emailSender => emailSender.Code.Equals(code);

        public static Expression<Func<EmailSender, bool>> Default() => emailSender => emailSender.Default.Value;
    }
}