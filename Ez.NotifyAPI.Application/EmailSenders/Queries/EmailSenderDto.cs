﻿namespace Ez.NotifyAPI.Application.EmailSenders.Queries
{
    public class EmailSenderDto
    {
        public string Code { get; set; }
        public string Title { get; set; }
    }
}