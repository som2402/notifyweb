﻿using Ez.Notify.Common.Constants;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Queries;

namespace Ez.NotifyAPI.Application.Extensions
{
    public static class CustomerNotifyConfigExtension
    {
        public static CustomerNotifyConfigDto MapStatusNotifyConfigToCustomerConfig(
            this CustomerNotifyConfigDto customerNotifyConfigDto,
            NotifyConfig notifyConfig, string accountId)
        {
            customerNotifyConfigDto.SendDesktop = notifyConfig.DesktopRequired.MappingStatus();
            customerNotifyConfigDto.SendMobile = notifyConfig.MobileRequired.MappingStatus();
            customerNotifyConfigDto.SendEmail = notifyConfig.Required.MappingStatus();
            customerNotifyConfigDto.SendWeb = notifyConfig.WebRequired.MappingStatus();
            customerNotifyConfigDto.NotifyConfigCode = notifyConfig.Code;
            customerNotifyConfigDto.AccountId = accountId;
            customerNotifyConfigDto.AppId = AppConstant.EzbuyAppId;
            return customerNotifyConfigDto;
        }
    }

    public static class MappingStatusExtension
    {
        public static int? MappingStatus(this int? status)
        {
            return status == NotifyConfigStatus.Off ? NotifyConfigStatus.InActive : status;
        }
    }
}