﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.WebNotifies.Repositories
{
    public interface IWebNotifyRepository : IRepository<WebNotify>
    {
        Task<IEnumerable<WebNotify>> GetByCustomerId(string customerId);
        Task<IEnumerable<WebNotify>> GetByCustomerId(string customerId, int take);
    }

    public class WebNotifyRepository : EfRepository<DataNotifyDbContext, WebNotify>, IWebNotifyRepository
    {
        public WebNotifyRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<WebNotify>> GetByCustomerId(string customerId)
        {
            return await Queryable.Where(WebNotifyExpression.CustomerId(customerId)).ToListAsync();
        }

        public async Task<IEnumerable<WebNotify>> GetByCustomerId(string customerId, int take)
        {
            return await Queryable.Where(WebNotifyExpression.CustomerId(customerId)).Take(take).ToListAsync();
        }
    }
}