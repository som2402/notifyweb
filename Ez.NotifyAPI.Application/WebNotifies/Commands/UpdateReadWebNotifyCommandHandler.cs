﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
//
// namespace Ez.Notify.Application.WebNotifies.Commands
// {
//     public class UpdateReadWebNotifyCommand : IRequest<BaseEntityResponse<bool>>
//     {
//         public UpdateReadWebNotifyCommand(IList<long> ids, string customerId)
//         {
//             Ids = ids;
//             CustomerId = customerId;
//         }
//
//         public IList<long> Ids { get; }
//         public string CustomerId { get; }
//     }
//
//     //public class UpdateReadWebNotifyCommandValidator : BaseValidator<UpdateReadWebNotifyCommand>
//     //{
//     //    public UpdateReadWebNotifyCommandValidator(IRepository<WebNotify> repository)
//     //    {
//     //        RuleFor(x => x.Id)
//     //            .GreaterThan(0)
//     //            .WithErrorCode("ID_VALUE_INVALID")
//     //            .WithState(_ => new Dictionary<string, string> {{"ORD_MIN_VALUE", "1"}}).Must(x =>
//     //                repository.GetByIdAsync(x).GetAwaiter().GetResult() != null)
//     //            .WithErrorCode("ID_NOT_FOUND");
//     //    }
//     //}
//
//     public class
//         UpdateReadWebNotifyCommandHandler : IRequestHandler<UpdateReadWebNotifyCommand, BaseEntityResponse<bool>>
//     {
//         private readonly IRepository<WebNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public UpdateReadWebNotifyCommandHandler(IRepository<WebNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<bool>> Handle(UpdateReadWebNotifyCommand request,
//             CancellationToken cancellationToken)
//         {
//             var webNotifies = await _repository.GetManyAsync(g => g.CustomerId == request.CustomerId && g.Read != true);
//             if (request.Ids != null && request.Ids.Count > 0)
//                 webNotifies = webNotifies.Where(g => request.Ids.Contains(g.Id)).ToList();
//
//             webNotifies.ToList().ForEach(item =>
//             {
//                 item.UpdateRead();
//                 _repository.Update(item);
//             });
//
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<bool>().SetData(true);
//             return response;
//         }
//     }
// }