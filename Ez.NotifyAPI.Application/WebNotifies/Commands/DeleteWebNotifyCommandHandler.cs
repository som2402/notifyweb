﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
//
// namespace Ez.Notify.Application.WebNotifies.Commands
// {
//     public class DeleteWebNotifyCommand : IRequest<BaseEntityResponse<bool>>
//     {
//         public DeleteWebNotifyCommand(IList<long> ids, string customerId)
//         {
//             Ids = ids;
//             CustomerId = customerId;
//         }
//
//         public IList<long> Ids { get; }
//         public string CustomerId { get; }
//     }
//
//     public class DeleteWebNotifyCommandHandler : IRequestHandler<DeleteWebNotifyCommand, BaseEntityResponse<bool>>
//     {
//         private readonly IRepository<WebNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public DeleteWebNotifyCommandHandler(IRepository<WebNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<bool>> Handle(DeleteWebNotifyCommand request,
//             CancellationToken cancellationToken)
//         {
//             var webNotifies = await _repository.GetManyAsync(g => g.CustomerId == request.CustomerId);
//             if (request.Ids != null && request.Ids.Count > 0)
//                 webNotifies = webNotifies.Where(g => request.Ids.Contains(g.Id)).ToList();
//
//             webNotifies.ToList().ForEach(item => { _repository.Delete(item); });
//
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<bool>().SetData(true);
//             return response;
//         }
//     }
// }