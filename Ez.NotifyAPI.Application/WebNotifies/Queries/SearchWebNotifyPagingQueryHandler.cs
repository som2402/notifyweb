﻿// using System.Collections.Generic;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.Queries;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.WebNotifies.Queries
// {
//     public class SearchWebNotifyPagingQuery : ListQuery, IRequest<PagingResponse<IList<WebNotifyDto>>>
//     {
//         public SearchWebNotifyPagingQuery(int pageIndex, int pageSize, string query, string sorts, string customerId,
//             string group, bool? read) : base(pageIndex, pageSize, query, sorts)
//         {
//             CustomerId = customerId;
//             Group = group;
//             Read = read;
//         }
//
//         public string CustomerId { get; }
//         public string Group { get; }
//         public bool? Read { get; }
//     }
//
//     public class
//         SearchWebNotifyPagingQueryHandler : IRequestHandler<SearchWebNotifyPagingQuery,
//             PagingResponse<IList<WebNotifyDto>>>
//     {
//         private readonly IRepository<WebNotify> _repository;
//
//         public SearchWebNotifyPagingQueryHandler(IRepository<WebNotify> repository)
//         {
//             _repository = repository;
//         }
//
//         public async Task<PagingResponse<IList<WebNotifyDto>>> Handle(SearchWebNotifyPagingQuery request,
//             CancellationToken cancellationToken)
//         {
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _repository.QueryAsync(x =>
//                             (string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%")) &&
//                             (string.IsNullOrEmpty(request.Group) || x.Group.Equals(request.Group)) &&
//                             (string.IsNullOrEmpty(request.CustomerId) || x.CustomerId.Equals(request.CustomerId)) &&
//                             (request.Read == null || request.Read == false && (x.Read == null || x.Read == false) ||
//                              request.Read == true && x.Read == true),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<WebNotifyDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<WebNotifyDto>>(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var sortsDefault = new Sorts
//             {
//                 new Sort {SortBy = "CreatedDate", SortDirection = Sort.SORT_DIRECTION_DESC}
//             };
//             var query = await
//                 _repository.QueryAsync(x =>
//                         (string.IsNullOrEmpty(request.CustomerId) || x.CustomerId.Equals(request.CustomerId)) &&
//                         (string.IsNullOrEmpty(request.Group) || x.Group.Equals(request.Group)) &&
//                         (request.Read == null || request.Read == false && (x.Read == null || x.Read == false) ||
//                          request.Read == true && x.Read == true) &&
//                         (string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Title, $"%{request.Query}%")),
//                     request.PageIndex, request.PageSize, sortsDefault);
//             var response =
//                 new PagingResponse<IList<WebNotifyDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = query.Items.Map<List<WebNotifyDto>>(),
//                     Total = query.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//     }
// }