﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.WebNotifies.Repositories;
using MediatR;
using Shared.BaseModel;
using Shared.Extensions;

namespace Ez.NotifyAPI.Application.WebNotifies.Queries
{
    public class GetListWebNotifyByCustomerIdQuery : IRequest<BaseEntityResponse<List<WebNotifyDto>>>
    {
        public GetListWebNotifyByCustomerIdQuery(string customerId)
        {
            CustomerId = customerId;
        }

        public string CustomerId { get; }
    }

    public class GetListWebNotifyByCustomerIdQueryHandler : IRequestHandler<GetListWebNotifyByCustomerIdQuery,
        BaseEntityResponse<List<WebNotifyDto>>>
    {
        private readonly IWebNotifyRepository _repository;

        public GetListWebNotifyByCustomerIdQueryHandler(IWebNotifyRepository repository)
        {
            _repository = repository;
        }


        public async Task<BaseEntityResponse<List<WebNotifyDto>>> Handle(GetListWebNotifyByCustomerIdQuery request,
            CancellationToken cancellationToken)
        {
            var result = await _repository.GetByCustomerId(request.CustomerId, 100);
            var response = new BaseEntityResponse<List<WebNotifyDto>>
            {
                Data = result.Map<List<WebNotifyDto>>(),
                Status = true
            };
            return response;
        }
    }
}