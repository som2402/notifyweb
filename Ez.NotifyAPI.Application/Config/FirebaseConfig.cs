﻿namespace Ez.NotifyAPI.Application.Config
{
    public class FirebaseConfig
    {
        public string GoogleCredential { get; set; }
    }
}