﻿namespace Ez.NotifyAPI.Application.Config
{
    public class RabbitMqConnectionConfig
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}