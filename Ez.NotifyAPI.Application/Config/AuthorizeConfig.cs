﻿namespace Ez.NotifyAPI.Application.Config
{
    public class AuthorizeConfig
    {
        public string Issuer { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public string ApiName { get; set; }
    }
}