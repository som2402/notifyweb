﻿using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Domain.Entity;
using Ez.NotifyAPI.Application.EmailActionClicks.Repositories;
using MediatR;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.EmailActionClicks.Command
{
    public class AddEmailActionClickCommand : IRequest<bool>
    {
        public AddEmailActionClickCommand(int emailId, string link)
        {
            EmailId = emailId;
            Link = link;
        }

        public int EmailId { get; private set; }
        public string Link { get; private set; }
    }

    public class AddEmailActionClickCommandHandler : IRequestHandler<AddEmailActionClickCommand, bool>
    {
        private readonly IEmailActionClickRepository _emailActionClickRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddEmailActionClickCommandHandler(IEmailActionClickRepository emailActionClickRepository,
            IUnitOfWork unitOfWork)
        {
            _emailActionClickRepository = emailActionClickRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddEmailActionClickCommand request, CancellationToken cancellationToken)
        {
            await _emailActionClickRepository.AddAsync(new EmailActionClick(request.EmailId, request.Link));
            await _unitOfWork.CommitAsync(cancellationToken);
            return true;
        }
    }
}