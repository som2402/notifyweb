﻿// using System.Threading;
// using System.Threading.Tasks;
// using Ez.IS4.API.Driver;
// using MediatR;
// using Ez.Notify.Application.Config;
// using Shared.BaseModel;
//
// namespace Ez.Notify.Application.AcessApps
// {
//     public class CheckPermissionQuery : IRequest<BaseResponse>
//     {
//         public CheckPermissionQuery(string resourceKey, string permission)
//         {
//             ResourceKey = resourceKey;
//             Permission = permission;
//         }
//
//         public string ResourceKey { get; }
//         public string Permission { get; }
//     }
//
//     public class CheckPermissionQueryHandler : IRequestHandler<CheckPermissionQuery, BaseResponse>
//     {
//         private readonly AccessClient _accessClient;
//         private readonly AppConfig _appConfig;
//
//         public CheckPermissionQueryHandler(AppConfig appConfig, AccessClient accessClient)
//         {
//             _appConfig = appConfig;
//             _accessClient = accessClient;
//         }
//
//         public async Task<BaseResponse> Handle(CheckPermissionQuery request, CancellationToken cancellationToken)
//         {
//             var response = new BaseResponse();
//             var data = await _accessClient.CheckPermission(_appConfig.AppGroupResourceKey, request.ResourceKey,
//                 request.Permission);
//             if (data) response.Status = true;
//
//             return response;
//         }
//     }
// }