﻿// using System.Collections.Generic;
// using System.IO;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Application.CampaignsLists.Queries;
// using Ez.Notify.Application.Lists.Queries;
// using Ez.Notify.Application.Subcribes.Queries;
// using Ez.Notify.Domain.Entity;
// using OfficeOpenXml;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Services;
//
// namespace Ez.Notify.Application.Lists.Commands
// {
//     public class ImportListsCommand : IRequest<BaseEntityResponse<List<ListsDto>>>
//     {
//         public ImportListsCommand(string filePath, string title, string description, string code)
//         {
//             FilePath = filePath;
//             Title = title;
//             Description = description;
//             Code = code;
//         }
//
//         public string FilePath { get; }
//         public string Title { get; }
//         public string Description { get; }
//         public string Code { get; }
//     }
//
//     public class ImportListsCommandHandler : IRequestHandler<ImportListsCommand, BaseEntityResponse<List<ListsDto>>>
//     {
//         private readonly IRepository<Domain.Entity.Lists> _listsRepository;
//         private readonly ISequenceService _sequenceService;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public ImportListsCommandHandler(
//             IRepository<Domain.Entity.Lists> listsRepository, IUnitOfWork unitOfWork, ISequenceService sequenceService)
//         {
//             _listsRepository = listsRepository;
//             _unitOfWork = unitOfWork;
//             _sequenceService = sequenceService;
//         }
//
//         public async Task<BaseEntityResponse<List<ListsDto>>> Handle(ImportListsCommand request,
//             CancellationToken cancellationToken)
//         {
//             await _sequenceService.TurnOnInsertIdentityWithSubcribes();
//             using (var package = new ExcelPackage(new FileInfo(request.FilePath)))
//             {
//                 var workSheet = package.Workbook.Worksheets[0];
//                 var id = await _sequenceService.GetNotifyDataNewId();
//                 var lists = new Domain.Entity.Lists();
//                 lists.Code = request.Code;
//                 lists.Description = request.Description;
//                 lists.Title = request.Title;
//                 for (var i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
//                 {
//                     var subscribes = new Subscribes();
//                     subscribes.Name = workSheet.Cells[i, 1].Value.ToString();
//                     subscribes.Email = workSheet.Cells[i, 2].Value.ToString();
//                     subscribes.Code = workSheet.Cells[i, 3].Value.ToString();
//                     subscribes.AccountId = workSheet.Cells[i, 3].Value.ToString();
//                     subscribes.ListId = (int) id;
//                     lists.Subscribes.Add(subscribes);
//                 }
//
//                 _listsRepository.Add(lists);
//                 await _unitOfWork.CommitAsync(cancellationToken);
//             }
//
//             var result = await _listsRepository.GetAllAsync();
//             var response = new BaseEntityResponse<List<ListsDto>>
//             {
//                 Data = ConvertListToListsDto(result.ToList()),
//                 Status = true
//             };
//             return response;
//         }
//
//         private List<ListsDto> ConvertListToListsDto(List<Domain.Entity.Lists> list)
//         {
//             var result = new List<ListsDto>();
//             foreach (var item in list)
//             {
//                 var listDto = new ListsDto
//                 {
//                     Code = item.Code,
//                     Id = item.Id,
//                     Description = item.Description,
//                     Sub = item.Sub,
//                     Title = item.Title
//                 };
//                 foreach (var itemj in item.Subscribes.ToList())
//                     listDto.Subscribes.Add(new SubscribesDto
//                     {
//                         Code = itemj.Code,
//                         Email = itemj.Email,
//                         Id = itemj.Id,
//                         Name = itemj.Name,
//                         AccountId = itemj.AccountId,
//                         ListId = itemj.ListId
//                     });
//
//                 foreach (var itemz in item.CampaignsLists.ToList())
//                     listDto.CampaignsLists.Add(new CampaignsListsDto
//                     {
//                         Id = itemz.Id,
//                         Status = itemz.Status,
//                         Sub = itemz.Sub,
//                         CampaignId = itemz.CampaignId,
//                         ListId = itemz.ListId
//                     });
//
//                 result.Add(listDto);
//             }
//
//             return result;
//         }
//     }
// }