﻿// using System.Collections.Generic;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Microsoft.EntityFrameworkCore;
// using Ez.Notify.Application.Queries;
// using Ez.Notify.Application.Subcribes.Queries;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.Common.Sort;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
//
// namespace Ez.Notify.Application.Lists.Queries
// {
//     public class GetSubcribesByListIdQuery : ListQuery, IRequest<PagingResponse<IList<SubscribesDto>>>
//     {
//         public GetSubcribesByListIdQuery(int pageIndex, int pageSize, string query, string sorts, int listId) : base(
//             pageIndex, pageSize, query, sorts)
//         {
//             ListId = listId;
//         }
//
//         public int ListId { get; }
//     }
//
//     public class
//         GetSubcribesByListIdQueryHandler : IRequestHandler<GetSubcribesByListIdQuery,
//             PagingResponse<IList<SubscribesDto>>>
//     {
//         private readonly IRepository<Subscribes> _repository;
//
//         public GetSubcribesByListIdQueryHandler(IRepository<Subscribes> repository)
//         {
//             _repository = repository;
//         }
//
//
//         public async Task<PagingResponse<IList<SubscribesDto>>> Handle(GetSubcribesByListIdQuery request,
//             CancellationToken cancellationToken)
//         {
//             if (!string.IsNullOrEmpty(request.Sorts))
//             {
//                 var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
//                 var sorts = new Sorts(sortConditions);
//                 var querySortResult = await
//                     _repository.QueryAsync(x =>
//                             (string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Name, $"%{request.Query}%")) &&
//                             x.ListId.Equals(request.ListId),
//                         request.PageIndex, request.PageSize, sorts);
//                 var sortResponse =
//                     new PagingResponse<IList<SubscribesDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                     {
//                         Data = querySortResult.Items.Map<List<SubscribesDto>>(),
//                         Total = querySortResult.Count,
//                         PageIndex = request.PageIndex,
//                         PageSize = request.PageSize
//                     };
//                 return sortResponse;
//             }
//
//             var query = await
//                 _repository.QueryAsync(x =>
//                         (string.IsNullOrEmpty(request.Query) || EF.Functions.Like(x.Name, $"%{request.Query}%")) &&
//                         x.ListId.Equals(request.ListId),
//                     request.PageIndex, request.PageSize);
//             var response =
//                 new PagingResponse<IList<SubscribesDto>>(true, new List<DetailError> {new DetailError(null, null)})
//                 {
//                     Data = query.Items.Map<List<SubscribesDto>>(),
//                     Total = query.Count,
//                     PageIndex = request.PageIndex,
//                     PageSize = request.PageSize
//                 };
//             return response;
//         }
//     }
// }