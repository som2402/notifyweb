﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ez.Notify.Common.Constants;
using Ez.Notify.Domain;
using Ez.Notify.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Shared.Common.Sort;
using Shared.Dto;
using Shared.EF.Implement;
using Shared.EF.Interfaces;

namespace Ez.NotifyAPI.Application.EmailNotifies.Repositories
{
    public interface IEmailNotifyRepository : IRepository<EmailNotify>
    {
        Task<EmailNotify> GetByIdAsync(long id);
        Task<IEnumerable<EmailNotify>> GetByRefCode(string refCode);
        Task<IEnumerable<EmailNotify>> GetByAccountId(string accountId);

        Task<QueryResult<EmailNotify>> GetListByAccountId(string accountId, string refType, int pageIndex, int pageSize,
            int? status = null, Sorts sort = null);

        Task<EmailNotify> GetByIdAsync(long id, string accountId);

        Task<IEnumerable<EmailNotify>> GetByIds(string accountId, params long[] emailNotifyIds);
    }

    public class EmailNotifyRepository : EfRepository<DataNotifyDbContext, EmailNotify>, IEmailNotifyRepository
    {
        public EmailNotifyRepository(DataNotifyDbContext context) : base(context)
        {
        }

        public async Task<EmailNotify> GetByIdAsync(long id)
        {
            return await Queryable.Where(EmailNotifyExpression.Id(id)).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByRefCode(string refCode)
        {
            return await Queryable.Where(EmailNotifyExpression.RefCode(refCode)).Take(100).OrderBy(x => x.CreatedDate)
                .ToListAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByAccountId(string accountId)
        {
            return await Queryable.Where(EmailNotifyExpression.CustomerId(accountId)).Take(100)
                .OrderBy(x => x.CreatedDate)
                .ToListAsync();
        }

        public async Task<QueryResult<EmailNotify>> GetListByAccountId(string accountId, string refType, int pageIndex,
            int pageSize, int? status, Sorts sort = null)
        {
            var data = Queryable.Where(x => x.AccountId == accountId
                                            && (
                                                (status == null &&
                                                 x.Status != EmailNotifyStatus.Deleted) ||
                                                (status == EmailNotifyStatus.Deleted &&
                                                 x.Status == EmailNotifyStatus.Deleted) ||
                                                (status == EmailNotifyStatus.UnRead &&
                                                 (x.Status == EmailNotifyStatus.UnRead ||
                                                  x.Status == null)) ||
                                                (x.Status == status)
                                            )
                                            && (string.IsNullOrWhiteSpace(refType) || x.RefType == refType)
            );
            var res = await data.ToQueryResultAsync(pageIndex, pageSize, sort);
            return res;
        }

        public async Task<EmailNotify> GetByIdAsync(long id, string accountId)
        {
            return await Queryable.Where(x => x.Id == id && x.AccountId == accountId).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<EmailNotify>> GetByIds(string accountId, params long[] emailNotifyIds)
        {
            return await Queryable.Where(x => x.AccountId == accountId && emailNotifyIds.Contains(x.Id))
                .ToListAsync();
        }
    }
}