﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Common.Constants;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Common.Models;
using Ez.Notify.Common.Validators;
using Ez.Notify.Domain.Entity;
using Ez.Notify.MQ.Driver;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Services;
using Shared.Validators;

namespace Ez.NotifyAPI.Application.EmailNotifies.Command
{
    
    public class SendEmailNotifyCommandValidator : BaseValidator<SendEmailNotifyCommand>
    {
        public SendEmailNotifyCommandValidator()
        {
            RuleForEach(x => x.Attachments).SetValidator(new FileValidator());
        }
    }

    public class SendEmailNotifyCommandHandler : IRequestHandler<SendEmailNotifyCommand>
    {
        private readonly ICustomerNotifyConfigRepository _customerNotifyConfigRepository;
        private readonly IEmailNotifyRepository _emailNotifyRepository;
        private readonly IEmailSenderRepository _emailSenderRepository;
        private readonly INotifyConfigRepository _notifyConfigRepository;
        private readonly MessageQueueClient _messageQueueClient;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserContext _userContext;

        public SendEmailNotifyCommandHandler(ICustomerNotifyConfigRepository customerNotifyConfigRepository,
            IEmailNotifyRepository emailNotifyRepository, IEmailSenderRepository emailSenderRepository,
            MessageQueueClient messageQueueClient, IUnitOfWork unitOfWork, IUserContext userContext,
            INotifyConfigRepository notifyConfigRepository)
        {
            _customerNotifyConfigRepository = customerNotifyConfigRepository;
            _emailNotifyRepository = emailNotifyRepository;
            _emailSenderRepository = emailSenderRepository;
            _messageQueueClient = messageQueueClient;
            _unitOfWork = unitOfWork;
            _userContext = userContext;
            _notifyConfigRepository = notifyConfigRepository;
        }


        public async Task<Unit> Handle(SendEmailNotifyCommand request, CancellationToken cancellationToken)
        {
            var listEmailAttachment = new List<EmailAttachment>();
            var fileAttachment = new List<FileAttachment>();
            await ProcessAttachment(request, cancellationToken, fileAttachment);
            var notifyConfig =
                await _notifyConfigRepository.GetNotifyConfigByCode(request.RefType);
            var id = await AddNotifyEmailToDatabase(request, cancellationToken, fileAttachment, listEmailAttachment,
                notifyConfig);
            var customerNotifyConfig = await _customerNotifyConfigRepository.FindByExpression(
                AppConstant.EzbuyAppId, request.AccountId, request.RefType);
            if (customerNotifyConfig != null &&
                (customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.InActive ||
                 customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.Developing))
            {
                return Unit.Value;
            }

            if (notifyConfig != null && notifyConfig.Required == NotifyConfigStatus.Off)
            {
                return Unit.Value;
            }

            await _messageQueueClient.SendEmail(new SendEmailNotifyToMessageQueueRequest(id,
                fileAttachment));
            return Unit.Value;
        }


        private async Task<long> AddNotifyEmailToDatabase(SendEmailNotifyCommand request,
            CancellationToken cancellationToken,
            List<FileAttachment> fileAttachment, List<EmailAttachment> listEmailAttachment, NotifyConfig notifyConfig)
        {
            EmailSender emailSender = null;
            string replaceSenderTitle;
            string replaceSenderCode;
            if (!string.IsNullOrEmpty(request.SenderCode))
                emailSender = await _emailSenderRepository.GetByCode(request.SenderCode);

            if (emailSender == null) emailSender = await _emailSenderRepository.GetByDefault();

            if (!string.IsNullOrEmpty(request.SenderTitle) && request.SenderTitle != "null")
                replaceSenderTitle = request.SenderTitle;
            else
                replaceSenderTitle = emailSender.Title;

            if (!string.IsNullOrEmpty(request.SenderCode) && request.SenderCode != "null")
                replaceSenderCode = request.SenderCode;
            else
                replaceSenderCode = emailSender.Code;
            var textBody = request.Html.ExtractText();
            var emailNotify = new EmailNotify(request.Subject, textBody.ReplaceWhiteSpace(), request.Html,
                request.To,
                string.Join(",", request.Cc),
                string.Join(",", request.Bcc), emailSender.Email, replaceSenderTitle, replaceSenderCode,
                request.RefCode, request.RefType,
                DateTime.UtcNow, _userContext.UserId.ToString(), string.Empty, null, 0, request.Zone,
                request.AccountId);
            if (fileAttachment.Count > 0)
                listEmailAttachment.AddRange(fileAttachment.Select(item =>
                    new EmailAttachment(item.FileName, item.Bytes)));
            var customerNotifyConfig = await _customerNotifyConfigRepository.FindByExpression(
                AppConstant.EzbuyAppId, request.AccountId, request.RefType);
            if (customerNotifyConfig != null &&
                (customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.InActive ||
                 customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.Developing))
                emailNotify.UpdateStatus(SendStatusConstant.UserRefusedToSubmit);
            else if (notifyConfig != null && notifyConfig.Required == NotifyConfigStatus.Off)
                emailNotify.UpdateStatus(SendStatusConstant.UserRefusedToSubmit);
            emailNotify.EmailAttachment = listEmailAttachment;
            await _emailNotifyRepository.AddAsync(emailNotify);
            await _unitOfWork.CommitAsync(cancellationToken);
            return emailNotify.Id;
        }

        private static async Task ProcessAttachment(SendEmailNotifyCommand request, CancellationToken cancellationToken,
            List<FileAttachment> fileAttachment)
        {
            if (request.Attachments != null)
                foreach (var file in request.Attachments.Where(file => file.Length > 0))
                {
                    byte[] fileBytes;
                    await using (var ms = new MemoryStream())
                    {
                        await file.CopyToAsync(ms, cancellationToken);
                        fileBytes = ms.ToArray();
                    }

                    var extension = Path.GetExtension(file.FileName);
                    var contentType = extension.GetMimeType();
                    fileAttachment.Add(new FileAttachment(fileBytes, file.FileName, contentType));
                }
        }
    }

    public class SendEmailNotifyCommand : IRequest
    {
        public SendEmailNotifyCommand(string to, string[] bcc, string[] cc, string senderCode,
            string senderTitle, string refCode, string refType, string subject,
            string html, List<IFormFile> attachments, int? zone, string accountId)
        {
            To = to;
            Bcc = bcc;
            Cc = cc;
            SenderCode = senderCode;
            SenderTitle = senderTitle;
            RefCode = refCode;
            RefType = refType;
            Subject = subject;
            Html = html;
            Attachments = attachments;
            Zone = zone;
            AccountId = accountId;
        }

        public string To { get; }
        public string[] Bcc { get; }
        public string[] Cc { get; }
        public string SenderCode { get; }
        public string SenderTitle { get; }
        public string RefCode { get; }
        public string RefType { get; }
        public string Subject { get; }
        public string Html { get; }
        public List<IFormFile> Attachments { get; }
        public int? Zone { get; }
        public string AccountId { get; }
    }
}