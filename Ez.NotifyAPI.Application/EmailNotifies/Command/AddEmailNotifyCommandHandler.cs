﻿// using System;
// using System.Threading;
// using System.Threading.Tasks;
// using MediatR;
// using Ez.Notify.Domain.Entity;
// using Shared.BaseModel;
// using Shared.EF.Interfaces;
// using Shared.Extensions;
// using Shared.Validators;
//
// namespace Ez.Notify.Application.EmailNotifies.Command
// {
//     public class AddEmailNotifyCommand : IRequest<BaseEntityResponse<EmailNotify>>
//     {
//         public AddEmailNotifyCommand(string subject, string textBody, string htmlBody, string to, string cc, string bcc,
//             string sender, string senderTitle, string senderCode, string refCode, string refType, string customerId,
//             DateTime? createdDate, string createdBy, string sendMessage, DateTime? sentDate, int? sentCount)
//         {
//             Subject = subject;
//             TextBody = textBody;
//             HtmlBody = htmlBody;
//             To = to;
//             Cc = cc;
//             Bcc = bcc;
//             Sender = sender;
//             SenderTitle = senderTitle;
//             SenderCode = senderCode;
//             RefCode = refCode;
//             RefType = refType;
//             CustomerId = customerId;
//             CreatedDate = createdDate;
//             CreatedBy = createdBy;
//             SendMessage = sendMessage;
//             SentDate = sentDate;
//             SentCount = sentCount;
//         }
//
//         public string Subject { get; }
//         public string TextBody { get; }
//         public string HtmlBody { get; }
//         public string To { get; }
//         public string Cc { get; }
//         public string Bcc { get; }
//         public string Sender { get; }
//         public string SenderTitle { get; }
//         public string SenderCode { get; }
//         public string RefCode { get; }
//         public string RefType { get; }
//         public string CustomerId { get; }
//         public DateTime? CreatedDate { get; }
//         public string CreatedBy { get; }
//         public string SendMessage { get; }
//         public DateTime? SentDate { get; }
//         public int? SentCount { get; }
//     }
//
//     public class AddEmailNotifyCommandValidator : BaseValidator<AddEmailNotifyCommand>
//     {
//     }
//
//     public class AddEmailNotifyCommandHandler : IRequestHandler<AddEmailNotifyCommand, BaseEntityResponse<EmailNotify>>
//     {
//         private readonly IRepository<EmailNotify> _repository;
//         private readonly IUnitOfWork _unitOfWork;
//
//         public AddEmailNotifyCommandHandler(IRepository<EmailNotify> repository, IUnitOfWork unitOfWork)
//         {
//             _repository = repository;
//             _unitOfWork = unitOfWork;
//         }
//
//         public async Task<BaseEntityResponse<EmailNotify>> Handle(AddEmailNotifyCommand request,
//             CancellationToken cancellationToken)
//         {
//             var data = _repository.Add(request.Map<EmailNotify>());
//             await _unitOfWork.CommitAsync(cancellationToken);
//             var response = new BaseEntityResponse<EmailNotify>();
//             response.SetData(data);
//             return response;
//         }
//     }
// }