﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Ez.NotifyAPI.Application.EmailNotifies.Command.Request
{
    public class SendEmailNotifyRequest
    {
        public string To { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string SenderCode { get; set; }
        public string SenderTitle { get; set; }
        public string RefCode { get; set; }
        public string RefType { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }
        public List<IFormFile> Attachments { get; set; }
        public int? Zone { get; set; }
        public string AccountId { get; set; }
    }
}