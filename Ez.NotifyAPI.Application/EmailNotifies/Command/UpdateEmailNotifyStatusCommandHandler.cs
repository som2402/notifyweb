﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using FluentValidation;
using MediatR;
using Shared.BaseModel;
using Shared.EF.Interfaces;
using Shared.Validators;

namespace Ez.NotifyAPI.Application.EmailNotifies.Command
{
    public class UpdateEmailNotifyStatusCommand : IRequest<BaseResponse>
    {
        public UpdateEmailNotifyStatusCommand(int? status, string accountId, params long[] emailNotifyIds)
        {
            Status = status;
            EmailNotifyIds = emailNotifyIds;
            AccountId = accountId;
        }

        public string AccountId { get; set; }
        public int? Status { get; }
        public long[] EmailNotifyIds { get; }
    }

    public class UpdateEmailNotifyStatusCommandValidator : BaseValidator<UpdateEmailNotifyStatusCommand>
    {
        public UpdateEmailNotifyStatusCommandValidator(IEmailNotifyRepository repository)
        {
            RuleFor(x => x).MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfigs = await repository.GetByIds(x.AccountId, x.EmailNotifyIds);
                return notifyConfigs?.Count() > 0;
            }).WithErrorCode("ID_NOT_FOUND");
        }
    }

    public class UpdateEmailNotifyStatusCommandHandler : IRequestHandler<UpdateEmailNotifyStatusCommand, BaseResponse>
    {
        private readonly IEmailNotifyRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmailNotifyStatusCommandHandler(IEmailNotifyRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseResponse> Handle(UpdateEmailNotifyStatusCommand request,
            CancellationToken cancellationToken)
        {
            var emailNotifies = await _repository.GetByIds(request.AccountId, request.EmailNotifyIds);
            var emailNotifiesUpdate = emailNotifies.Select(x =>
            {
                x.Status = request.Status;
                return x;
            }).ToList();

            await _repository.UpdateRangeAsync(emailNotifiesUpdate);
            await _unitOfWork.CommitAsync(cancellationToken);
            var response = new BaseResponse(true, null);
            return response;
        }
    }
}