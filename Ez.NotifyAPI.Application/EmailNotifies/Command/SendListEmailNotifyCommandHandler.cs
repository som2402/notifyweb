﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Ez.Notify.Common.Constants;
using Ez.Notify.Common.Interfaces;
using Ez.Notify.Common.Models;
using Ez.Notify.Common.Validators;
using Ez.Notify.Domain.Entity;
using Ez.Notify.MQ.Driver;
using Ez.NotifyAPI.Application.CustomerNotifyConfigs.Repositories;
using Ez.NotifyAPI.Application.EmailNotifies.Command.Request;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using Ez.NotifyAPI.Application.NotifyConfigs.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Shared.EF.Interfaces;
using Shared.Extensions;
using Shared.Services;
using Shared.Validators;
using Shared.Validators.Regexs;

namespace Ez.NotifyAPI.Application.EmailNotifies.Command
{
    public class SendListEmailNotifyCommand : IRequest
    {
        public SendListEmailNotifyCommand(List<SendEmailNotifyRequest> sendEmailNotifyRequests)
        {
            SendEmailNotifyRequests = sendEmailNotifyRequests;
        }

        public List<SendEmailNotifyRequest> SendEmailNotifyRequests { get; private set; }
    }

    public class ListFileValidator : BaseValidator<SendEmailNotifyRequest>
    {
        public ListFileValidator()
        {
            RuleForEach(x => x.Attachments).SetValidator(new FileValidator());
        }
    }


    public class SendListEmailNotifyCommandValidator : BaseValidator<SendListEmailNotifyCommand>
    {
        public SendListEmailNotifyCommandValidator()
        {
            RuleForEach(x => x.SendEmailNotifyRequests).SetValidator(new ListFileValidator());
        }
    }

    public class SendListEmailNotifyCommandHandler : IRequestHandler<SendListEmailNotifyCommand>
    {
        private readonly ICustomerNotifyConfigRepository _customerNotifyConfigRepository;
        private readonly IEmailNotifyRepository _emailNotifyRepository;
        private readonly IEmailSenderRepository _emailSenderRepository;
        private readonly INotifyConfigRepository _notifyConfigRepository;
        private readonly MessageQueueClient _messageQueueClient;
        private readonly IUnitOfWork _unitOfWork;

        public SendListEmailNotifyCommandHandler(ICustomerNotifyConfigRepository customerNotifyConfigRepository,
            IEmailNotifyRepository emailNotifyRepository, IEmailSenderRepository emailSenderRepository,
            INotifyConfigRepository notifyConfigRepository, MessageQueueClient messageQueueClient,
            IUnitOfWork unitOfWork)
        {
            _customerNotifyConfigRepository = customerNotifyConfigRepository;
            _emailNotifyRepository = emailNotifyRepository;
            _emailSenderRepository = emailSenderRepository;
            _notifyConfigRepository = notifyConfigRepository;
            _messageQueueClient = messageQueueClient;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(SendListEmailNotifyCommand request, CancellationToken cancellationToken)
        {
            var listEmailAttachment = new List<EmailAttachment>();
            var fileAttachment = new List<FileAttachment>();
            foreach (var item in request.SendEmailNotifyRequests)
            {
                await ProcessAttachment(item, cancellationToken, fileAttachment);
                var notifyConfig =
                    await _notifyConfigRepository.GetNotifyConfigByCode(item.RefType);
                var id = await AddNotifyEmailToDatabase(item, cancellationToken, fileAttachment, listEmailAttachment,
                    notifyConfig);
                var customerNotifyConfig = await _customerNotifyConfigRepository.FindByExpression(
                    AppConstant.EzbuyAppId, item.AccountId, item.RefType);
                if (customerNotifyConfig != null &&
                    (customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.InActive ||
                     customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.Developing))
                {
                    return Unit.Value;
                }

                if (notifyConfig != null && notifyConfig.Required == NotifyConfigStatus.Off)
                {
                    return Unit.Value;
                }

                await _messageQueueClient.SendEmail(new SendEmailNotifyToMessageQueueRequest(id,
                    fileAttachment));
                return Unit.Value;
            }

            return Unit.Value;
        }

        private async Task<long> AddNotifyEmailToDatabase(SendEmailNotifyRequest request,
            CancellationToken cancellationToken,
            List<FileAttachment> fileAttachment, List<EmailAttachment> listEmailAttachment, NotifyConfig notifyConfig)
        {
            EmailSender emailSender = null;
            string replaceSenderTitle;
            string replaceSenderCode;
            if (!string.IsNullOrEmpty(request.SenderCode))
                emailSender = await _emailSenderRepository.GetByCode(request.SenderCode);

            emailSender ??= await _emailSenderRepository.GetByDefault();

            if (!string.IsNullOrEmpty(request.SenderTitle) && request.SenderTitle != "null")
                replaceSenderTitle = request.SenderTitle;
            else
                replaceSenderTitle = emailSender.Title;

            if (!string.IsNullOrEmpty(request.SenderCode) && request.SenderCode != "null")
                replaceSenderCode = request.SenderCode;
            else
                replaceSenderCode = emailSender.Code;
            var textBody = request.Html.ExtractText();
            Regex emailRegex = new Regex(Regexes.Email);
            var bcc = new string[] { };
            var cc = new string[] { };

            if (!string.IsNullOrEmpty(request.Bcc))
            {
                bcc = request.Bcc.Split(",");
            }

            if (!string.IsNullOrEmpty(request.Cc))
            {
                cc = request.Cc.Split(",");
            }

            bcc = bcc.Where(x => emailRegex.Match(x).Success).ToArray();
            cc = cc.Where(x => emailRegex.Match(x).Success).ToArray();

            var emailNotify = new EmailNotify(request.Subject, textBody.ReplaceWhiteSpace(), request.Html,
                request.To,
                string.Join(",", cc),
                string.Join(",", bcc), emailSender.Email, replaceSenderTitle, replaceSenderCode,
                request.RefCode, request.RefType,
                DateTime.UtcNow, Guid.NewGuid().ToString(), string.Empty, null, 0, request.Zone,
                request.AccountId);
            if (fileAttachment.Count > 0)
                listEmailAttachment.AddRange(fileAttachment.Select(item =>
                    new EmailAttachment(item.FileName, item.Bytes)));
            var customerNotifyConfig = await _customerNotifyConfigRepository.FindByExpression(
                AppConstant.EzbuyAppId, request.AccountId, request.RefType);
            if (customerNotifyConfig != null &&
                (customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.InActive ||
                 customerNotifyConfig.SendEmail == CustomerNotifyConfigStatus.Developing))
                emailNotify.UpdateStatus(SendStatusConstant.UserRefusedToSubmit);
            else if (notifyConfig != null && notifyConfig.Required == NotifyConfigStatus.Off)
                emailNotify.UpdateStatus(SendStatusConstant.UserRefusedToSubmit);
            emailNotify.EmailAttachment = listEmailAttachment;
            await _emailNotifyRepository.AddAsync(emailNotify);
            await _unitOfWork.CommitAsync(cancellationToken);
            return emailNotify.Id;
        }

        private static async Task ProcessAttachment(SendEmailNotifyRequest request, CancellationToken cancellationToken,
            List<FileAttachment> fileAttachment)
        {
            if (request.Attachments != null)
                foreach (var file in request.Attachments.Where(file => file.Length > 0))
                {
                    byte[] fileBytes;
                    await using (var ms = new MemoryStream())
                    {
                        await file.CopyToAsync(ms, cancellationToken);
                        fileBytes = ms.ToArray();
                    }

                    var extension = Path.GetExtension(file.FileName);
                    var contentType = extension.GetMimeType();
                    fileAttachment.Add(new FileAttachment(fileBytes, file.FileName, contentType));
                }
        }
    }
}