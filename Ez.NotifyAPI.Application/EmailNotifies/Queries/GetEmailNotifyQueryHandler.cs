﻿using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using FluentValidation;
using MediatR;
using Shared.BaseModel;
using Shared.Extensions;
using Shared.Validators;

namespace Ez.NotifyAPI.Application.EmailNotifies.Queries
{
    public class GetEmailNotifyQuery : IRequest<BaseEntityResponse<EmailNotifyDto>>
    {
        public GetEmailNotifyQuery(long id, string accountId)
        {
            Id = id;
            AccountId = accountId;
        }

        public long Id { get; set; }
        public string AccountId { get; set; }
    }

    public class GetEmailNotifyQueryValidator : BaseValidator<GetEmailNotifyQuery>
    {
        public GetEmailNotifyQueryValidator(IEmailNotifyRepository repository)
        {
            RuleFor(x => x).NotNull().WithErrorCode("ID_NOT_ALLOW_NULL").MustAsync(async (x, cancellationToken) =>
            {
                var notifyConfig = await repository.GetByIdAsync(x.Id, x.AccountId);
                return notifyConfig != null;
            }).WithErrorCode("EMAIL_NOTIFY_NOT_FOUND");
        }
    }

    public class GetEmailNotifyQueryHandler : IRequestHandler<GetEmailNotifyQuery, BaseEntityResponse<EmailNotifyDto>>
    {
        private readonly IEmailNotifyRepository _repository;

        public GetEmailNotifyQueryHandler(IEmailNotifyRepository repository)
        {
            _repository = repository;
        }

        public async Task<BaseEntityResponse<EmailNotifyDto>> Handle(GetEmailNotifyQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _repository.GetByIdAsync(request.Id, request.AccountId);
            var response = new BaseEntityResponse<EmailNotifyDto>().SetData(entity.Map<EmailNotifyDto>());
            return response;
        }
    }
}