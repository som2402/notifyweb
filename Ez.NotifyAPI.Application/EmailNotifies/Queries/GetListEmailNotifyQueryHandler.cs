﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.BaseModel;
using Shared.Common.Sort;
using Shared.Extensions;

namespace Ez.NotifyAPI.Application.EmailNotifies.Queries
{
    public class GetListEmailNotifyQuery : IRequest<PagingResponse<IList<EmailNotifyListDto>>>
    {
        public GetListEmailNotifyQuery(int pageIndex, int pageSize, string accountId, string refType, int? status,
            Sorts sorts = null)
        {
            AccountId = accountId;
            RefType = refType;
            PageIndex = pageIndex;
            PageSize = pageSize;
            Status = status;
            Sorts = sorts;
        }

        public string AccountId { get; }
        public string RefType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int? Status { get; set; }
        public Sorts Sorts { get; set; }
    }

    public class GetListEmailNotifyQueryHandler : IRequestHandler<GetListEmailNotifyQuery,
        PagingResponse<IList<EmailNotifyListDto>>>
    {
        private readonly IEmailNotifyRepository _emailNotifyRepository;

        public GetListEmailNotifyQueryHandler(IEmailNotifyRepository emailNotifyRepository)
        {
            _emailNotifyRepository = emailNotifyRepository;
        }


        public async Task<PagingResponse<IList<EmailNotifyListDto>>> Handle(GetListEmailNotifyQuery request,
            CancellationToken cancellationToken)
        {
            //if (!string.IsNullOrEmpty(request.Sorts))
            //{
            //    var sortConditions = request.Sorts.TryDeserialize<IEnumerable<Sort>>();
            //    var sorts = new Sorts(sortConditions);
            //    var querySortResult = await repository.GetListByAccountId(request.AccountId, request.PageIndex, request.PageSize, sorts);

            //    var sortResponse =
            //        new PagingResponse<IList<EmailNotifyDto>>(true, new List<DetailError> { new DetailError(null, null) })
            //        {
            //            Data = querySortResult.Items.Map<List<EmailNotifyDto>>(),
            //            Total = querySortResult.Count,
            //            PageIndex = request.PageIndex,
            //            PageSize = request.PageSize
            //        };
            //    return sortResponse;
            //}

            var query = await _emailNotifyRepository.GetListByAccountId(request.AccountId, request.RefType,
                request.PageIndex,
                request.PageSize,
                request.Status);
            var response =
                new PagingResponse<IList<EmailNotifyListDto>>(true, new List<DetailError> {new DetailError(null, null)})
                {
                    Data = query.Items.Map<IList<EmailNotifyListDto>>().ToList(),
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    Total = query.Count
                };
            return response;
        }
    }
}