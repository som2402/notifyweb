﻿using System;
using Ez.Notify.Domain;
using Ez.Notify.MQ.Worker.Configs;
using Ez.NotifyAPI.Application.Config;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using GreenPipes;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shared.Constants;
using Shared.EF.Implement;
using Shared.EF.Interfaces;
using Shared.Ioc;
using Shared.Services;

namespace Ez.Notify.MQ.Worker.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataNotifyDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.DefaultConnectionString));
            });
            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<DataNotifyDbContext>>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddClassesInterfaces(typeof(IEmailNotifyRepository).Assembly);
            services.AddClassesInterfaces(typeof(IEmailSenderRepository).Assembly);
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailService, EmailSenderService>();
            return services;
        }

        public static IServiceCollection AddQueueServices(this IServiceCollection services,
            HostBuilderContext context)
        {
            var queueSettings = new QueueSettings();
            context.Configuration.GetSection("QueueSettings").Bind(queueSettings);
            services.AddMassTransit(c =>
            {
                c.AddConsumer<EmailSentConsumer>();
                c.AddConsumer<EmailUpdatedConsumer>();
                c.AddBus(serviceProvider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(queueSettings.HostName, queueSettings.VirtualHost, h =>
                    {
                        h.Username(queueSettings.UserName);
                        h.Password(queueSettings.Password);
                    });
                    cfg.ReceiveEndpoint(queueSettings.QueueSendEmail, endpointConfigurator =>
                    {
                        // endpointConfigurator.ExchangeType = ExchangeType.Direct;
                        endpointConfigurator.UseRetry(r => r.Interval(3, TimeSpan.FromSeconds(3)));
                        endpointConfigurator.Consumer<EmailSentConsumer>(serviceProvider);
                    });
                    cfg.ReceiveEndpoint(queueSettings.QueueUpdateEmail, endpointConfigurator =>
                    {
                        // endpointConfigurator.ExchangeType = ExchangeType.Direct;
                        endpointConfigurator.UseRetry(r => r.Interval(3, TimeSpan.FromSeconds(3)));
                        endpointConfigurator.Consumer<EmailUpdatedConsumer>(serviceProvider);
                    });
                }));
            });
            // services.AddSingleton<IPublishEndpoint>(provider => provider.GetRequiredService<IBusControl>());


            return services;
        }
        
        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("AppConfig").Get<AppConfig>());
            return services;
        }

        public static IServiceCollection AddConfigurationServices(this IServiceCollection service,
            HostBuilderContext context)
        {
            var queueSettings = new QueueSettings();
            context.Configuration.GetSection("QueueSettings").Bind(queueSettings);
            service.AddSingleton(queueSettings);

            return service;
        }
    }
}