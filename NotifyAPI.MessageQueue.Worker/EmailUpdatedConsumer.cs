﻿using System;
using System.Threading.Tasks;
using Ez.Notify.Common.Constants;
using Ez.Notify.MQ.Contracts.MessageContracts;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using MassTransit;
using Shared.EF.Interfaces;

namespace Ez.Notify.MQ.Worker
{
    public class EmailUpdatedConsumer : IConsumer<IEmailDetailedMessage>
    {
        private readonly IEmailNotifyRepository _emailNotifyRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EmailUpdatedConsumer(IEmailNotifyRepository emailNotifyRepository, IUnitOfWork unitOfWork)
        {
            _emailNotifyRepository = emailNotifyRepository;
            _unitOfWork = unitOfWork;
        }
        public async Task Consume(ConsumeContext<IEmailDetailedMessage> context)
        {
            try
            {
                var emailNotify = await _emailNotifyRepository.GetByIdAsync(context.Message.EmailId);
                Console.WriteLine("email updated " + context.Message.EmailId);

                if (emailNotify != null)
                {
                    emailNotify.UpdateStatus(SendStatusConstant.SubmittedSuccessfully);
                    await _emailNotifyRepository.UpdatePartialAsync(emailNotify.Id, emailNotify);

                    await _unitOfWork.CommitAsync();
                }

                // await context.RespondAsync<EmailSentAccepted>(new
                // {
                //     Value = $"Received: {context.Message.MessageId}"
                // });
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex1 email updated " + context.Message.EmailId);

                Console.WriteLine(ex);
                throw;
            }
        }
    }
}