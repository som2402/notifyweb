﻿using System;
using System.Threading.Tasks;
using Ez.Notify.MQ.Worker.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace Ez.Notify.MQ.Worker
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            await Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config
                        .SetBasePath(Environment.CurrentDirectory)
                        .AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json",
                            true);
                    config.AddEnvironmentVariables();

                    if (args != null) config.AddCommandLine(args);
                })
                .ConfigureLogging(loggingBuilder =>
                {
                    var logger = new LoggerConfiguration()
                        .MinimumLevel.Debug().MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .Enrich.FromLogContext()
                        .WriteTo.Console(
                            outputTemplate:
                            "[{Timestamp:dd-MM-yyyy HH:mm:ss} {Level:u3}]{NewLine}{Message:lj}{NewLine}{NewLine}{Exception}{NewLine}{NewLine}")
                        .WriteTo.RollingFile(@"./logs/log-{Date}.txt",
                            outputTemplate:
                            "[{Timestamp:dd-MM-yyyy HH:mm:ss} {Level:u3}]{NewLine}{Message:lj}{NewLine}{NewLine}{Exception}{NewLine}{NewLine}")
                        .CreateLogger();

                    loggingBuilder.AddSerilog(logger, true);
                })
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDbContext(hostContext.Configuration)
                        .AddConfigurationServices(hostContext)
                        .AddRepositories().AddUnitOfWork().AddServices().AddQueueServices(hostContext)
                        .AddConfig(hostContext.Configuration)
                        .AddHostedService<ConsumerWorker>();
                    // .AddHostedService<SendEmailService>().AddHostedService<UpdateStatusEmailService>();

                    //services.BuildServiceProvider().GetRequiredService<Shared.Services.IEmailService>().SendAsync("smtp.sendgrid.net",
                    //    587,
                    //    "apikey",
                    //    "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",
                    //    "info@ichiba.vn",
                    //    "ith.dev.vn@gmail.com",
                    //    "test send grid",
                    //    "test send grid",
                    //    null,
                    //    null,
                    //    null,
                    //    "test send grid"
                    //    ).Wait();
                })
                .Build()
                .RunAsync();
        }
    }
}