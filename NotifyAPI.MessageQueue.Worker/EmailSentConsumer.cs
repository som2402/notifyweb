﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ez.Notify.Common.Constants;
using Ez.Notify.Domain.Entity;
using Ez.Notify.MQ.Contracts.MessageContracts;
using Ez.NotifyAPI.Application.Config;
using Ez.NotifyAPI.Application.EmailNotifies.Repositories;
using Ez.NotifyAPI.Application.EmailSenders.Repositories;
using MailKit.Net.Smtp;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Shared.Common;
using Shared.EF.Interfaces;
using Shared.Services;

namespace Ez.Notify.MQ.Worker
{
    public class EmailSentConsumer : IConsumer<IEmailSentMessage>
    {
        private static List<EmailSender> _emailSenders = new List<EmailSender>();
        private static readonly List<int> ExcludeIds = new List<int>();
        private readonly IEmailService _emailService;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IEmailNotifyRepository _repositoryEmailNotify;
        private readonly IEmailSenderRepository _repositoryEmailSender;
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppConfig _appConfig;
        private int _currentId;

        public EmailSentConsumer(IEmailService emailService, IPublishEndpoint publishEndpoint,
            IEmailNotifyRepository repositoryEmailNotify, IEmailSenderRepository repositoryEmailSender,
            IUnitOfWork unitOfWork, AppConfig appConfig)
        {
            _emailService = emailService;
            _publishEndpoint = publishEndpoint;
            _repositoryEmailNotify = repositoryEmailNotify;
            _repositoryEmailSender = repositoryEmailSender;
            _unitOfWork = unitOfWork;
            _appConfig = appConfig;
        }


        public async Task Consume(ConsumeContext<IEmailSentMessage> context)
        {
            var emailDto = context.Message.EmailDto;
            Console.WriteLine(context.Message.EmailDto.EmailId);
            var emailNotify = await _repositoryEmailNotify.GetByIdAsync(emailDto.EmailId);
            var replaceLinkElement = await ProcessReplaceHtml(emailNotify);
            emailNotify.UpdateHtmlBody(replaceLinkElement);
            await _repositoryEmailNotify.UpdatePartialAsync(emailNotify.Id, emailNotify);
            await _unitOfWork.CommitAsync();
            try
            {
                string[] cc = null;
                string[] bcc = null;
                var emailSender =
                    await _repositoryEmailSender.GetByCode(emailNotify.SenderCode);
                if (!string.IsNullOrEmpty(emailNotify.Cc)) cc = emailNotify.Cc.Split(",");

                if (!string.IsNullOrEmpty(emailNotify.Bcc)) bcc = emailNotify.Bcc.Split(",");

                if (emailSender != null)
                {
                    if (emailSender?.Smtpport != null)
                    {
                        _currentId = emailSender.Id;
                        await _emailService.SendAsync(emailSender.Smtp, emailSender.Smtpport.Value,
                            emailSender.UserName,
                            emailSender.Password, emailSender.Email, emailNotify.To,
                            emailNotify.Subject, replaceLinkElement,
                            context.Message.EmailDto.Attachments, cc,
                            bcc, emailNotify.SenderTitle);
                    }

                    ExcludeIds.Clear();
                    var emailDetailedMessageEmailSender = new EmailDetailedMessage
                    {
                        CreationDate = DateTime.Now,
                        EmailId = context.Message.EmailDto.EmailId,
                        MessageId = Guid.NewGuid()
                    };
                    await _publishEndpoint.Publish(emailDetailedMessageEmailSender);
                }
                else
                {
                    if (_emailSenders.Count == 0)
                    {
                        var emailSenders = await _repositoryEmailSender.Queryable.ToListAsync();
                        _emailSenders = emailSenders.ToList();
                    }

                    emailSender = ExcludeIds.Count > 0
                        ? _emailSenders.FirstOrDefault(x => ExcludeIds.All(e => !x.Id.Equals(e)))
                        : _emailSenders?.FirstOrDefault(x => x.Default != null && x.Default.Value);
                    if (emailSender?.Smtpport != null)
                    {
                        _currentId = emailSender.Id;
                        await _emailService.SendAsync(emailSender.Smtp, emailSender.Smtpport.Value,
                            emailSender.UserName,
                            emailSender.Password, emailSender.UserName, emailNotify.To,
                            emailNotify.Subject, emailNotify.HtmlBody,
                            context.Message.EmailDto.Attachments, cc,
                            bcc, emailNotify.SenderTitle);
                    }

                    ExcludeIds.Clear();
                    var emailDetailedMessage = new EmailDetailedMessage
                    {
                        CreationDate = DateTime.Now,
                        EmailId = context.Message.EmailDto.EmailId,
                        MessageId = Guid.NewGuid()
                    };
                    await _publishEndpoint.Publish(emailDetailedMessage);
                }

                // await context.RespondAsync<EmailAccepted>(new
                // {
                //     Value = $"Received: {context.Message.MessageId}"
                // });
            }
            catch (SmtpCommandException smtpCommandException)
            {
                Console.WriteLine("Ex1 " + context.Message.EmailDto.EmailId);

                ExcludeIds.Add(_currentId);
                emailNotify.UpdateError(smtpCommandException.Message, SendStatusConstant.SubmitAnError);
                await _repositoryEmailNotify.UpdatePartialAsync(emailNotify.Id, emailNotify);
                await _unitOfWork.CommitAsync();
                throw;
            }
            catch (Exception e)
            {
                emailNotify.UpdateError(e.Message, SendStatusConstant.SubmitAnError);
                await _repositoryEmailNotify.UpdatePartialAsync(emailNotify.Id, emailNotify);
                await _unitOfWork.CommitAsync();
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<string> ProcessReplaceHtml(EmailNotify emailNotify)
        {
            var currentDomain = _appConfig.NotifyPublicApiDomain;
            var replaceHtml = Regex.Replace(emailNotify.HtmlBody, @"\\r\\n", "");
            replaceHtml = replaceHtml.Replace("\\", "");
            // var hashLogo = GetHashOpenEmail(emailNotify.Id);
            // var srcLogo = string.Format(Tracking.FormatOpenEmailTracking, currentDomain, Tracking.LinkOpenEmailTracking,
            //     emailNotify.Id, hashLogo);
            // replaceHtml = replaceHtml.Replace("{{logo}}", srcLogo);
            // if (string.IsNullOrEmpty(replaceHtml)) return replaceHtml;
            // TextReader reader = new StringReader(replaceHtml ?? string.Empty);
            // var doc = new HtmlDocument();
            // doc.Load(reader);
            // var nodeAElement = doc.DocumentNode.SelectNodes("//a[@href]");
            // if (nodeAElement != null)
            //     foreach (var link in nodeAElement)
            //     {
            //         var att = link.Attributes["href"];
            //         var oldUrl = att.Value;
            //         var hash = GetHash(emailNotify.Id, oldUrl);
            //         att.Value = string.Format(Tracking.FormatLinkClickTracking, currentDomain, Tracking.LinkTracking,
            //             emailNotify.Id, oldUrl, hash);
            //     }
            //
            // var memoryStream = new MemoryStream();
            // doc.Save(memoryStream);
            // memoryStream.Seek(0, SeekOrigin.Begin);
            // var streamReader = new StreamReader(memoryStream);
            // replaceHtml = await streamReader.ReadToEndAsync();
            return replaceHtml;
        }


        private string GetHash(long id, string url)
        {
            var hashInput = string.Join("|", id, url);
            var hash = Utility.BCryptHash(hashInput, _appConfig.ClickTrackingPrivateKeyPepper);
            return hash;
        }

        private string GetHashOpenEmail(long id)
        {
            var hash = Utility.BCryptHash(id.ToString(), _appConfig.ClickTrackingPrivateKeyPepper);
            return hash;
        }
    }
}